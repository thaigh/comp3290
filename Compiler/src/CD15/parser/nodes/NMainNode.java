package CD15.parser.nodes;

import CD15.entity.TokenType;
import CD15.parser.NodeDataType;
import CD15.parser.ParseTreeNodeType;
import CD15.parser.Parser;
import CD15.parser.TreeNode;

import java.util.Arrays;

/**
 * Generates a Main node of the form:
 * < main> ::= local < idList> ; < stats>
 *
 * @author Tyler Haigh - C3182929
 * @since 27/08/2015
 */
public class NMainNode {

    /**
     * Attempts to generate the Main Node given a valid token stream
     * @param p The Parser
     * @return A valid Tree Node for a valid token stream or NUNDF
     */
    public static TreeNode make(Parser p) {

        TreeNode main = new TreeNode(ParseTreeNodeType.NUNDEF);

        if (!p.peekAndConsume(TokenType.TLOCL)) {
            p.syntaxError("Expected keyword 'local'", p.peekLine(), p.peekColumn());
            return main;
        }

        TreeNode idList = NIdListNode.make(p);
        if (idList != null && idList.getNodeType() == ParseTreeNodeType.NUNDEF) {
            try { errorRecoverIdList(p); }
            catch (Exception e) { return main; }
        }

        if (!p.peekAndConsume(TokenType.TSEMI)) {
            p.syntaxError("Expected semicolon ; at end of ID List", p.peekLine(), p.peekColumn());
            try { errorRecoverySemi(p); }
            catch (Exception e) { return main; }
        }

        TreeNode stats = SStatsNode.make(p);

        main = new TreeNode(ParseTreeNodeType.NMAIN, idList, stats);
        main.setDataType(NodeDataType.Node);
        return main;
    }

    /**
     * Attempts to recover after processing the ID List
     * @param p The Parser
     * @throws Exception Unable to recover
     */
    private static void errorRecoverIdList(Parser p) throws Exception {
        int nextSemi = p.next(TokenType.TSEMI);
        if (nextSemi > -1) p.consume(nextSemi);
        else throw new Exception("Unable to recover");
    }

    /**
     * Attempts to recover after failing to consume the semi-colon
     * @param p The Parser
     * @throws Exception Unable to recover
     */
    private static void errorRecoverySemi(Parser p) throws Exception {
        int[] scanAmounts = new int[12];
        // Scan ahead to find the next statement or END
        scanAmounts[0] = p.next(TokenType.TLOOP);
        scanAmounts[1] = p.next(TokenType.TEXIT);
        scanAmounts[2] = p.next(TokenType.TIFKW);
        scanAmounts[3] = p.next(TokenType.TINPT);
        scanAmounts[4] = p.next(TokenType.TPRIN);
        scanAmounts[5] = p.next(TokenType.TPRLN);
        scanAmounts[6] = p.next(TokenType.TCALL);
        scanAmounts[7] = p.next(TokenType.TIDNT);
        scanAmounts[8] = p.next(TokenType.TELSE);
        scanAmounts[9] = p.next(TokenType.TELSF);
        scanAmounts[10] = p.next(TokenType.TSEMI); // SEMI for IdList or first Statement
        scanAmounts[11] = p.next(TokenType.TENDK); // END PROC or END PROGRAM

        // Find the min
        Arrays.sort(scanAmounts);
        int minScanAmount = -1;
        for (int i = 0; i < scanAmounts.length; i++) {
            if (scanAmounts[i] != -1) {
                minScanAmount = scanAmounts[i];
                break;
            }
        }

        if (minScanAmount == -1) throw new Exception("Unable to scan ahead");
        else {
            if (p.peek(minScanAmount).getType() == TokenType.TSEMI)
                p.consume(minScanAmount+1);
            else p.consume(minScanAmount);
        }
    }
}
