package CD15.codegen;

/**
 * Defines the available Op Codes used for the AC 15 Simulator
 *
 * @author Tyler Haigh - C3182929
 * @since 7/10/2015
 */
public enum OpCode {



    /** HALT - Stop this run, close files */HALT(0),
    /** NOOP - Do Nothing. Used to blank out bytes by hand */NOOP(1),
    /** CLEAR - Set ACC to FLOT 0.0 */CLEAR(2),

    // Single Byte Arithmetic/Logical instructions

    /** CHGS - Change Sign of integer or float in ACC */CHGS(3),
    /** ABS - Absolute value of integer or float in ACC */ABS(4),
    /** NOT - Logical Negation of boolean value in the accumulator */NOT(5),

    // Relational Operators - All are single byte - testing the accumulator against zero

    GRT(10),
    GEQ(11),
    LSS(12),
    LEQ(13),
    EQL(14),
    NEQ(15),

    // Input/Output Instructions - Names of I/O Files obtained from user interface

    /** READF - Read a float value from the input file */READF(20),
    /** READI - Read a long integer value from the input file */READI(21),
    /** VALPR - Print a space and a value to the output file */VALPR(22),
    /** STRPR - Print a String whose address is on the Stack */STRPR(23),
    /** CHRPR - Print a Single Character */CHRPR(24),
    /** NEWLN - Print a newline character to the output file */NEWLN(25),
    /** SPACE - Print a space character to the output file */SPACE(26),

    // Single Byte Index Register Operations

    /** ACCIDX - Move (int) ACC Fl Pt value into Index Register */ACCIDX(30),
    /** INCIDX - Increment Index Register */INCIDX(31),

    // +++++++++++++++++++++++ AUTOINDIRECT OPERATIONS +++++++++++++++++
    // These have a ",D" option (odd opcodes) which address an
    // array or subprogram via the adr and sdr respectively.
    // The standard (even) opcode opnd is a 4 byte memory address.
    // The ",D" (odd) opcode opnd is a 2 byte array or subprog no.

    // Arithmetic Instructions - Operate on Accumulator

    /** ADD -  Add to ACC */ADD(50),
    /** ADD,D -  Add to ACC */ADD_D(51),
    /** SUB -  Subtract (ACC = ACC - opnd) */SUB(52),
    /** SUB,D -  Subtract (ACC = ACC - opnd) */SUB_D(53),
    /** MUL -  Multiply into ACC */MUL(54),
    /** MUL,D -  Multiply into ACC */MUL_D(55),
    /** DIV -  Divide (ACC = ACC / opnd) */DIV(56),
    /** DIV,D -  Divide (ACC = ACC / opnd) */DIV_D(57),
    /** IDIV - Integer Divide (ACC = ACC div opnd) */IDIV(58),
    /** IDIV_D - Integer Divide (ACC = ACC div opnd) */IDIV_D(59),

    // Logical Instructions

    /** AND - Logical AND */AND(60),
    /** AND,D - Logical AND */AND_D(61),
    /** OR  - Logical OR */OR(62),
    /** OR,D  - Logical OR */OR_D(63),
    /** XOR - Exclusive OR */XOR(64),
    /** XOR - Exclusive OR */XOR_D(65),

    // Get/Store Instructions

    /** GET - Get moves operand value into ACC */GET(66),
    /** GET,D - Get moves operand value into ACC */GET_D(67),
    /** STO - Store moves ACC value to operand memory target */STO(68),
    /** STO,D - Store moves ACC value to operand memory target */STO_D(69),

    // Index Register Load

    /** LDIDX - move rounded memory value at 4-byte address into Index Register */LDIDX(72),

    // +++++++++++++++++++++++++++++++++ NON-AUTOINDIRECT OPERATIONS +++++++++++++++++++
    // These still have an effective ",D" option

    // Call/Return Instructions

    /** CALL - takes a 2-byte subprogram number as its operand */CALL(81),
    /** RETN - takes a 2-byte subprogram number as its operand */RETN(83),

    // Get/Store Direct Instructions

    /** GETADDR - does a get without autoindirection */GETADDR(84),
    /** GETADDR,D - gets memory address value for an array element via adr and idx */GETADDR_D(85),
    /** GETDESC - gets a copy of an array descriptor via a standard 4-byte address - for copying formal DESC parameters */GETDESC(86),
    /** GETDESC,D - gets copy of actual descriptor (no autoindirection) */GETDESC_D(87),
    /** STODIR - does a direct store without autoindirection */STODIR(88),

    // Branch (Jump) Instructions - All have even opcodes as they all take a 4-byte opnd as a branch distance

    CBB(90),
    CBF(92),
    BB(94),
    BF(96);

    private int value;
    private OpCode(int val) { this.value = val; }
    public int value() { return value; }
}
