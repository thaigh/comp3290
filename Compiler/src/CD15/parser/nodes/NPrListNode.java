package CD15.parser.nodes;

import CD15.entity.Token;
import CD15.entity.TokenType;
import CD15.parser.NodeDataType;
import CD15.parser.ParseTreeNodeType;
import CD15.parser.Parser;
import CD15.parser.TreeNode;

/**
 * Generates a PrList node of the form:
 * < prList>        ::= < printItem> < prListTail>
 * < prListTail>    ::= ɛ | , < prList>
 *
 * @author Tyler Haigh - C3182929
 * @since 29/08/2015
 */
public class NPrListNode {

    /**
     * Attempts to generate a PrList node for a valid token steam
     * @param p The Parser
     * @return A valid Tree Node for a valid token stream or NUNDF
     */
    public static TreeNode make(Parser p) {

        TreeNode printItem = printItem(p);
        if (printItem != null && printItem.getNodeType() == ParseTreeNodeType.NUNDEF) {
            try { errorRecovery(p); }
            catch (Exception e) { return new TreeNode(ParseTreeNodeType.NUNDEF); }
        }

        TreeNode tail = prListTail(p);

        if (tail == null) return printItem;
        else {
            TreeNode prList = new TreeNode(ParseTreeNodeType.NPRLIST, printItem, tail);
            prList.setDataType(NodeDataType.Node);
            return prList;
        }
    }

    /**
     * Generates a printitem node
     * @param p The Parser
     * @return Either a String or Expression Node
     */
    private static TreeNode printItem(Parser p) {

        Token nextTok = p.peek();
        if (nextTok.getType() == TokenType.TSTRG) {
            p.consume();

            p.insertConstant(nextTok);
            TreeNode string = new TreeNode(ParseTreeNodeType.NSTRG);
            string.setConstRecord(nextTok.getSymbolTableRecord());
            string.setDataType(NodeDataType.String);
            return string;
        } else {
            TreeNode expr = NExprNode.make(p);
            return expr;
        }
    }

    /**
     * Generates the PrintList Trail Node
     * @param p The Parser
     * @return Null if there is no PrList node following or a Tree Node for the PrList
     */
    private static TreeNode prListTail(Parser p) {

        Token nextTok = p.peek();
        if (nextTok.getType() != TokenType.TCOMA) return null;
        else {
            p.consume();

            TreeNode prList = make(p);
            return prList;
        }
    }

    /**
     * Attempts error recovery in the event of an invalid token stream
     * @param p The Parser
     * @throws Exception Unable to recover
     */
    private static void errorRecovery(Parser p) throws Exception {
        // Find next comma or semi
        int nextComma = p.next(TokenType.TCOMA);
        int nextSemi = p.next(TokenType.TSEMI);

        if (nextComma > -1 && nextComma < 5) p.consume(nextComma);
        else if (nextSemi > -1) p.consume(nextSemi);
        else throw new Exception("Unable to recover");
    }
}
