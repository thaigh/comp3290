package CD15.entity;

/**
 * Defines the type of error message created
 *
 * @author Tyler Haigh - C3182929
 * @since 2/08/2015
 */
public enum ErrorType {
    Lexical,
    Syntactic,
    Semantic,
    Logical,
    Fatal
}
