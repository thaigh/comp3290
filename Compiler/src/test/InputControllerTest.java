package test;

import CD15.controller.InputController;
import junit.framework.Assert;
import org.junit.Test;

/** 
* InputController Tester. 
* 
* @author <Authors name> 
* @since <pre>Aug 15, 2015</pre> 
* @version 1.0 
*/ 
public class InputControllerTest { 


    /**
     * Method: processInput(String input)
     * Testing for removal of \r\n to only \n
     */
    @Test
    public void testProcessInput_rn() throws Exception {
        String before = "This \r\n Line";
        String expected = "This \n Line";
        String actual = InputController.processInput(before);

        Assert.assertEquals(expected, actual);
    }

    /**
     * Method: processInput(String input)
     * Testing conversion of \t to ascii space
     */
    @Test
    public void testProcessInput_tab() throws Exception {
        String before = "This \t Line";
        String expected = "This   Line";
        String actual = InputController.processInput(before);

        Assert.assertEquals(expected, actual);
    }


} 
