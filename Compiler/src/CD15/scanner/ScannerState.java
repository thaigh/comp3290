package CD15.scanner;

/**
 * Defines the possible States the Scanner class can be in whilst processing
 * input from the source file
 *
 * @author Tyler Haigh - C3182929
 * @since 2/08/2015
 */
public enum ScannerState {
    Initialised,
    Illegal,
    Return,

    // Identifiers
    Identifier,
    InvalidIdentifier,
    KeyWord,

    // Literals
    StringLiteral,
    StringLiteralTerminate,
    InvalidString,

    ZeroLiteral,
    IntegerLiteral,
    RealLiteral,
    InvalidIntLiteral,
    InvalidRealLiteral,

    // Punctuation - Should return immediately if found
    RightParenthesis,
    LeftParenthesis,
    Comma,
    RightBracket,
    LeftBracket,
    SemiColon,
    DotOperator,

    // Staging Areas
    RealLiteralStaging,
    SingleCommentOrDivide,
    CommentOrAdd,
    BangStaging,
    MultiLineCommentStage2,
    MultiLineCommentUnStage2,
    MultiLineCommentUnStage1,

    // Operators
    Divide,
    DivideEquals,
    Multiply,
    MultiplyEquals,
    Minus,
    MinusEquals,
    Add,
    AddEquals,
    Assignment,
    Equality,
    BangEquals,
    GreaterThan,
    GreaterThanEquals,
    LessThan,
    LessThanEquals,
    InvalidOperator,

    // Comments
    SingleLineComment,
    MultiLineComment,
    TerminateMLComment,

    // Halting Region - If no whitespace was found between tokens, but still valid
    Halt,


}
