package CD15.parser.nodes;

import CD15.entity.Token;
import CD15.entity.TokenType;
import CD15.parser.NodeDataType;
import CD15.parser.ParseTreeNodeType;
import CD15.parser.Parser;
import CD15.parser.TreeNode;

/**
 * Generates a PList node of the form:
 * < pList>     ::= < param> < pListTail>
 * < pListTail> ::= ɛ | , < pList>
 *
 * @author Tyler Haigh - C3182929
 * @since 28/08/2015
 */
public class NPListNode {

    /**
     * Attempts to make a PList Node given a valid token stream
     * @param p The Parser
     * @return A valid Tree Node for a valid token stream or NUNDF
     */
    public static TreeNode make(Parser p) {

        TreeNode param = param(p);
        if (param != null && param.getNodeType() == ParseTreeNodeType.NUNDEF) {
            try { errorRecovery(p); }
            catch (Exception e) { return new TreeNode(ParseTreeNodeType.NUNDEF); }
        }

        TreeNode tail = pListTail(p);

        if (tail == null) return param;
        else {
            TreeNode pList = new TreeNode(ParseTreeNodeType.NPLIST, param, tail);
            pList.setDataType(NodeDataType.Node);
            return pList;
        }
    }

    /**
     * Generates a param node of the form:
     * < param>         ::= < id> < paramTail>
     * < paramTail>     ::= ɛ | [ ]
     * @param p The Parser
     * @return A valid Tree Node for a valid token stream or NUNDF
     */
    private static TreeNode param(Parser p) {

        TreeNode param = new TreeNode(ParseTreeNodeType.NUNDEF);

        if (!p.peekAndConsume(TokenType.TIDNT)) {
            p.syntaxError("Expected identifier for variable parameter", p.peekLine(), p.peekColumn());

            // Check if we are still processing VARs or have found VALs
            if (p.peek().getType() == TokenType.TVALP)
                return null;

            return param;
        }
        Token id = p.lastToken();

        // Prevent double declaration
        if (p.lookupIdentifier(id.getLexeme()) != null) {
            p.semanticError("Param '" + id.getLexeme() + "' is already defined", id.getLine(), id.getColumn());
            p.createInvalidRecord(id);
        } else {
            p.insertIdentifier(id);
        }

        // Process the tail. Easier to do here than in another method
        // ɛ | [ ]
        Token nextTok = p.peek();
        if (nextTok.getType() == TokenType.TLBRK) {
            p.consume();

            if (!p.peekAndConsume(TokenType.TRBRK)) {
                p.syntaxError("Expected right bracket ] for array variable parameter", p.peekLine(), p.peekColumn());
                return param;
            }

            // Return NARRPAR
            param = new TreeNode(ParseTreeNodeType.NARRPAR);
            param.setDataType(NodeDataType.FloatArray);
        } else {
            // Return NSIMPAR
            param = new TreeNode(ParseTreeNodeType.NSIMPAR);
            param.setDataType(NodeDataType.Float);
        }

        param.setIdentifierRecord(id.getSymbolTableRecord());

        // Give the Symbol Table Record a link to the node so we can verify data types later
        id.getSymbolTableRecord().setTreeNode(param);

        return param;

    }

    /**
     * Generates the tail for the PList node
     * @param p The Parser
     * @return Null if no PList node follows or a Tree Node for the PList
     */
    public static TreeNode pListTail(Parser p) {

        Token nextTok = p.peek();
        if (nextTok.getType() != TokenType.TCOMA) return null;
        else {
            p.consume();

            TreeNode pList = make(p);
            return  pList;
        }
    }

    /**
     * Attempts error recovery in the event of an invalid token stream
     * @param p The Parser
     * @throws Exception Unable to recover
     */
    private static void errorRecovery(Parser p) throws Exception {
        int nextComma = p.next(TokenType.TCOMA);
        if (nextComma > -1 && nextComma < 3)
            p.consume(nextComma);
        else throw new Exception("Unable to recover");
    }

}
