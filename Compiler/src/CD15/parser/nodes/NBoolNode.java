package CD15.parser.nodes;

import CD15.entity.Token;
import CD15.entity.TokenType;
import CD15.parser.NodeDataType;
import CD15.parser.ParseTreeNodeType;
import CD15.parser.Parser;
import CD15.parser.TreeNode;

/**
 * Generates a Bool node of the form:
 *
 * < bool>      ::= < rel> < boolTail>
 * < boolTail>  ::= ɛ | < logOp> < bool>
 *
 * @author Tyler Haigh - C3182929
 * @since 29/08/2015
 */
public class NBoolNode {

    /**
     * Attempts to make a bool node
     * @param p The Parser
     * @return A valid bool node for a valid token stream, or NUNDF is invalid token stream
     */
    public static TreeNode make(Parser p) {

        TreeNode rel = NRelNode.make(p);
        TreeNode tail = boolTail(p, rel);

        if (tail == null) return rel;
        else return tail;
    }

    /**
     * Attempts to make a tail node for the bool expression
     * @param p The Parser
     * @param left The left side of the bool expression
     * @return A valid bool node for a valid token stream or null if no logical operator
     *         joins two boolean expressions
     */
    private static TreeNode boolTail(Parser p, TreeNode left) {

        Token nextTok = p.peek();
        switch (nextTok.getType()) {
            case TANDK: case TORKW: case TXORK: {
                TreeNode logOp = logOp(p);

                TreeNode rel = NRelNode.make(p);
                logOp.setLeft(left);
                logOp.setRight(rel);

                Token followsRel = p.peek();

                if (left.getDataType() != NodeDataType.Boolean) {
                    p.semanticError("Left side of the expression does not evaluate to a boolean expression", nextTok.getLine(), nextTok.getColumn());
                } else if (rel.getDataType() != NodeDataType.Boolean) {
                    p.semanticError("Right side of the expression does not evaluate to a boolean expression", followsRel.getLine(), followsRel.getColumn());
                } else {
                    logOp.setDataType(NodeDataType.Boolean);
                }

                TreeNode boolTail = boolTail(p, logOp);
                if (boolTail == null)return logOp;
                else return boolTail;
            }
            default: return null;
        }
    }

    /**
     * Generates a logical operator node
     * @param p The Parser
     * @return The logical operator node for a given Token
     */
    private static TreeNode logOp (Parser p) {
        Token nextTok = p.peek();
        TreeNode logOp = new TreeNode(ParseTreeNodeType.NUNDEF);

        if (nextTok.getType() == TokenType.TANDK) {
            p.consume();
            logOp.setNodeType(ParseTreeNodeType.NAND);
        }
        if (nextTok.getType() == TokenType.TORKW) {
            p.consume();
            logOp.setNodeType(ParseTreeNodeType.NOR);
        }
        if (nextTok.getType() == TokenType.TXORK) {
            p.consume();
            logOp.setNodeType(ParseTreeNodeType.NXOR);
        }

        // Check if the did consume the token
        if (logOp.getNodeType() == ParseTreeNodeType.NUNDEF)
            p.syntaxError("Expected a logical operator in boolean expression", nextTok.getLine(), nextTok.getColumn());

        return logOp;
    }
}
