package CD15.entity;

/**
 * An error message generated during compilation
 *
 * @author Tyler Haigh - C3182929
 * @since 2/08/2015
 */
public class Error {

    private ErrorType type;
    private String message;
    private int line;
    private int column;

    public Error() {
        this.type = ErrorType.Lexical;
        this.message = "";
        this.line = 0;
        this.column = 0;
    }

    public Error(ErrorType type, String message, int line, int column) {
        this.type = type;
        this.message = message;
        this.line = line;
        this.column = column;
    }

    @Override
    public String toString() {
        return type.name() + " Error at Line: " + line +  " Column: " + column + ": " + message ;
    }

    public ErrorType getType() {
        return type;
    }

    public void setType(ErrorType type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line = line;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }
}
