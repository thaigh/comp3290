package CD15.parser;

/**
 * Defines the possible data types for a Tree Node.
 * These will be used in Semantic Analysis and Code Generation
 *
 * @author Tyler Haigh - C3182929
 * @since 19/09/2015
 */
public enum NodeDataType {
    Float,
    FloatArray,
    Boolean,
    Undefined,
    String,

    // Special datatypes
    Program,
    Procedure,
    Node
}
