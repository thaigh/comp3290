package CD15.parser.nodes;

import CD15.entity.Token;
import CD15.entity.TokenType;
import CD15.parser.NodeDataType;
import CD15.parser.ParseTreeNodeType;
import CD15.parser.Parser;
import CD15.parser.TreeNode;

/**
 * Generates a Param ID List Node of the form:
 * < idList>        ::= < id> < idListTail>
 * < idListTail>    ::= ɛ | , < idList>
 *
 * @author Tyler Haigh - C3182929
 * @since 28/08/2015
 */
public class NPIdListNode {

    /**
     * Attempts to generate a PIdList Node given a valid token stream
     * @param p The Parser
     * @return A valid Tree Node for a valid token stream or NUNDF
     */
    public static TreeNode make(Parser p) {

        TreeNode idNode = new TreeNode(ParseTreeNodeType.NUNDEF);

        Token id = p.peek();
        if (id.getType() != TokenType.TIDNT) {
            p.syntaxError("Expected identifier name for value parameter", p.peekLine(), p.peekColumn());

            // Check if we have found ourselves in LOCAL or STAT before trying to recover
            TokenType type = id.getType();
            switch (type) {
                case TLOOP: case TEXIT: case TIFKW: case TINPT:
                case TPRIN: case TPRLN: case TCALL: case TSEMI:
                case TENDK: case TLOCL:
                    return idNode;
            }

            try { errorRecovery(p); }
            catch (Exception e) { return idNode; }
        } else {
            p.consume();

            // Prevent double declaration
            if (p.lookupIdentifier(id.getLexeme()) != null) {
                p.semanticError("Parameter '" + id.getLexeme() + "' is already declared", id.getLine(), id.getColumn());
                p.createInvalidRecord(id);
            } else {
                p.insertIdentifier(id);
            }

            idNode = new TreeNode(ParseTreeNodeType.NSIMPAR);
            idNode.setIdentifierRecord(id.getSymbolTableRecord());
            idNode.setDataType(NodeDataType.Float);

            // Give the Symbol Table Record a link to the node so we can verify data types later
            id.getSymbolTableRecord().setTreeNode(idNode);
        }

        TreeNode tail = tail(p);
        if (tail == null) return idNode;
        else {
            TreeNode idList = new TreeNode(ParseTreeNodeType.NPLIST, idNode, tail);
            idList.setDataType(NodeDataType.Node);
            return idList;
        }
    }

    /**
     * Generates the tail node for the PIdList
     * @param p The Parser
     * @return Null if no following PIdList exists, or a Tree Node for the PIdList
     */
    private static TreeNode tail(Parser p) {

        Token nextTok = p.peek();
        if (nextTok.getType() != TokenType.TCOMA) return null;
        else {
            p.consume();

            TreeNode idList = make(p);
            return idList;
        }
    }

    /**
     * Attempts error recovery in the event of an invalid token stream
     * @param p The Parser
     * @throws Exception Unable to recover
     */
    private static void errorRecovery(Parser p) throws Exception {
        // Find next comma
        int nextComma = p.next(TokenType.TCOMA);
        if (nextComma > -1 && nextComma < 3) p.consume(nextComma);
        else throw new Exception("Unable to recover");
    }
}
