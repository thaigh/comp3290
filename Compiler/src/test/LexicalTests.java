package test;

import CD15.entity.Token;
import CD15.entity.TokenType;
import CD15.scanner.Scanner;
import junit.framework.Assert;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;


/**
 * Scanner Tester.
 *
 * @author Tyler Haigh
 * @since <pre>Aug 18, 2015</pre>
 * @version 1.0
 */
public class LexicalTests {

    /**
     * Method: nextToken(String line)
     */
    @Test
    public void testStringFail() throws Exception {
        String inputs = "+/+\n" +
                "  This program fails the lexical analysis stage of\n" +
                "  compilation as it contains a string that is not terminated\n" +
                "  on the same line (i.e. Contains a newline character before\n" +
                "    +/+ \n" +
                " +/+ \n" +
                "  terminating \"\n" +
                "+/+\n" +
                "program stringFail\n" +
                "    local var;\n" +
                "    printline \"Hello world\n" +
                "end program stringFail\n" +
                "\n";

        List<Token> tokens = new LinkedList<Token>() {{
            add(new Token(TokenType.TPROG, null, 9, 1));
            add(new Token(TokenType.TIDNT, "stringFail", 9, 9));
            add(new Token(TokenType.TLOCL, null, 10, 5));
            add(new Token(TokenType.TVARP, null, 10, 11));
            add(new Token(TokenType.TSEMI, null, 10, 14));
            add(new Token(TokenType.TPRLN, null, 11, 5));
            add(new Token(TokenType.TUNDF, "\"Hello world", 11, 15));
            add(new Token(TokenType.TENDK, null, 12, 1));
            add(new Token(TokenType.TPROG, null, 12, 5));
            add(new Token(TokenType.TIDNT, "stringFail", 12, 13));
            add(new Token(TokenType.TEOF, null, 14, 1));
        }};

        Scanner sc = new Scanner(inputs);
        int i = 0;
        while(!sc.isFinished()) {
            Token tok = sc.nextValidToken();

            if (tok != null) {
                Assert.assertEquals(tokens.get(i).getType(), tok.getType());
                Assert.assertEquals(tokens.get(i).getLexeme(), tok.getLexeme());
                Assert.assertEquals(tokens.get(i).getLine(), tok.getLine());
                Assert.assertEquals(tokens.get(i).getColumn(), tok.getColumn());
                i++;
            }
        }
    }

    @Test
    public void testUnterminatedCommentFail() throws Exception {
        final String inputs = "+/+\n" +
                "  This program will fail the lexical analysis stage as it\n" +
                "  does not contain a terminator for the block comment\n" +
                "\n" +
                "program unterminatedComment\n" +
                "    local varx ;\n" +
                "    printline varx ;\n" +
                "end program unterminatedComment\n" +
                "\n" +
                "\n";

        List<Token> tokens = new LinkedList<Token>() {{
            add(new Token(TokenType.TUNDF, "\"" + inputs, 11, 1)); // This is acceptable
            add(new Token(TokenType.TEOF, null, 11, 1));
        }};

        Scanner sc = new Scanner(inputs);
        int i = 0;
        while(!sc.isFinished()) {
            Token tok = sc.nextValidToken();

            if (tok != null) {
                Assert.assertEquals(tokens.get(i).getType(), tok.getType());
                Assert.assertEquals(tokens.get(i).getLexeme(), tok.getLexeme());
                Assert.assertEquals(tokens.get(i).getLine(), tok.getLine());
                Assert.assertEquals(tokens.get(i).getColumn(), tok.getColumn());
                i++;
            }
        }
    }

    @Test
    public void testCompositeOperatorFail() throws Exception {
        String inputs = "+/+\n" +
                "  This program fails the lexical analysis stage of\n" +
                "  compilation as it contains a space between the bang(!)\n" +
                "  character and the equals character\n" +
                "\n" +
                "+/+\n" +
                "program compositeOperatorFail\n" +
                "    ! = // Return TUNDF and TASGN\n" +
                "end program compositeOperatorFail\n" +
                "\n";

        List<Token> tokens = new LinkedList<Token>() {{
            add(new Token(TokenType.TPROG, null, 7, 1));
            add(new Token(TokenType.TIDNT, "compositeOperatorFail", 7, 9));
            add(new Token(TokenType.TUNDF, "!", 8, 5));
            add(new Token(TokenType.TASGN, null, 8, 7));
            add(new Token(TokenType.TENDK, null, 9, 1));
            add(new Token(TokenType.TPROG, null, 9, 5));
            add(new Token(TokenType.TIDNT, "compositeOperatorFail", 9, 13));
            add(new Token(TokenType.TEOF, null, 11, 1));
        }};

        Scanner sc = new Scanner(inputs);
        int i = 0;
        while(!sc.isFinished()) {
            Token tok = sc.nextValidToken();

            if (tok != null) {
                Assert.assertEquals(tokens.get(i).getType(), tok.getType());
                Assert.assertEquals(tokens.get(i).getLexeme(), tok.getLexeme());
                Assert.assertEquals(tokens.get(i).getLine(), tok.getLine());
                Assert.assertEquals(tokens.get(i).getColumn(), tok.getColumn());
                i++;
            }
        }
    }

    @Test
    public void testIllegalCharacterFail() throws Exception {
        String inputs = "+/+\n" +
                "  This program fails the lexical analysis stage of\r\n" +
                "  compilation as it contains several illegal characters that\n" +
                "  are unsupported by the language\r\n" +
                "\r\n" +
                "+/+\n" +
                "program illegalCharacterFail\r\n" +
                "    printline / not& and~ or@ xor\n" +
                "end program illegalCharacterFail\r\n" +
                "\r\n";

        List<Token> tokens = new LinkedList<Token>() {{
            add(new Token(TokenType.TPROG, null, 7, 1));
            add(new Token(TokenType.TIDNT, "illegalCharacterFail", 7, 9));
            add(new Token(TokenType.TPRLN, null, 8, 5));
            add(new Token(TokenType.TDIVD, null, 8, 15));
            add(new Token(TokenType.TNOTK, null, 8, 17));
            add(new Token(TokenType.TUNDF, "&", 8, 20));
            add(new Token(TokenType.TANDK, null, 8, 22));
            add(new Token(TokenType.TUNDF, "~", 8, 25));
            add(new Token(TokenType.TORKW, null, 8, 27));
            add(new Token(TokenType.TUNDF, "@", 8, 29));
            add(new Token(TokenType.TXORK, null, 8, 31));
            add(new Token(TokenType.TENDK, null, 9, 1));
            add(new Token(TokenType.TPROG, null, 9, 5));
            add(new Token(TokenType.TIDNT, "illegalCharacterFail", 9, 13));
            add(new Token(TokenType.TEOF, null, 11, 1));
        }};

        Scanner sc = new Scanner(inputs);
        int i = 0;
        while(!sc.isFinished()) {
            Token tok = sc.nextValidToken();

            if (tok != null) {
                Assert.assertEquals(tokens.get(i).getType(), tok.getType());
                Assert.assertEquals(tokens.get(i).getLexeme(), tok.getLexeme());
                Assert.assertEquals(tokens.get(i).getLine(), tok.getLine());
                Assert.assertEquals(tokens.get(i).getColumn(), tok.getColumn());
                i++;
            }
        }
    }

    @Test
    public void testInvalidFlitFail() throws Exception {
        String inputs = "+/+\n" +
                "  This program fails the lexical analysis stage of\n" +
                "  compilation as it contains invalid TILIT and TFLIT tokens\n" +
                "+/+\n" +
                "program invalidFlitFail \n" +
                "    123abc // Invalid\n" +
                "    123.abc // Invalid 123.   Valid abc\n" +
                "    123.456abc // Invalid\n" +
                "end program invalidFlitFail\n" +
                "\n";

        List<Token> tokens = new LinkedList<Token>() {{
            add(new Token(TokenType.TPROG, null, 5, 1));
            add(new Token(TokenType.TIDNT, "invalidFlitFail", 5, 9));
            add(new Token(TokenType.TUNDF, "123abc", 6, 5));
            add(new Token(TokenType.TUNDF, "123.", 7, 5));
            add(new Token(TokenType.TIDNT, "abc", 7, 9));
            add(new Token(TokenType.TUNDF, "123.456abc", 8, 5));
            add(new Token(TokenType.TENDK, null, 9, 1));
            add(new Token(TokenType.TPROG, null, 9, 5));
            add(new Token(TokenType.TIDNT, "invalidFlitFail", 9, 13));
            add(new Token(TokenType.TEOF, null, 11, 1));
        }};

        Scanner sc = new Scanner(inputs);
        int i = 0;
        while(!sc.isFinished()) {
            Token tok = sc.nextValidToken();

            if (tok != null) {
                Assert.assertEquals(tokens.get(i).getType(), tok.getType());
                Assert.assertEquals(tokens.get(i).getLexeme(), tok.getLexeme());
                Assert.assertEquals(tokens.get(i).getLine(), tok.getLine());
                Assert.assertEquals(tokens.get(i).getColumn(), tok.getColumn());
                i++;
            }
        }
    }

    @Test
    public void testMissingSemiPass() throws Exception {
        String inputs = "+/+\n" +
                "  This program passes the lexical analysis stage of\n" +
                "  compilation but will fail the syntactic analysis stage\n" +
                "  as it does not contain a semi-colon when declaring the\n" +
                "  local variable \"varx\"\n" +
                "+/+\n" +
                "program missingSemi\n" +
                "    local varx // Missing semi colon\n" +
                "    printline varx;\n" +
                "end program missingSemi\n" +
                "\n";

        List<Token> tokens = new LinkedList<Token>() {{
            add(new Token(TokenType.TPROG, null, 7, 1));
            add(new Token(TokenType.TIDNT, "missingSemi", 7, 9));
            add(new Token(TokenType.TLOCL, null, 8, 5));
            add(new Token(TokenType.TIDNT, "varx", 8, 11));
            //add(new Token(TokenType.TSEMI, null, 8, 1));
            add(new Token(TokenType.TPRLN, null, 9, 5));
            add(new Token(TokenType.TIDNT, "varx", 9, 15));
            add(new Token(TokenType.TSEMI, null, 9, 19));
            add(new Token(TokenType.TENDK, null, 10, 1));
            add(new Token(TokenType.TPROG, null, 10, 5));
            add(new Token(TokenType.TIDNT, "missingSemi", 10, 13));
            add(new Token(TokenType.TEOF, null, 12, 1));
        }};

        Scanner sc = new Scanner(inputs);
        int i = 0;
        while(!sc.isFinished()) {
            Token tok = sc.nextValidToken();

            if (tok != null) {
                Assert.assertEquals(tokens.get(i).getType(), tok.getType());
                Assert.assertEquals(tokens.get(i).getLexeme(), tok.getLexeme());
                Assert.assertEquals(tokens.get(i).getLine(), tok.getLine());
                Assert.assertEquals(tokens.get(i).getColumn(), tok.getColumn());
                i++;
            }
        }
    }

    @Test
    public void testMisspelledKeywordPass() throws Exception {
        String inputs = "+/+\n" +
                "  This program passes the lexical analysis stage of\n" +
                "  Compilation but fails the syntactic analysis stage\n" +
                "  as it has misspelled the keyword printline (written as\n" +
                "  printlline)\n" +
                "+/+\n" +
                "program misspelledKeyword\n" +
                "    local varx ;\n" +
                "    printlline varx ; // Misspelled keyword\n" +
                "end program misspelledKeyword\n" +
                "\n";

        List<Token> tokens = new LinkedList<Token>() {{
            add(new Token(TokenType.TPROG, null, 7, 1));
            add(new Token(TokenType.TIDNT, "misspelledKeyword", 7, 9));
            add(new Token(TokenType.TLOCL, null, 8, 5));
            add(new Token(TokenType.TIDNT, "varx", 8, 11));
            add(new Token(TokenType.TSEMI, null, 8, 16));
            add(new Token(TokenType.TIDNT, "printlline", 9, 5));
            add(new Token(TokenType.TIDNT, "varx", 9, 16));
            add(new Token(TokenType.TSEMI, null, 9, 21));
            add(new Token(TokenType.TENDK, null, 10, 1));
            add(new Token(TokenType.TPROG, null, 10, 5));
            add(new Token(TokenType.TIDNT, "misspelledKeyword", 10, 13));
            add(new Token(TokenType.TEOF, null, 12, 1));
        }};

        Scanner sc = new Scanner(inputs);
        int i = 0;
        while(!sc.isFinished()) {
            Token tok = sc.nextValidToken();

            if (tok != null) {
                Assert.assertEquals(tokens.get(i).getType(), tok.getType());
                Assert.assertEquals(tokens.get(i).getLexeme(), tok.getLexeme());
                Assert.assertEquals(tokens.get(i).getLine(), tok.getLine());
                Assert.assertEquals(tokens.get(i).getColumn(), tok.getColumn());
                i++;
            }
        }
    }

    @Test
    public void testCommentedOutProgramPass() throws Exception {
        String inputs = "+/+\n" +
                "  This program should pass lexical analysis, returning only\n" +
                "  the TEOF token to the symbol table. However, since\n" +
                "  <program> is required to have a program definition, this\n" +
                "  will not pass syntactic analysis\n" +
                "\n" +
                "  program commentedOutProgram\n" +
                "      local empty ;\n" +
                "      printline \"\" ;\n" +
                "  end program commentedOutProgram\n" +
                "+/+\n" +
                "\n";

        List<Token> tokens = new LinkedList<Token>() {{
            add(new Token(TokenType.TEOF, null, 13, 1));
        }};

        Scanner sc = new Scanner(inputs);
        int i = 0;
        while(!sc.isFinished()) {
            Token tok = sc.nextValidToken();

            if (tok != null) {
                Assert.assertEquals(tokens.get(i).getType(), tok.getType());
                Assert.assertEquals(tokens.get(i).getLexeme(), tok.getLexeme());
                Assert.assertEquals(tokens.get(i).getLine(), tok.getLine());
                Assert.assertEquals(tokens.get(i).getColumn(), tok.getColumn());
                i++;
            }
        }
    }

    @Test
    public void testMisuseKeywordPass() throws Exception {
        String inputs = "+/+\n" +
                "  This program passes lexical analysis but will fail \n" +
                "  syntactic analysis, as it attempts\n" +
                "  to make use of keywords as identifiers\n" +
                "+/+\n" +
                "\n" +
                "program helloWorld\n" +
                "    local program; // Use of keyword as identifier\n" +
                "    printline program;\n" +
                "end program helloWorld\n" +
                "\n";

        List<Token> tokens = new LinkedList<Token>() {{
            add(new Token(TokenType.TPROG, null, 7, 1));
            add(new Token(TokenType.TIDNT, "helloWorld", 7, 9));
            add(new Token(TokenType.TLOCL, null, 8, 5));
            add(new Token(TokenType.TPROG, null, 8, 11));
            add(new Token(TokenType.TSEMI, null, 8, 18));
            add(new Token(TokenType.TPRLN, null, 9, 5));
            add(new Token(TokenType.TPROG, null, 9, 15));
            add(new Token(TokenType.TSEMI, null, 9, 22));
            add(new Token(TokenType.TENDK, null, 10, 1));
            add(new Token(TokenType.TPROG, null, 10, 5));
            add(new Token(TokenType.TIDNT, "helloWorld", 10, 13));
            add(new Token(TokenType.TEOF, null, 12, 1));
        }};

        Scanner sc = new Scanner(inputs);
        int i = 0;
        while(!sc.isFinished()) {
            Token tok = sc.nextValidToken();

            if (tok != null) {
                Assert.assertEquals(tokens.get(i).getType(), tok.getType());
                Assert.assertEquals(tokens.get(i).getLexeme(), tok.getLexeme());
                Assert.assertEquals(tokens.get(i).getLine(), tok.getLine());
                Assert.assertEquals(tokens.get(i).getColumn(), tok.getColumn());
                i++;
            }
        }
    }

    @Test
    public void testNonsenseTokensPass() throws Exception {
        String inputs = "+/+\n" +
                "  This program will in fact pass lexical analysis as all\n" +
                "  are valid tokens as defined by the language, however it\n" +
                "  is nonsensical and will fail syntactic analysis\n" +
                "+/+\n" +
                "\n" +
                "program]end[arrays)proc(var;val,    .\n" +
                "=local+=loop-=exit*=when/=call==with!=if>then<=else<elsif>=\n" +
                "+input-print*printline/not and or xor div length\n";

        List<Token> tokens = new LinkedList<Token>() {{
            add(new Token(TokenType.TPROG, null, 7, 1)); add(new Token(TokenType.TRBRK, null, 7, 8));
            add(new Token(TokenType.TENDK, null, 7, 9)); add(new Token(TokenType.TLBRK, null, 7, 12));
            add(new Token(TokenType.TARRS, null, 7, 13)); add(new Token(TokenType.TRPAR, null, 7, 19));
            add(new Token(TokenType.TPROC, null, 7, 20)); add(new Token(TokenType.TLPAR, null, 7, 24));
            add(new Token(TokenType.TVARP, null, 7, 25)); add(new Token(TokenType.TSEMI, null, 7, 28));
            add(new Token(TokenType.TVALP, null, 7, 29)); add(new Token(TokenType.TCOMA, null, 7, 32));
            add(new Token(TokenType.TDOTT, null, 7, 37));

            add(new Token(TokenType.TASGN, null, 8, 1));
            add(new Token(TokenType.TLOCL, null, 8, 2)); add(new Token(TokenType.TPLEQ, null, 8, 7));
            add(new Token(TokenType.TLOOP, null, 8, 9)); add(new Token(TokenType.TMNEQ, null, 8, 13));
            add(new Token(TokenType.TEXIT, null, 8, 15)); add(new Token(TokenType.TMLEQ, null, 8, 19));
            add(new Token(TokenType.TWHEN, null, 8, 21)); add(new Token(TokenType.TDVEQ, null, 8, 25));
            add(new Token(TokenType.TCALL, null, 8, 27)); add(new Token(TokenType.TDEQL, null, 8, 31));
            add(new Token(TokenType.TWITH, null, 8, 33)); add(new Token(TokenType.TNEQL, null, 8, 37));
            add(new Token(TokenType.TIFKW, null, 8, 39)); add(new Token(TokenType.TGRTR, null, 8, 41));
            add(new Token(TokenType.TTHEN, null, 8, 42)); add(new Token(TokenType.TLEQL, null, 8, 46));
            add(new Token(TokenType.TELSE, null, 8, 48)); add(new Token(TokenType.TLESS, null, 8, 52));
            add(new Token(TokenType.TELSF, null, 8, 53)); add(new Token(TokenType.TGREQ, null, 8, 58));

            add(new Token(TokenType.TPLUS, null, 9, 1)); add(new Token(TokenType.TINPT, null, 9, 2));
            add(new Token(TokenType.TSUBT, null, 9, 7)); add(new Token(TokenType.TPRIN, null, 9, 8));
            add(new Token(TokenType.TMULT, null, 9, 13)); add(new Token(TokenType.TPRLN, null, 9, 14));
            add(new Token(TokenType.TDIVD, null, 9, 23)); add(new Token(TokenType.TNOTK, null, 9, 24));
            add(new Token(TokenType.TANDK, null, 9, 28)); add(new Token(TokenType.TORKW, null, 9, 32));
            add(new Token(TokenType.TXORK, null, 9, 35)); add(new Token(TokenType.TIDIV, null, 9, 39));
            add(new Token(TokenType.TLENG, null, 9, 43));
            add(new Token(TokenType.TEOF, null, 9, 49));
        }};

        Scanner sc = new Scanner(inputs);
        int i = 0;
        while(!sc.isFinished()) {
            Token tok = sc.nextValidToken();

            if (tok != null) {
                Assert.assertEquals(tokens.get(i).getType(), tok.getType());
                Assert.assertEquals(tokens.get(i).getLexeme(), tok.getLexeme());
                Assert.assertEquals(tokens.get(i).getLine(), tok.getLine());
                Assert.assertEquals(tokens.get(i).getColumn(), tok.getColumn());
                i++;
            }
        }
    }

    @Test
    public void testMismatchProgIdentsPass() throws Exception {
        String inputs = "+/+\n" +
                "  This program will fail semantic analysis, as it does not\n" +
                "  contain matching program identifiers\n" +
                "+/+\n" +
                "\n" +
                "program helloWorld\n" +
                "    local empty ;\n" +
                "    printline \"Hello World!\" ;\n" +
                "end program goodByeWorld\n" +
                "\n";

        List<Token> tokens = new LinkedList<Token>() {{
            add(new Token(TokenType.TPROG, null, 6, 1));
            add(new Token(TokenType.TIDNT, "helloWorld", 6, 9));
            add(new Token(TokenType.TLOCL, null, 7, 5));
            add(new Token(TokenType.TIDNT, "empty", 7, 11));
            add(new Token(TokenType.TSEMI, null, 7, 17));
            add(new Token(TokenType.TPRLN, null, 8, 5));
            add(new Token(TokenType.TSTRG, "Hello World!", 8, 15));
            add(new Token(TokenType.TSEMI, null, 8, 30));
            add(new Token(TokenType.TENDK, null, 9, 1));
            add(new Token(TokenType.TPROG, null, 9, 5));
            add(new Token(TokenType.TIDNT, "goodByeWorld", 9, 13));
            add(new Token(TokenType.TEOF, null, 11, 1));
        }};

        Scanner sc = new Scanner(inputs);
        int i = 0;
        while(!sc.isFinished()) {
            Token tok = sc.nextValidToken();

            if (tok != null) {
                Assert.assertEquals(tokens.get(i).getType(), tok.getType());
                Assert.assertEquals(tokens.get(i).getLexeme(), tok.getLexeme());
                Assert.assertEquals(tokens.get(i).getLine(), tok.getLine());
                Assert.assertEquals(tokens.get(i).getColumn(), tok.getColumn());
                i++;
            }
        }
    }

    @Test
    public void testUndefinedIdentPass() throws Exception {
        String inputs = "+/+\n" +
                "  This program will fail semantic analysis, as it attempts\n" +
                "  to make use of a variable before it has been declared\n" +
                "+/+\n" +
                "\n" +
                "program helloWorld\n" +
                "    local empty ;\n" +
                "    printline undefined;\n" +
                "end program helloWorld\n" +
                "\n";

        List<Token> tokens = new LinkedList<Token>() {{
            add(new Token(TokenType.TPROG, null, 6, 1));
            add(new Token(TokenType.TIDNT, "helloWorld", 6, 9));
            add(new Token(TokenType.TLOCL, null, 7, 5));
            add(new Token(TokenType.TIDNT, "empty", 7, 11));
            add(new Token(TokenType.TSEMI, null, 7, 17));
            add(new Token(TokenType.TPRLN, null, 8, 5));
            add(new Token(TokenType.TIDNT, "undefined", 8, 15));
            add(new Token(TokenType.TSEMI, null, 8, 24));
            add(new Token(TokenType.TENDK, null, 9, 1));
            add(new Token(TokenType.TPROG, null, 9, 5));
            add(new Token(TokenType.TIDNT, "helloWorld", 9, 13));
            add(new Token(TokenType.TEOF, null, 11, 1));
        }};

        Scanner sc = new Scanner(inputs);
        int i = 0;
        while(!sc.isFinished()) {
            Token tok = sc.nextValidToken();

            if (tok != null) {
                Assert.assertEquals(tokens.get(i).getType(), tok.getType());
                Assert.assertEquals(tokens.get(i).getLexeme(), tok.getLexeme());
                Assert.assertEquals(tokens.get(i).getLine(), tok.getLine());
                Assert.assertEquals(tokens.get(i).getColumn(), tok.getColumn());
                i++;
            }
        }
    }

    @Test
    public void testAddToNPass() throws Exception {
        String inputs = "+/+\n" +
                "  This program gets a number from the user and adds up all\n" +
                "  numbers from 0 to n (the input number)\n" +
                "+/+\n" +
                "program addToN\n" +
                "\n" +
                "    local n, counter, sum ;\n" +
                "    input n;\n" +
                "\n" +
                "    loop looper\n" +
                "        sum += counter ;\n" +
                "        counter += 1 ;\n" +
                "        exit looper when counter > n ;\n" +
                "    end loop looper\n" +
                "\n" +
                "    printline sum ;\n" +
                "end program addToN\n";

        List<Token> tokens = new LinkedList<Token>() {{
            add(new Token(TokenType.TPROG, null, 5, 1));
            add(new Token(TokenType.TIDNT, "addToN", 5, 9));
            add(new Token(TokenType.TLOCL, null, 7, 5));
            add(new Token(TokenType.TIDNT, "n", 7, 11));
            add(new Token(TokenType.TCOMA, null, 7, 12));
            add(new Token(TokenType.TIDNT, "counter", 7, 14));
            add(new Token(TokenType.TCOMA, null, 7, 21));
            add(new Token(TokenType.TIDNT, "sum", 7, 23));
            add(new Token(TokenType.TSEMI, null, 7, 27));
            add(new Token(TokenType.TINPT, null, 8, 5));
            add(new Token(TokenType.TIDNT, "n", 8, 11));
            add(new Token(TokenType.TSEMI, null, 8, 12));

            add(new Token(TokenType.TLOOP, null, 10, 5));
            add(new Token(TokenType.TIDNT, "looper", 10, 10));
            add(new Token(TokenType.TIDNT, "sum", 11, 9));
            add(new Token(TokenType.TPLEQ, null, 11, 13));
            add(new Token(TokenType.TIDNT, "counter", 11, 16));
            add(new Token(TokenType.TSEMI, null, 11, 24));
            add(new Token(TokenType.TIDNT, "counter", 12, 9));
            add(new Token(TokenType.TPLEQ, null, 12, 17));
            add(new Token(TokenType.TILIT, "1", 12, 20));
            add(new Token(TokenType.TSEMI, null, 12, 22));
            add(new Token(TokenType.TEXIT, null, 13, 9));
            add(new Token(TokenType.TIDNT, "looper", 13, 14));
            add(new Token(TokenType.TWHEN, null, 13, 21));
            add(new Token(TokenType.TIDNT, "counter", 13, 26));
            add(new Token(TokenType.TGRTR, null, 13, 34));
            add(new Token(TokenType.TIDNT, "n", 13, 36));
            add(new Token(TokenType.TSEMI, null, 13, 38));
            add(new Token(TokenType.TENDK, null, 14, 5));
            add(new Token(TokenType.TLOOP, null, 14, 9));
            add(new Token(TokenType.TIDNT, "looper", 14, 14));

            add(new Token(TokenType.TPRLN, null, 16, 5));
            add(new Token(TokenType.TIDNT, "sum", 16, 15));
            add(new Token(TokenType.TSEMI, null, 16, 19));
            add(new Token(TokenType.TENDK, null, 17, 1));
            add(new Token(TokenType.TPROG, null, 17, 5));
            add(new Token(TokenType.TIDNT, "addToN", 17, 13));
            add(new Token(TokenType.TEOF, null, 18, 1));
        }};

        Scanner sc = new Scanner(inputs);
        int i = 0;
        while(!sc.isFinished()) {
            Token tok = sc.nextValidToken();

            if (tok != null) {
                Assert.assertEquals(tokens.get(i).getType(), tok.getType());
                Assert.assertEquals(tokens.get(i).getLexeme(), tok.getLexeme());
                Assert.assertEquals(tokens.get(i).getLine(), tok.getLine());
                Assert.assertEquals(tokens.get(i).getColumn(), tok.getColumn());
                i++;
            }
        }
    }

    @Test
    public void testMultiplesPass() throws Exception {
        String inputs = "+/+\n" +
                "  This program gets a number from the user and prints the\n" +
                "  times table of the number from 0 to 12\n" +
                "+/+\n" +
                "program multiples\n" +
                "\n" +
                "    local base, counter ;\n" +
                "    input base ;\n" +
                "\n" +
                "    loop multiply\n" +
                "        exit multiply when counter > 12;\n" +
                "        printline base, \" x \", counter, \" = \", base * counter;\n" +
                "        counter += 1 ;\n" +
                "    end loop multiply\n" +
                "\n" +
                "end program multiples\n";

        List<Token> tokens = new LinkedList<Token>() {{
            add(new Token(TokenType.TPROG, null, 5, 1));
            add(new Token(TokenType.TIDNT, "multiples", 5, 9));
            add(new Token(TokenType.TLOCL, null, 7, 5));
            add(new Token(TokenType.TIDNT, "base", 7, 11));
            add(new Token(TokenType.TCOMA, null, 7, 15));
            add(new Token(TokenType.TIDNT, "counter", 7, 17));
            add(new Token(TokenType.TSEMI, null, 7, 25));
            add(new Token(TokenType.TINPT, null, 8, 5));
            add(new Token(TokenType.TIDNT, "base", 8, 11));
            add(new Token(TokenType.TSEMI, null, 8, 16));

            add(new Token(TokenType.TLOOP, null, 10, 5));
            add(new Token(TokenType.TIDNT, "multiply", 10, 10));

            add(new Token(TokenType.TEXIT, null, 11, 9));
            add(new Token(TokenType.TIDNT, "multiply", 11, 14));
            add(new Token(TokenType.TWHEN, null, 11, 23));
            add(new Token(TokenType.TIDNT, "counter", 11, 28));
            add(new Token(TokenType.TGRTR, null, 11, 36));
            add(new Token(TokenType.TILIT, "12", 11, 38));
            add(new Token(TokenType.TSEMI, null, 11, 40));

            add(new Token(TokenType.TPRLN, null, 12, 9));
            add(new Token(TokenType.TIDNT, "base", 12, 19));
            add(new Token(TokenType.TCOMA, null, 12, 23));
            add(new Token(TokenType.TSTRG, " x ", 12, 25));
            add(new Token(TokenType.TCOMA, null, 12, 30));
            add(new Token(TokenType.TIDNT, "counter", 12, 32));
            add(new Token(TokenType.TCOMA, null, 12, 39));
            add(new Token(TokenType.TSTRG, " = ", 12, 41));
            add(new Token(TokenType.TCOMA, null, 12, 46));
            add(new Token(TokenType.TIDNT, "base", 12, 48));
            add(new Token(TokenType.TMULT, null, 12, 53));
            add(new Token(TokenType.TIDNT, "counter", 12, 55));
            add(new Token(TokenType.TSEMI, null, 12, 62));

            add(new Token(TokenType.TIDNT, "counter", 13, 9));
            add(new Token(TokenType.TPLEQ, null, 13, 17));
            add(new Token(TokenType.TILIT, "1", 13, 20));
            add(new Token(TokenType.TSEMI, null, 13, 22));

            add(new Token(TokenType.TENDK, null, 14, 5));
            add(new Token(TokenType.TLOOP, null, 14, 9));
            add(new Token(TokenType.TIDNT, "multiply", 14, 14));

            add(new Token(TokenType.TENDK, null, 16, 1));
            add(new Token(TokenType.TPROG, null, 16, 5));
            add(new Token(TokenType.TIDNT, "multiples", 16, 13));
            add(new Token(TokenType.TEOF, null, 17, 1));
        }};

        Scanner sc = new Scanner(inputs);
        int i = 0;
        while(!sc.isFinished()) {
            Token tok = sc.nextValidToken();

            if (tok != null) {
                Assert.assertEquals(tokens.get(i).getType(), tok.getType());
                Assert.assertEquals(tokens.get(i).getLexeme(), tok.getLexeme());
                Assert.assertEquals(tokens.get(i).getLine(), tok.getLine());
                Assert.assertEquals(tokens.get(i).getColumn(), tok.getColumn());
                i++;
            }
        }
    }

    @Test
    public void testMaxPass() throws Exception {
        String inputs = "+/+\n" +
                "  This program calculates the maximum value out of a pair of\n" +
                "  given numbers\n" +
                "+/+\n" +
                "\n" +
                "program max\n" +
                "\n" +
                "    proc maxVal var a, b\n" +
                "        if a > b then\n" +
                "            printline a, \" is greatest\";\n" +
                "        elsif a < b then\n" +
                "            printline b, \" is greatest\";\n" +
                "        else\n" +
                "            printline \"Values are same\";\n" +
                "        end if\n" +
                "    end proc maxVal\n" +
                "\n" +
                "    local x, y ;\n" +
                "\n" +
                "    x = 1 ;\n" +
                "    y = 3 ;\n" +
                "\n" +
                "    call maxVal with x, y;\n" +
                "\n" +
                "end program max\n";

        List<Token> tokens = new LinkedList<Token>() {{
            add(new Token(TokenType.TPROG, null, 6, 1));
            add(new Token(TokenType.TIDNT, "max", 6, 9));

            add(new Token(TokenType.TPROC, null, 8, 5));
            add(new Token(TokenType.TIDNT, "maxVal", 8, 10));
            add(new Token(TokenType.TVARP, null, 8, 17));
            add(new Token(TokenType.TIDNT, "a", 8, 21));
            add(new Token(TokenType.TCOMA, null, 8, 22));
            add(new Token(TokenType.TIDNT, "b", 8, 24));

            add(new Token(TokenType.TIFKW, null, 9, 9));
            add(new Token(TokenType.TIDNT, "a", 9, 12));
            add(new Token(TokenType.TGRTR, null, 9, 14));
            add(new Token(TokenType.TIDNT, "b", 9, 16));
            add(new Token(TokenType.TTHEN, null, 9, 18));
            add(new Token(TokenType.TPRLN, null, 10, 13));
            add(new Token(TokenType.TIDNT, "a", 10, 23));
            add(new Token(TokenType.TCOMA, null, 10, 24));
            add(new Token(TokenType.TSTRG, " is greatest", 10, 26));
            add(new Token(TokenType.TSEMI, null, 10, 40));

            add(new Token(TokenType.TELSF, null, 11, 9));
            add(new Token(TokenType.TIDNT, "a", 11, 15));
            add(new Token(TokenType.TLESS, null, 11, 17));
            add(new Token(TokenType.TIDNT, "b", 11, 19));
            add(new Token(TokenType.TTHEN, null, 11, 21));
            add(new Token(TokenType.TPRLN, null, 12, 13));
            add(new Token(TokenType.TIDNT, "b", 12, 23));
            add(new Token(TokenType.TCOMA, null, 12, 24));
            add(new Token(TokenType.TSTRG, " is greatest", 12, 26));
            add(new Token(TokenType.TSEMI, null, 12, 40));

            add(new Token(TokenType.TELSE, null, 13, 9));
            add(new Token(TokenType.TPRLN, null, 14, 13));
            add(new Token(TokenType.TSTRG, "Values are same", 14, 23));
            add(new Token(TokenType.TSEMI, null, 14, 40));
            add(new Token(TokenType.TENDK, null, 15, 9));
            add(new Token(TokenType.TIFKW, null, 15, 13));
            add(new Token(TokenType.TENDK, null, 16, 5));
            add(new Token(TokenType.TPROC, null, 16, 9));
            add(new Token(TokenType.TIDNT, "maxVal", 16, 14));

            add(new Token(TokenType.TLOCL, null, 18, 5));
            add(new Token(TokenType.TIDNT, "x", 18, 11));
            add(new Token(TokenType.TCOMA, null, 18, 12));
            add(new Token(TokenType.TIDNT, "y", 18, 14));
            add(new Token(TokenType.TSEMI, null, 18, 16));

            add(new Token(TokenType.TIDNT, "x", 20, 5));
            add(new Token(TokenType.TASGN, null, 20, 7));
            add(new Token(TokenType.TILIT, "1", 20, 9));
            add(new Token(TokenType.TSEMI, null, 20, 11));

            add(new Token(TokenType.TIDNT, "y", 21, 5));
            add(new Token(TokenType.TASGN, null, 21, 7));
            add(new Token(TokenType.TILIT, "3", 21, 9));
            add(new Token(TokenType.TSEMI, null, 21, 11));

            add(new Token(TokenType.TCALL, null, 23, 5));
            add(new Token(TokenType.TIDNT, "maxVal", 23, 10));
            add(new Token(TokenType.TWITH, null, 23, 17));
            add(new Token(TokenType.TIDNT, "x", 23, 22));
            add(new Token(TokenType.TCOMA, null, 23, 23));
            add(new Token(TokenType.TIDNT, "y", 23, 25));
            add(new Token(TokenType.TSEMI, null, 23, 26));

            add(new Token(TokenType.TENDK, null, 25, 1));
            add(new Token(TokenType.TPROG, null, 25, 5));
            add(new Token(TokenType.TIDNT, "max", 25, 13));
            add(new Token(TokenType.TEOF, null, 26, 1));
        }};

        Scanner sc = new Scanner(inputs);
        int i = 0;
        while(!sc.isFinished()) {
            Token tok = sc.nextValidToken();

            if (tok != null) {
                Assert.assertEquals(tokens.get(i).getType(), tok.getType());
                Assert.assertEquals(tokens.get(i).getLexeme(), tok.getLexeme());
                Assert.assertEquals(tokens.get(i).getLine(), tok.getLine());
                Assert.assertEquals(tokens.get(i).getColumn(), tok.getColumn());
                i++;
            }
        }
    }

    @Test
    public void testArrayPass() throws Exception {
        String inputs = "+/+\n" +
                "  This program gets input from the user and stores It in an\n" +
                "  array. The result is then printed from the array\n" +
                "+/+\n" +
                "\n" +
                "program arrayProg\n" +
                "    arrays mArr [1] ;\n" +
                "    local uInput ;\n" +
                "\n" +
                "    print \"Input a number: \" ;\n" +
                "    input uInput ;\n" +
                "\n" +
                "    mArr[0] = uInput ;\n" +
                "    printline mArr[0] ;\n" +
                "\n" +
                "end program arrayProg\n";

        List<Token> tokens = new LinkedList<Token>() {{
            add(new Token(TokenType.TPROG, null, 6, 1));
            add(new Token(TokenType.TIDNT, "arrayProg", 6, 9));

            add(new Token(TokenType.TARRS, null, 7, 5));
            add(new Token(TokenType.TIDNT, "mArr", 7, 12));
            add(new Token(TokenType.TLBRK, null, 7, 17));
            add(new Token(TokenType.TILIT, "1", 7, 18));
            add(new Token(TokenType.TRBRK, null, 7, 19));
            add(new Token(TokenType.TSEMI, null, 7, 21));

            add(new Token(TokenType.TLOCL, null, 8, 5));
            add(new Token(TokenType.TIDNT, "uInput", 8, 11));
            add(new Token(TokenType.TSEMI, null, 8, 18));

            add(new Token(TokenType.TPRIN, null, 10, 5));
            add(new Token(TokenType.TSTRG, "Input a number: ", 10, 11));
            add(new Token(TokenType.TSEMI, null, 10, 30));

            add(new Token(TokenType.TINPT, null, 11, 5));
            add(new Token(TokenType.TIDNT, "uInput", 11, 11));
            add(new Token(TokenType.TSEMI, null, 11, 18));

            add(new Token(TokenType.TIDNT, "mArr", 13, 5));
            add(new Token(TokenType.TLBRK, null, 13, 9));
            add(new Token(TokenType.TILIT, "0", 13, 10));
            add(new Token(TokenType.TRBRK, null, 13, 11));
            add(new Token(TokenType.TASGN, null, 13, 13));
            add(new Token(TokenType.TIDNT, "uInput", 13, 15));
            add(new Token(TokenType.TSEMI, null, 13, 22));

            add(new Token(TokenType.TPRLN, null, 14, 5));
            add(new Token(TokenType.TIDNT, "mArr", 14, 15));
            add(new Token(TokenType.TLBRK, null, 14, 19));
            add(new Token(TokenType.TILIT, "0", 14, 20));
            add(new Token(TokenType.TRBRK, null, 14, 21));
            add(new Token(TokenType.TSEMI, null, 14, 23));

            add(new Token(TokenType.TENDK, null, 16, 1));
            add(new Token(TokenType.TPROG, null, 16, 5));
            add(new Token(TokenType.TIDNT, "arrayProg", 16, 13));
            add(new Token(TokenType.TEOF, null, 17, 1));
        }};

        Scanner sc = new Scanner(inputs);
        int i = 0;
        while(!sc.isFinished()) {
            Token tok = sc.nextValidToken();

            if (tok != null) {
                Assert.assertEquals(tokens.get(i).getType(), tok.getType());
                Assert.assertEquals(tokens.get(i).getLexeme(), tok.getLexeme());
                Assert.assertEquals(tokens.get(i).getLine(), tok.getLine());
                Assert.assertEquals(tokens.get(i).getColumn(), tok.getColumn());
                i++;
            }
        }
    }
}