package CD15.scanner;

/**
 * Defines the ASCII grouping to which valid ASCII characters can belong
 *
 * @author Tyler Haigh - C3182929
 * @since 2/08/2015
 */
public enum StringClassification {
    String,
    Numeric,
    Punctuation,
    NonPrintable,
    Space,
    Unknown,
    NewLine
}
