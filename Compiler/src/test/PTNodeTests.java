package test;

import CD15.entity.Token;
import CD15.entity.TokenType;
import CD15.parser.NodeDataType;
import CD15.parser.ParseTreeNodeType;
import CD15.parser.Parser;
import CD15.parser.TreeNode;
import CD15.parser.nodes.*;
import CD15.scanner.Scanner;
import junit.framework.Assert;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

/**
 * InputController Tester.
 *
 * @author <Authors name>
 * @since <pre>Aug 15, 2015</pre>
 * @version 1.0
 */
public class PTNodeTests {

    @Test
    public void testParseProgram() throws Exception {
        String input = "program test end program test";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NProgNode.make(p);

        p.pushScope("prog");
        TreeNode prog = new TreeNode(ParseTreeNodeType.NPROG, p.lookupIdentifier("test"));
        TreeNode main = new TreeNode(ParseTreeNodeType.NUNDEF);

        prog.setRight(main);
        prog.setDataType(NodeDataType.Program);

        Assert.assertTrue(prog.equals(actual));
    }

    @Test
    public void testParseArrayList() throws Exception {
        String input = "arrays x[1], y[2], z[3]";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NArrlNode.make(p);

        TreeNode arrayList1 = new TreeNode(ParseTreeNodeType.NARRYL);
        TreeNode arrayList2 = new TreeNode(ParseTreeNodeType.NARRYL);
        TreeNode arrayDecX = new TreeNode(ParseTreeNodeType.NARRDEC);
        TreeNode arrayDecY = new TreeNode(ParseTreeNodeType.NARRDEC);
        TreeNode arrayDecZ = new TreeNode(ParseTreeNodeType.NARRDEC);

        TreeNode one = new TreeNode(ParseTreeNodeType.NILIT);
        TreeNode two= new TreeNode(ParseTreeNodeType.NILIT);
        TreeNode three = new TreeNode(ParseTreeNodeType.NILIT);

        arrayList1.setLeft(arrayDecX);
        arrayList1.setRight(arrayList2);
        arrayList2.setLeft(arrayDecY);
        arrayList2.setRight(arrayDecZ);

        arrayList1.setDataType(NodeDataType.Node);
        arrayList2.setDataType(NodeDataType.Node);

        arrayDecX.setIdentifierRecord(p.lookupIdentifier("x"));
        arrayDecY.setIdentifierRecord(p.lookupIdentifier("y"));
        arrayDecZ.setIdentifierRecord(p.lookupIdentifier("z"));
        arrayDecX.setLeft(one);
        arrayDecY.setLeft(two);
        arrayDecZ.setLeft(three);
        arrayDecX.setDataType(NodeDataType.FloatArray);
        arrayDecY.setDataType(NodeDataType.FloatArray);
        arrayDecZ.setDataType(NodeDataType.FloatArray);

        one.setConstRecord(p.lookupConstant("1"));
        two.setConstRecord(p.lookupConstant("2"));
        three.setConstRecord(p.lookupConstant("3"));
        one.setDataType(NodeDataType.Float);
        two.setDataType(NodeDataType.Float);
        three.setDataType(NodeDataType.Float);

        Assert.assertTrue(arrayList1.equals(actual));
    }

    @Test
    public void testParseArrayDec() throws Exception {
        String input = "x[1]";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NArrDecNode.make(p);

        TreeNode arrayDecX = new TreeNode(ParseTreeNodeType.NARRDEC);
        TreeNode one = new TreeNode(ParseTreeNodeType.NILIT);

        arrayDecX.setIdentifierRecord(p.lookupIdentifier("x"));
        arrayDecX.setLeft(one);
        arrayDecX.setDataType(NodeDataType.FloatArray);

        one.setConstRecord(p.lookupConstant("1"));
        one.setDataType(NodeDataType.Float);

        Assert.assertTrue(arrayDecX.equals(actual));
    }

    @Test
    public void testParseProc() throws Exception {
        String input = "proc max\n" +
                "input x ;\n" +
                "end proc max";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NProcNode.make(p);

        TreeNode proc = new TreeNode(ParseTreeNodeType.NPROC, p.lookupIdentifier("max"));
        TreeNode inputNode = new TreeNode(ParseTreeNodeType.NINPUT);

        proc.setDataType(NodeDataType.Procedure);
        inputNode.setDataType(NodeDataType.Node);

        p.pushScope("#invalid");
        TreeNode x = new TreeNode(ParseTreeNodeType.NSIMVAR, p.lookupIdentifier("x"));

        proc.setRight(inputNode);
        inputNode.setLeft(x);

        Assert.assertTrue(proc.equals(actual));
    }

    @Test
    public void testParseProcList() throws Exception {
        String input =
                "proc one\n" +
                "    input x ;\n" +
                "end proc one\n" +
                "proc two\n" +
                "    input y ;\n" +
                "end proc two\n" +
                "proc three\n" +
                "    input z ;\n" +
                "end proc three";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NProclNode.make(p);

        TreeNode procList1 = new TreeNode(ParseTreeNodeType.NPROCL);
        TreeNode procList2 = new TreeNode(ParseTreeNodeType.NPROCL);

        TreeNode inputNode1 = new TreeNode(ParseTreeNodeType.NINPUT);
        TreeNode inputNode2 = new TreeNode(ParseTreeNodeType.NINPUT);
        TreeNode inputNode3 = new TreeNode(ParseTreeNodeType.NINPUT);

        // Scope variables
        TreeNode proc1 = new TreeNode(ParseTreeNodeType.NPROC, p.lookupIdentifier("one"));
        TreeNode proc2 = new TreeNode(ParseTreeNodeType.NPROC, p.lookupIdentifier("two"));
        TreeNode proc3 = new TreeNode(ParseTreeNodeType.NPROC, p.lookupIdentifier("three"));

        proc1.setDataType(NodeDataType.Procedure);
        proc2.setDataType(NodeDataType.Procedure);
        proc3.setDataType(NodeDataType.Procedure);
        inputNode1.setDataType(NodeDataType.Node);
        inputNode2.setDataType(NodeDataType.Node);
        inputNode3.setDataType(NodeDataType.Node);

        p.pushScope("#invalid");
        TreeNode x = new TreeNode(ParseTreeNodeType.NSIMVAR, p.lookupIdentifier("x"));
        TreeNode y = new TreeNode(ParseTreeNodeType.NSIMVAR, p.lookupIdentifier("y"));
        TreeNode z = new TreeNode(ParseTreeNodeType.NSIMVAR, p.lookupIdentifier("z"));

        procList1.setLeft(proc1);
        procList1.setRight(procList2);
        procList2.setLeft(proc2);
        procList2.setRight(proc3);

        procList1.setDataType(NodeDataType.Node);
        procList2.setDataType(NodeDataType.Node);

        proc1.setRight(inputNode1);
        inputNode1.setLeft(x);
        proc2.setRight(inputNode2);
        inputNode2.setLeft(y);
        proc3.setRight(inputNode3);
        inputNode3.setLeft(z);

        System.out.println(actual.printTree());
        Assert.assertTrue(procList1.equals(actual));
    }

    @Test
    public void testParseMain() throws Exception {
        String input = "local x,y,z;\n" +
                "input x,y,z ;";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NMainNode.make(p);

        TreeNode main = new TreeNode(ParseTreeNodeType.NMAIN);
        TreeNode idList1 = new TreeNode(ParseTreeNodeType.NIDLST);
        TreeNode idList2 = new TreeNode(ParseTreeNodeType.NIDLST);
        TreeNode idX = new TreeNode(ParseTreeNodeType.NSIMDEC, p.lookupIdentifier("x"));
        TreeNode idY = new TreeNode(ParseTreeNodeType.NSIMDEC, p.lookupIdentifier("y"));
        TreeNode idZ = new TreeNode(ParseTreeNodeType.NSIMDEC, p.lookupIdentifier("z"));

        TreeNode inputNode = new TreeNode(ParseTreeNodeType.NINPUT);
        TreeNode vList1 = new TreeNode(ParseTreeNodeType.NVLIST);
        TreeNode vList2 = new TreeNode(ParseTreeNodeType.NVLIST);
        TreeNode x = new TreeNode(ParseTreeNodeType.NSIMVAR, p.lookupIdentifier("x"));
        TreeNode y = new TreeNode(ParseTreeNodeType.NSIMVAR, p.lookupIdentifier("y"));
        TreeNode z = new TreeNode(ParseTreeNodeType.NSIMVAR, p.lookupIdentifier("z"));

        main.setLeft(idList1);
        idList1.setLeft(idX);
        idList1.setRight(idList2);
        idList2.setLeft(idY);
        idList2.setRight(idZ);

        main.setRight(inputNode);
        inputNode.setLeft(vList1);
        vList1.setLeft(x);
        vList1.setRight(vList2);
        vList2.setLeft(y);
        vList2.setRight(z);

        idList1.setDataType(NodeDataType.Node);
        idList2.setDataType(NodeDataType.Node);
        inputNode.setDataType(NodeDataType.Node);
        main.setDataType(NodeDataType.Node);
        idX.setDataType(NodeDataType.Float);
        idY.setDataType(NodeDataType.Float);
        idZ.setDataType(NodeDataType.Float);
        x.setDataType(NodeDataType.Float);
        y.setDataType(NodeDataType.Float);
        z.setDataType(NodeDataType.Float);
        vList1.setDataType(NodeDataType.Node);
        vList2.setDataType(NodeDataType.Node);

        System.out.println(actual.printTree());
        Assert.assertTrue(main.equals(actual));
    }

    @Test
    public void testParseParameters() throws Exception {
        String input = "var a, mArr[], b val x,y,z";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = SParamsNode.make(p);

        TreeNode params = new TreeNode(ParseTreeNodeType.NPARAMS);

        TreeNode pList1 = new TreeNode(ParseTreeNodeType.NPLIST);
        TreeNode pList2 = new TreeNode(ParseTreeNodeType.NPLIST);
        TreeNode a = new TreeNode(ParseTreeNodeType.NSIMPAR, p.lookupIdentifier("a"));
        TreeNode mArr = new TreeNode(ParseTreeNodeType.NARRPAR, p.lookupIdentifier("mArr"));
        TreeNode b = new TreeNode(ParseTreeNodeType.NSIMPAR, p.lookupIdentifier("b"));

        TreeNode idList1 = new TreeNode(ParseTreeNodeType.NPLIST);
        TreeNode idList2 = new TreeNode(ParseTreeNodeType.NPLIST);
        TreeNode x = new TreeNode(ParseTreeNodeType.NSIMPAR, p.lookupIdentifier("x"));
        TreeNode y = new TreeNode(ParseTreeNodeType.NSIMPAR, p.lookupIdentifier("y"));
        TreeNode z = new TreeNode(ParseTreeNodeType.NSIMPAR, p.lookupIdentifier("z"));

        params.setLeft(pList1);
        params.setRight(idList1);
        params.setDataType(NodeDataType.Node);

        pList1.setLeft(a);
        pList1.setRight(pList2);
        pList2.setLeft(mArr);
        pList2.setRight(b);

        pList1.setDataType(NodeDataType.Node);
        pList2.setDataType(NodeDataType.Node);
        a.setDataType(NodeDataType.Float);
        mArr.setDataType(NodeDataType.FloatArray);
        b.setDataType(NodeDataType.Float);

        idList1.setLeft(x);
        idList1.setRight(idList2);
        idList2.setLeft(y);
        idList2.setRight(z);

        idList1.setDataType(NodeDataType.Node);
        idList2.setDataType(NodeDataType.Node);
        x.setDataType(NodeDataType.Float);
        y.setDataType(NodeDataType.Float);
        z.setDataType(NodeDataType.Float);

        Assert.assertTrue(params.equals(actual));
    }

    @Test
    public void testParsePList() throws Exception {
        String input = "a, mArr[], b";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NPListNode.make(p);

        TreeNode pList1 = new TreeNode(ParseTreeNodeType.NPLIST);
        TreeNode pList2 = new TreeNode(ParseTreeNodeType.NPLIST);
        TreeNode a = new TreeNode(ParseTreeNodeType.NSIMPAR, p.lookupIdentifier("a"));
        TreeNode mArr = new TreeNode(ParseTreeNodeType.NARRPAR, p.lookupIdentifier("mArr"));
        TreeNode b = new TreeNode(ParseTreeNodeType.NSIMPAR, p.lookupIdentifier("b"));

        pList1.setLeft(a);
        pList1.setRight(pList2);
        pList2.setLeft(mArr);
        pList2.setRight(b);

        pList1.setDataType(NodeDataType.Node);
        pList2.setDataType(NodeDataType.Node);
        a.setDataType(NodeDataType.Float);
        mArr.setDataType(NodeDataType.FloatArray);
        b.setDataType(NodeDataType.Float);

        Assert.assertTrue(pList1.equals(actual));
    }

    @Test
    public void testParseParamVarSimPar() throws Exception {
        String input = "a";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NPListNode.make(p);

        TreeNode a = new TreeNode(ParseTreeNodeType.NSIMPAR, p.lookupIdentifier("a"));
        a.setDataType(NodeDataType.Float);

        Assert.assertTrue(a.equals(actual));
    }

    @Test
    public void testParseParamVarArrPar() throws Exception {
        String input = "a[]";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NPListNode.make(p);

        TreeNode a = new TreeNode(ParseTreeNodeType.NARRPAR, p.lookupIdentifier("a"));
        a.setDataType(NodeDataType.FloatArray);

        Assert.assertTrue(a.equals(actual));
    }

    @Test
    public void testParseParamValSimPar() throws Exception {
        String input = "a";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NPIdListNode.make(p);

        TreeNode a = new TreeNode(ParseTreeNodeType.NSIMPAR, p.lookupIdentifier("a"));
        a.setDataType(NodeDataType.Float);

        Assert.assertTrue(a.equals(actual));
    }

    @Test
    public void testParsePIdList() throws Exception {
        String input = "x,y,z";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NPIdListNode.make(p);

        TreeNode idList1 = new TreeNode(ParseTreeNodeType.NPLIST);
        TreeNode idList2 = new TreeNode(ParseTreeNodeType.NPLIST);
        TreeNode x = new TreeNode(ParseTreeNodeType.NSIMPAR, p.lookupIdentifier("x"));
        TreeNode y = new TreeNode(ParseTreeNodeType.NSIMPAR, p.lookupIdentifier("y"));
        TreeNode z = new TreeNode(ParseTreeNodeType.NSIMPAR, p.lookupIdentifier("z"));

        idList1.setLeft(x);
        idList1.setRight(idList2);
        idList2.setLeft(y);
        idList2.setRight(z);

        idList1.setDataType(NodeDataType.Node);
        idList2.setDataType(NodeDataType.Node);
        x.setDataType(NodeDataType.Float);
        y.setDataType(NodeDataType.Float);
        z.setDataType(NodeDataType.Float);

        Assert.assertTrue(idList1.equals(actual));
    }

    @Test
    public void testParseDeclList() throws Exception {
        String input = "a, mArr[1], b";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NDeclListNode.make(p);

        TreeNode pList1 = new TreeNode(ParseTreeNodeType.NDLIST);
        TreeNode pList2 = new TreeNode(ParseTreeNodeType.NDLIST);
        TreeNode a = new TreeNode(ParseTreeNodeType.NSIMDEC, p.lookupIdentifier("a"));
        TreeNode mArr = new TreeNode(ParseTreeNodeType.NARRDEC, p.lookupIdentifier("mArr"));
        TreeNode b = new TreeNode(ParseTreeNodeType.NSIMDEC, p.lookupIdentifier("b"));
        TreeNode one = new TreeNode(ParseTreeNodeType.NILIT);

        mArr.setLeft(one);
        one.setConstRecord(p.lookupConstant("1"));

        pList1.setLeft(a);
        pList1.setRight(pList2);
        pList2.setLeft(mArr);
        pList2.setRight(b);

        pList1.setDataType(NodeDataType.Node);
        pList2.setDataType(NodeDataType.Node);
        a.setDataType(NodeDataType.Float);
        mArr.setDataType(NodeDataType.FloatArray);
        b.setDataType(NodeDataType.Float);
        one.setDataType(NodeDataType.Float);

        Assert.assertTrue(pList1.equals(actual));
    }

    @Test
    public void testParseSimDec() throws Exception {
        String input = "a";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NDeclListNode.make(p);

        TreeNode a = new TreeNode(ParseTreeNodeType.NSIMDEC, p.lookupIdentifier("a"));
        a.setDataType(NodeDataType.Float);

        Assert.assertTrue(a.equals(actual));
    }

    @Test
    public void testParseArrDec() throws Exception {
        String input = "a[1]";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NDeclListNode.make(p);

        TreeNode a = new TreeNode(ParseTreeNodeType.NARRDEC, p.lookupIdentifier("a"));
        TreeNode one = new TreeNode(ParseTreeNodeType.NILIT);

        a.setLeft(one);
        a.setDataType(NodeDataType.FloatArray);
        one.setConstRecord(p.lookupConstant("1"));
        one.setDataType(NodeDataType.Float);

        Assert.assertTrue(a.equals(actual));
    }

    @Test
    public void testParseProcLocals() throws Exception {
        String input = "local a,b[1],c ;";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = SLocalsNode.make(p);

        TreeNode locals = new TreeNode(ParseTreeNodeType.NDLIST);
        TreeNode idList = new TreeNode(ParseTreeNodeType.NDLIST);
        TreeNode a = new TreeNode(ParseTreeNodeType.NSIMDEC);
        TreeNode b = new TreeNode(ParseTreeNodeType.NARRDEC);
        TreeNode c = new TreeNode(ParseTreeNodeType.NSIMDEC);
        TreeNode one = new TreeNode(ParseTreeNodeType.NILIT);

        locals.setLeft(a);
        locals.setRight(idList);
        idList.setLeft(b);
        idList.setRight(c);

        a.setIdentifierRecord(p.lookupIdentifier("a"));
        b.setIdentifierRecord(p.lookupIdentifier("b"));
        c.setIdentifierRecord(p.lookupIdentifier("c"));
        one.setConstRecord(p.lookupConstant("1"));
        b.setLeft(one);

        locals.setDataType(NodeDataType.Node);
        idList.setDataType(NodeDataType.Node);
        a.setDataType(NodeDataType.Float);
        b.setDataType(NodeDataType.FloatArray);
        c.setDataType(NodeDataType.Float);
        one.setDataType(NodeDataType.Float);

        Assert.assertTrue(locals.equals(actual));
    }

    @Test
    public void testParseStatList() throws Exception {
        String input = "input retMsg;\n" +
                "printline retMsg;\n" +
                "print retMsg;\n";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = SStatsNode.make(p);

        TreeNode statList1 = new TreeNode(ParseTreeNodeType.NSLIST);
        TreeNode statList2 = new TreeNode(ParseTreeNodeType.NSLIST);

        TreeNode inputNode = new TreeNode(ParseTreeNodeType.NINPUT);
        TreeNode printline = new TreeNode(ParseTreeNodeType.NPRLN);
        TreeNode print = new TreeNode(ParseTreeNodeType.NPRINT);

        p.pushScope("#invalid");
        TreeNode retMsg = new TreeNode(ParseTreeNodeType.NSIMVAR, p.lookupIdentifier("retMsg"));

        statList1.setLeft(inputNode);
        statList1.setRight(statList2);
        statList2.setLeft(printline);
        statList2.setRight(print);

        inputNode.setLeft(retMsg);
        printline.setLeft(retMsg);
        print.setLeft(retMsg);

        statList1.setDataType(NodeDataType.Node);
        statList2.setDataType(NodeDataType.Node);
        inputNode.setDataType(NodeDataType.Node);
        printline.setDataType(NodeDataType.Node);
        print.setDataType(NodeDataType.Node);

        System.out.println(actual.printTree());
        Assert.assertTrue(statList1.equals(actual));
    }

    @Test
    public void testParseLoopStat() throws Exception {
        String input = "loop mainLoop end loop mainLoop";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = SStatsNode.make(p);

        TreeNode loop = new TreeNode(ParseTreeNodeType.NLOOP);
        TreeNode stats = new TreeNode(ParseTreeNodeType.NUNDEF);

        loop.setLeft(stats);
        loop.setIdentifierRecord(p.lookupIdentifier("mainLoop"));
        loop.setDataType(NodeDataType.Undefined);
        loop.getIdentifierRecord().setTreeNode(loop);

        Assert.assertTrue(loop.equals(actual));
    }

    @Test
    public void testParseExitStat() throws Exception {
        String input = "exit mLoop when x<1;";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = SStatsNode.make(p);

        p.pushScope("#invalid");
        TreeNode exit = new TreeNode(ParseTreeNodeType.NEXIT, p.lookupIdentifier("mLoop"));
        TreeNode bool = new TreeNode(ParseTreeNodeType.NLESS);
        TreeNode x = new TreeNode(ParseTreeNodeType.NSIMVAR, p.lookupIdentifier("x"));
        TreeNode one = new TreeNode(ParseTreeNodeType.NILIT);
        one.setConstRecord(p.lookupConstant("1"));

        bool.setLeft(x);
        bool.setRight(one);
        exit.setLeft(bool);

        one.setDataType(NodeDataType.Float);

        System.out.println(actual.printTree());
        Assert.assertTrue(exit.equals(actual));
    }

    @Test
    public void testParseIfStat() throws Exception {
        String input = "if x<1 then end if";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = SStatsNode.make(p);

        TreeNode ifNode = new TreeNode(ParseTreeNodeType.NIFT);
        TreeNode bool = new TreeNode(ParseTreeNodeType.NLESS);
        TreeNode x = new TreeNode(ParseTreeNodeType.NSIMVAR);
        TreeNode one = new TreeNode(ParseTreeNodeType.NILIT);
        TreeNode stats = new TreeNode(ParseTreeNodeType.NUNDEF);

        ifNode.setLeft(bool);
        ifNode.setRight(stats);

        bool.setLeft(x);
        bool.setRight(one);

        p.pushScope("#invalid");
        x.setIdentifierRecord(p.lookupIdentifier("x"));
        one.setConstRecord(p.lookupConstant("1"));
        one.setDataType(NodeDataType.Float);

        Assert.assertTrue(ifNode.equals(actual));
    }

    @Test
    public void testParseIfElseStat() throws Exception {
        String input = "if x<1 then else end if";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = SStatsNode.make(p);

        TreeNode ifNode = new TreeNode(ParseTreeNodeType.NIFTE);
        TreeNode bool = new TreeNode(ParseTreeNodeType.NLESS);
        TreeNode x = new TreeNode(ParseTreeNodeType.NSIMVAR);
        TreeNode one = new TreeNode(ParseTreeNodeType.NILIT);
        TreeNode stats1 = new TreeNode(ParseTreeNodeType.NUNDEF);
        TreeNode stats2 = new TreeNode(ParseTreeNodeType.NUNDEF);

        ifNode.setLeft(bool);
        ifNode.setMiddle(stats1);
        ifNode.setRight(stats2);

        bool.setLeft(x);
        bool.setRight(one);

        p.pushScope("#invalid");
        x.setIdentifierRecord(p.lookupIdentifier("x"));
        one.setConstRecord(p.lookupConstant("1"));
        one.setDataType(NodeDataType.Float);

        System.out.println(actual.printTree());
        Assert.assertTrue(ifNode.equals(actual));
    }

    @Test
    public void testParseIfElsIfStat() throws Exception {
        String input = "if x<1 then elsif x<2 then elsif x<3 then else end if";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = SStatsNode.make(p);

        TreeNode ifNode = new TreeNode(ParseTreeNodeType.NIFTE);
        TreeNode elsifNode1 = new TreeNode(ParseTreeNodeType.NIFTE);
        TreeNode elsifNode2 = new TreeNode(ParseTreeNodeType.NIFTE);

        TreeNode bool1 = new TreeNode(ParseTreeNodeType.NLESS);
        TreeNode bool2 = new TreeNode(ParseTreeNodeType.NLESS);
        TreeNode bool3 = new TreeNode(ParseTreeNodeType.NLESS);
        TreeNode x = new TreeNode(ParseTreeNodeType.NSIMVAR);
        TreeNode one = new TreeNode(ParseTreeNodeType.NILIT);
        TreeNode two = new TreeNode(ParseTreeNodeType.NILIT);
        TreeNode three = new TreeNode(ParseTreeNodeType.NILIT);

        TreeNode stats1 = new TreeNode(ParseTreeNodeType.NUNDEF);
        TreeNode stats2 = new TreeNode(ParseTreeNodeType.NUNDEF);
        TreeNode stats3 = new TreeNode(ParseTreeNodeType.NUNDEF);
        TreeNode stats4 = new TreeNode(ParseTreeNodeType.NUNDEF);

        bool1.setLeft(x);
        bool1.setRight(one);

        bool2.setLeft(x);
        bool2.setRight(two);

        bool3.setLeft(x);
        bool3.setRight(three);

        ifNode.setLeft(bool1);
        ifNode.setMiddle(stats1);
        ifNode.setRight(elsifNode1);

        elsifNode1.setLeft(bool2);
        elsifNode1.setMiddle(stats2);
        elsifNode1.setRight(elsifNode2);

        elsifNode2.setLeft(bool3);
        elsifNode2.setMiddle(stats3);
        elsifNode2.setRight(stats4);

        p.pushScope("#invalid");
        x.setIdentifierRecord(p.lookupIdentifier("x"));
        one.setConstRecord(p.lookupConstant("1"));
        two.setConstRecord(p.lookupConstant("2"));
        three.setConstRecord(p.lookupConstant("3"));
        one.setDataType(NodeDataType.Float);
        two.setDataType(NodeDataType.Float);
        three.setDataType(NodeDataType.Float);

        Assert.assertTrue(ifNode.equals(actual));
    }

    @Test
    public void testParseAsgnStat() throws Exception {
        String input = "x = 1 ;";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NAsgnNode.make(p);

        p.pushScope("#invalid");
        TreeNode asgn = new TreeNode(ParseTreeNodeType.NASGN);
        TreeNode x = new TreeNode(ParseTreeNodeType.NSIMVAR, p.lookupIdentifier("x"));
        TreeNode one = new TreeNode(ParseTreeNodeType.NILIT);
        one.setConstRecord(p.lookupConstant("1"));

        asgn.setLeft(x);
        asgn.setRight(one);
        one.setDataType(NodeDataType.Float);

        Assert.assertTrue(asgn.equals(actual));
    }

    @Test
    public void testParsePleqStat() throws Exception {
        String input = "x += 1 ;";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NAsgnNode.make(p);

        p.pushScope("#invalid");
        TreeNode pleq = new TreeNode(ParseTreeNodeType.NPLEQ);
        TreeNode x = new TreeNode(ParseTreeNodeType.NSIMVAR, p.lookupIdentifier("x"));
        TreeNode one = new TreeNode(ParseTreeNodeType.NILIT);
        one.setConstRecord(p.lookupConstant("1"));

        pleq.setLeft(x);
        pleq.setRight(one);
        one.setDataType(NodeDataType.Float);

        Assert.assertTrue(pleq.equals(actual));
    }

    @Test
    public void testParseMneqStat() throws Exception {
        String input = "x -= 1 ;";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NAsgnNode.make(p);

        p.pushScope("#invalid");
        TreeNode mneq = new TreeNode(ParseTreeNodeType.NMNEQ);
        TreeNode x = new TreeNode(ParseTreeNodeType.NSIMVAR, p.lookupIdentifier("x"));
        TreeNode one = new TreeNode(ParseTreeNodeType.NILIT);
        one.setConstRecord(p.lookupConstant("1"));

        mneq.setLeft(x);
        mneq.setRight(one);
        one.setDataType(NodeDataType.Float);

        Assert.assertTrue(mneq.equals(actual));
    }

    @Test
    public void testParseSteqStat() throws Exception {
        String input = "x *= 1 ;";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NAsgnNode.make(p);

        p.pushScope("#invalid");
        TreeNode steq = new TreeNode(ParseTreeNodeType.NSTEQ);
        TreeNode x = new TreeNode(ParseTreeNodeType.NSIMVAR, p.lookupIdentifier("x"));
        TreeNode one = new TreeNode(ParseTreeNodeType.NILIT);
        one.setConstRecord(p.lookupConstant("1"));

        steq.setLeft(x);
        steq.setRight(one);
        one.setDataType(NodeDataType.Float);

        Assert.assertTrue(steq.equals(actual));
    }

    @Test
    public void testParseDveqStat() throws Exception {
        String input = "x /= 1 ;";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NAsgnNode.make(p);

        p.pushScope("#invalid");
        TreeNode dveq = new TreeNode(ParseTreeNodeType.NDVEQ);
        TreeNode x = new TreeNode(ParseTreeNodeType.NSIMVAR, p.lookupIdentifier("x"));
        TreeNode one = new TreeNode(ParseTreeNodeType.NILIT);
        one.setConstRecord(p.lookupConstant("1"));

        dveq.setLeft(x);
        dveq.setRight(one);
        one.setDataType(NodeDataType.Float);

        Assert.assertTrue(dveq.equals(actual));
    }

    @Test
    public void testParseInputStat() throws Exception {
        String input = "input x,y[1],z;";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NStatNode.make(p);

        TreeNode inputNode = new TreeNode(ParseTreeNodeType.NINPUT);
        TreeNode vList1 = new TreeNode(ParseTreeNodeType.NVLIST);
        TreeNode vList2 = new TreeNode(ParseTreeNodeType.NVLIST);

        p.pushScope("#invalid");
        TreeNode x = new TreeNode(ParseTreeNodeType.NSIMVAR, p.lookupIdentifier("x"));
        TreeNode y = new TreeNode(ParseTreeNodeType.NARRVAR, p.lookupIdentifier("y"));
        TreeNode z = new TreeNode(ParseTreeNodeType.NSIMVAR, p.lookupIdentifier("z"));
        TreeNode one = new TreeNode(ParseTreeNodeType.NILIT);
        one.setConstRecord(p.lookupConstant("1"));

        inputNode.setDataType(NodeDataType.Node);
        one.setDataType(NodeDataType.Float);
        vList1.setDataType(NodeDataType.Node);
        vList2.setDataType(NodeDataType.Node);

        y.setLeft(one);
        inputNode.setLeft(vList1);
        vList1.setLeft(x);
        vList1.setRight(vList2);
        vList2.setLeft(y);
        vList2.setRight(z);

        Assert.assertTrue(inputNode.equals(actual));
    }

    @Test
    public void testParsePrintStat() throws Exception {
        String input = "print x,y[1],z;";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NStatNode.make(p);

        TreeNode printNode = new TreeNode(ParseTreeNodeType.NPRINT);
        TreeNode prList1 = new TreeNode(ParseTreeNodeType.NPRLIST);
        TreeNode prList2 = new TreeNode(ParseTreeNodeType.NPRLIST);

        p.pushScope("#invalid");
        TreeNode x = new TreeNode(ParseTreeNodeType.NSIMVAR, p.lookupIdentifier("x"));
        TreeNode y = new TreeNode(ParseTreeNodeType.NARRVAR, p.lookupIdentifier("y"));
        TreeNode z = new TreeNode(ParseTreeNodeType.NSIMVAR, p.lookupIdentifier("z"));
        TreeNode one = new TreeNode(ParseTreeNodeType.NILIT);
        one.setConstRecord(p.lookupConstant("1"));

        y.setLeft(one);

        printNode.setLeft(prList1);
        prList1.setLeft(x);
        prList1.setRight(prList2);
        prList2.setLeft(y);
        prList2.setRight(z);

        printNode.setDataType(NodeDataType.Node);
        prList1.setDataType(NodeDataType.Node);
        prList2.setDataType(NodeDataType.Node);
        one.setDataType(NodeDataType.Float);

        Assert.assertTrue(printNode.equals(actual));
    }

    @Test
    public void testParsePrintLineStat() throws Exception {
        String input = "printline x,y[1],z;";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NStatNode.make(p);

        TreeNode printNode = new TreeNode(ParseTreeNodeType.NPRLN);
        TreeNode prList1 = new TreeNode(ParseTreeNodeType.NPRLIST);
        TreeNode prList2 = new TreeNode(ParseTreeNodeType.NPRLIST);

        p.pushScope("#invalid");
        TreeNode x = new TreeNode(ParseTreeNodeType.NSIMVAR, p.lookupIdentifier("x"));
        TreeNode y = new TreeNode(ParseTreeNodeType.NARRVAR, p.lookupIdentifier("y"));
        TreeNode z = new TreeNode(ParseTreeNodeType.NSIMVAR, p.lookupIdentifier("z"));
        TreeNode one = new TreeNode(ParseTreeNodeType.NILIT);
        one.setConstRecord(p.lookupConstant("1"));

        y.setLeft(one);

        printNode.setLeft(prList1);
        prList1.setLeft(x);
        prList1.setRight(prList2);
        prList2.setLeft(y);
        prList2.setRight(z);

        printNode.setDataType(NodeDataType.Node);
        prList1.setDataType(NodeDataType.Node);
        prList2.setDataType(NodeDataType.Node);
        one.setDataType(NodeDataType.Float);

        Assert.assertTrue(printNode.equals(actual));
    }

    @Test
    public void testParsePrintLineEmptyStat() throws Exception {
        String input = "printline;";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NStatNode.make(p);

        TreeNode printNode = new TreeNode(ParseTreeNodeType.NPRLN);
        printNode.setDataType(NodeDataType.Node);

        Assert.assertTrue(printNode.equals(actual));
    }

    @Test
    public void testParsePrintLineStringStat() throws Exception {
        String input = "printline \"Hello World\";";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NStatNode.make(p);

        TreeNode printNode = new TreeNode(ParseTreeNodeType.NPRLN);
        TreeNode string = new TreeNode(ParseTreeNodeType.NSTRG);
        string.setConstRecord(p.lookupConstant("Hello World"));

        printNode.setLeft(string);
        printNode.setDataType(NodeDataType.Node);
        string.setDataType(NodeDataType.String);

        Assert.assertTrue(printNode.equals(actual));
    }

    @Test
    public void testParseCallEmptyStat() throws Exception {
        String input = "call nothing;";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NStatNode.make(p);

        p.pushScope("#invalid");
        TreeNode inputNode = new TreeNode(ParseTreeNodeType.NCALL, p.lookupIdentifier("nothing"));

        Assert.assertTrue(inputNode.equals(actual));
    }

    @Test
    public void testParseCallWithStat() throws Exception {
        String input = "call nothing with x,y[1],z;";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NStatNode.make(p);

        p.pushScope("#invalid");
        TreeNode inputNode = new TreeNode(ParseTreeNodeType.NCALL, p.lookupIdentifier("nothing"));
        TreeNode eList1 = new TreeNode(ParseTreeNodeType.NELIST);
        TreeNode eList2 = new TreeNode(ParseTreeNodeType.NELIST);

        TreeNode x = new TreeNode(ParseTreeNodeType.NSIMVAR, p.lookupIdentifier("x"));
        TreeNode y = new TreeNode(ParseTreeNodeType.NARRVAR, p.lookupIdentifier("y"));
        TreeNode z = new TreeNode(ParseTreeNodeType.NSIMVAR, p.lookupIdentifier("z"));
        TreeNode one = new TreeNode(ParseTreeNodeType.NILIT);

        one.setConstRecord(p.lookupConstant("1"));

        one.setDataType(NodeDataType.Float);
        eList1.setDataType(NodeDataType.Node);
        eList2.setDataType(NodeDataType.Node);

        y.setLeft(one);
        inputNode.setLeft(eList1);
        eList1.setLeft(x);
        eList1.setRight(eList2);
        eList2.setLeft(y);
        eList2.setRight(z);

        System.out.println(actual.printTree());
        Assert.assertTrue(inputNode.equals(actual));
    }

    @Test
    public void testParseVList() throws Exception {
        String input = "a, b[1], c";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NVListNode.make(p);

        p.pushScope("#invalid");
        TreeNode vList1 = new TreeNode(ParseTreeNodeType.NVLIST);
        TreeNode vList2 = new TreeNode(ParseTreeNodeType.NVLIST);
        TreeNode a = new TreeNode(ParseTreeNodeType.NSIMVAR, p.lookupIdentifier("a"));
        TreeNode b = new TreeNode(ParseTreeNodeType.NARRVAR, p.lookupIdentifier("b"));
        TreeNode c = new TreeNode(ParseTreeNodeType.NSIMVAR, p.lookupIdentifier("c"));
        TreeNode one = new TreeNode(ParseTreeNodeType.NILIT);

        one.setConstRecord(p.lookupConstant("1"));

        one.setDataType(NodeDataType.Float);
        vList1.setDataType(NodeDataType.Node);
        vList2.setDataType(NodeDataType.Node);

        b.setLeft(one);
        vList1.setLeft(a);
        vList1.setRight(vList2);
        vList2.setLeft(b);
        vList2.setRight(c);

        System.out.println(actual.printTree());
        Assert.assertTrue(vList1.equals(actual));
    }

    @Test
    public void testParseArrVar() throws Exception {
        String input = "b[1]";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NVListNode.make(p);

        p.pushScope("#invalid");
        TreeNode b = new TreeNode(ParseTreeNodeType.NARRVAR, p.lookupIdentifier("b"));
        TreeNode one = new TreeNode(ParseTreeNodeType.NILIT);
        one.setConstRecord(p.lookupConstant("1"));

        b.setLeft(one);

        one.setDataType(NodeDataType.Float);

        Assert.assertTrue(b.equals(actual));
    }

    @Test
    public void testParseSimVar() throws Exception {
        String input = "a";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NVListNode.make(p);

        p.pushScope("#invalid");
        TreeNode b = new TreeNode(ParseTreeNodeType.NSIMVAR, p.lookupIdentifier("a"));

        Assert.assertTrue(b.equals(actual));
    }

    @Test
    public void testParseMegaExpression() throws Exception {
        String input = "a + a - a * a / a div b[1] + 1 - 1.1 * b.length / (not a > 1)";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NExprNode.make(p);

        TreeNode add1 = new TreeNode(ParseTreeNodeType.NADD);
        TreeNode add2 = new TreeNode(ParseTreeNodeType.NADD);
        TreeNode minus1 = new TreeNode(ParseTreeNodeType.NSUB);
        TreeNode minus2 = new TreeNode(ParseTreeNodeType.NSUB);
        TreeNode times1 = new TreeNode(ParseTreeNodeType.NMUL);
        TreeNode times2 = new TreeNode(ParseTreeNodeType.NMUL);
        TreeNode divide1 = new TreeNode(ParseTreeNodeType.NDIV);
        TreeNode divide2 = new TreeNode(ParseTreeNodeType.NDIV);
        TreeNode intDiv = new TreeNode(ParseTreeNodeType.NIDIV);
        TreeNode not = new TreeNode(ParseTreeNodeType.NNOT);
        TreeNode greater = new TreeNode(ParseTreeNodeType.NGTR);

        p.pushScope("#invalid");
        TreeNode a = new TreeNode(ParseTreeNodeType.NSIMVAR, p.lookupIdentifier("a"));
        TreeNode b = new TreeNode(ParseTreeNodeType.NARRVAR, p.lookupIdentifier("b"));
        TreeNode one = new TreeNode(ParseTreeNodeType.NILIT);
        TreeNode oneOne = new TreeNode(ParseTreeNodeType.NFLIT);
        TreeNode bLength = new TreeNode(ParseTreeNodeType.NLEN, p.lookupIdentifier("b"));

        one.setConstRecord(p.lookupConstant("1"));
        oneOne.setConstRecord(p.lookupConstant("1.1"));
        one.setDataType(NodeDataType.Float);
        oneOne.setDataType(NodeDataType.Float);
        b.setLeft(one);


        minus2.setLeft(add2);
        minus2.setRight(divide2);

        add2.setLeft(minus1);
        add2.setRight(one);
        minus1.setLeft(add1);
        minus1.setRight(intDiv);
        add1.setLeft(a);
        add1.setRight(a);
        intDiv.setLeft(divide1);
        intDiv.setRight(b);
        divide1.setLeft(times1);
        divide1.setRight(a);
        times1.setLeft(a);
        times1.setRight(a);

        divide2.setLeft(times2);
        divide2.setRight(not);
        times2.setLeft(oneOne);
        times2.setRight(bLength);
        not.setLeft(greater);
        greater.setLeft(a);
        greater.setRight(one);

        Assert.assertTrue(minus2.equals(actual));
    }

    @Test
    public void testParseEList() throws Exception {
        String input = "a + b[1], a - b.length, a * b[a-1]";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NEListNode.make(p);

        TreeNode eList1 = new TreeNode(ParseTreeNodeType.NELIST);
        TreeNode eList2 = new TreeNode(ParseTreeNodeType.NELIST);

        TreeNode add = new TreeNode(ParseTreeNodeType.NADD);
        TreeNode minus = new TreeNode(ParseTreeNodeType.NSUB);
        TreeNode times = new TreeNode(ParseTreeNodeType.NMUL);

        p.pushScope("#invalid");
        TreeNode a = new TreeNode(ParseTreeNodeType.NSIMVAR, p.lookupIdentifier("a"));
        TreeNode b = new TreeNode(ParseTreeNodeType.NARRVAR, p.lookupIdentifier("b"));
        TreeNode bExpr = new TreeNode(ParseTreeNodeType.NARRVAR, p.lookupIdentifier("b"));
        TreeNode one = new TreeNode(ParseTreeNodeType.NILIT);
        TreeNode bLength = new TreeNode(ParseTreeNodeType.NLEN, p.lookupIdentifier("b"));
        TreeNode aSubOne = new TreeNode(ParseTreeNodeType.NSUB);

        one.setConstRecord(p.lookupConstant("1"));

        one.setDataType(NodeDataType.Float);
        eList1.setDataType(NodeDataType.Node);
        eList2.setDataType(NodeDataType.Node);

        b.setLeft(one);
        bExpr.setLeft(aSubOne);
        aSubOne.setLeft(a);
        aSubOne.setRight(one);

        eList1.setLeft(add);
        eList1.setRight(eList2);
        eList2.setLeft(minus);
        eList2.setRight(times);

        add.setLeft(a);
        add.setRight(b);
        minus.setLeft(a);
        minus.setRight(bLength);
        times.setLeft(a);
        times.setRight(bExpr);

        Assert.assertTrue(eList1.equals(actual));
    }

    @Test
     public void testParseAndBool() throws Exception {
        String input = "x == 1 and x != 1";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NBoolNode.make(p);

        p.pushScope("#invalid");
        TreeNode and = new TreeNode(ParseTreeNodeType.NAND);
        TreeNode xEqOne = new TreeNode(ParseTreeNodeType.NEQL);
        TreeNode xNeqOne = new TreeNode(ParseTreeNodeType.NNEQ);
        TreeNode x = new TreeNode(ParseTreeNodeType.NSIMVAR, p.lookupIdentifier("x"));
        TreeNode one = new TreeNode(ParseTreeNodeType.NILIT);

        one.setConstRecord(p.lookupConstant("1"));
        one.setDataType(NodeDataType.Float);

        and.setLeft(xEqOne);
        and.setRight(xNeqOne);

        xEqOne.setLeft(x);
        xEqOne.setRight(one);
        xNeqOne.setLeft(x);
        xNeqOne.setRight(one);

        Assert.assertTrue(and.equals(actual));
    }


    @Test
    public void testParseMultiBool() throws Exception {
        String input = "x == 1 and x == 1 or x == 1 xor x == 1";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NBoolNode.make(p);

        p.pushScope("#invalid");
        TreeNode and = new TreeNode(ParseTreeNodeType.NAND);
        TreeNode or = new TreeNode(ParseTreeNodeType.NOR);
        TreeNode xor = new TreeNode(ParseTreeNodeType.NXOR);

        TreeNode xEqOne = new TreeNode(ParseTreeNodeType.NEQL);
        TreeNode x = new TreeNode(ParseTreeNodeType.NSIMVAR, p.lookupIdentifier("x"));
        TreeNode one = new TreeNode(ParseTreeNodeType.NILIT);

        one.setConstRecord(p.lookupConstant("1"));
        one.setDataType(NodeDataType.Float);

        xor.setLeft(or);
        xor.setRight(xEqOne);
        or.setLeft(and);
        or.setRight(xEqOne);
        and.setLeft(xEqOne);
        and.setRight(xEqOne);

        xEqOne.setLeft(x);
        xEqOne.setRight(one);

        Assert.assertTrue(xor.equals(actual));
    }

    @Test
    public void testParseOrBool() throws Exception {
        String input = "x > 1 or x < 1";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NBoolNode.make(p);

        p.pushScope("#invalid");
        TreeNode or = new TreeNode(ParseTreeNodeType.NOR);
        TreeNode greater = new TreeNode(ParseTreeNodeType.NGTR);
        TreeNode less = new TreeNode(ParseTreeNodeType.NLESS);
        TreeNode x = new TreeNode(ParseTreeNodeType.NSIMVAR, p.lookupIdentifier("x"));
        TreeNode one = new TreeNode(ParseTreeNodeType.NILIT);
        one.setConstRecord(p.lookupConstant("1"));
        one.setDataType(NodeDataType.Float);

        or.setLeft(greater);
        or.setRight(less);

        greater.setLeft(x);
        greater.setRight(one);
        less.setLeft(x);
        less.setRight(one);

        Assert.assertTrue(or.equals(actual));
    }

    @Test
    public void testParseXorBool() throws Exception {
        String input = "x >= 1 xor x <= 1";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NBoolNode.make(p);

        p.pushScope("#invalid");
        TreeNode xor = new TreeNode(ParseTreeNodeType.NXOR);
        TreeNode greaterEq = new TreeNode(ParseTreeNodeType.NGEQ);
        TreeNode lessEq = new TreeNode(ParseTreeNodeType.NLEQ);
        TreeNode x = new TreeNode(ParseTreeNodeType.NSIMVAR, p.lookupIdentifier("x"));
        TreeNode one = new TreeNode(ParseTreeNodeType.NILIT);
        one.setConstRecord(p.lookupConstant("1"));
        one.setDataType(NodeDataType.Float);

        xor.setLeft(greaterEq);
        xor.setRight(lessEq);

        greaterEq.setLeft(x);
        greaterEq.setRight(one);
        lessEq.setLeft(x);
        lessEq.setRight(one);

        Assert.assertTrue(xor.equals(actual));
    }

    @Test
    public void testParseNotBool() throws Exception {
        String input = "not x >= 1";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NBoolNode.make(p);

        p.pushScope("#invalid");
        TreeNode not = new TreeNode(ParseTreeNodeType.NNOT);
        TreeNode greaterEq = new TreeNode(ParseTreeNodeType.NGEQ);
        TreeNode x = new TreeNode(ParseTreeNodeType.NSIMVAR, p.lookupIdentifier("x"));
        TreeNode one = new TreeNode(ParseTreeNodeType.NILIT);
        one.setConstRecord(p.lookupConstant("1"));
        one.setDataType(NodeDataType.Float);

        not.setLeft(greaterEq);
        greaterEq.setLeft(x);
        greaterEq.setRight(one);

        Assert.assertTrue(not.equals(actual));
    }

    @Test
    public void testParseAddExpr() throws Exception {
        String input = "a + b.length";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NExprNode.make(p);

        p.pushScope("#invalid");
        TreeNode add = new TreeNode(ParseTreeNodeType.NADD);
        TreeNode a = new TreeNode(ParseTreeNodeType.NSIMVAR, p.lookupIdentifier("a"));
        TreeNode bLength = new TreeNode(ParseTreeNodeType.NLEN, p.lookupIdentifier("b"));

        add.setLeft(a);
        add.setRight(bLength);

        Assert.assertTrue(add.equals(actual));
    }

    @Test
    public void testParseSubtractExpr() throws Exception {
        String input = "a - b[1]";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NExprNode.make(p);

        p.pushScope("#invalid");
        TreeNode subtract = new TreeNode(ParseTreeNodeType.NSUB);
        TreeNode a = new TreeNode(ParseTreeNodeType.NSIMVAR, p.lookupIdentifier("a"));
        TreeNode b = new TreeNode(ParseTreeNodeType.NARRVAR, p.lookupIdentifier("b"));
        TreeNode one = new TreeNode(ParseTreeNodeType.NILIT);
        one.setConstRecord(p.lookupConstant("1"));
        one.setDataType(NodeDataType.Float);

        b.setLeft(one);
        subtract.setLeft(a);
        subtract.setRight(b);

        a.getIdentifierRecord().setTreeNode(a);
        b.getIdentifierRecord().setTreeNode(b);

        Assert.assertTrue(subtract.equals(actual));
    }

    @Test
    public void testParseSubtractMultipleExpr() throws Exception {
        String input = "10 - 5 - 1*3 - 2/1";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NExprNode.make(p);

        TreeNode subtract1 = new TreeNode(ParseTreeNodeType.NSUB);
        TreeNode subtract2 = new TreeNode(ParseTreeNodeType.NSUB);
        TreeNode subtract3 = new TreeNode(ParseTreeNodeType.NSUB);
        TreeNode times = new TreeNode(ParseTreeNodeType.NMUL);
        TreeNode divide = new TreeNode(ParseTreeNodeType.NDIV);

        TreeNode ten = new TreeNode(ParseTreeNodeType.NILIT);
        TreeNode five = new TreeNode(ParseTreeNodeType.NILIT);
        TreeNode one = new TreeNode(ParseTreeNodeType.NILIT);
        TreeNode three = new TreeNode(ParseTreeNodeType.NILIT);
        TreeNode two = new TreeNode(ParseTreeNodeType.NILIT);

        ten.setConstRecord(p.lookupConstant("10"));
        five.setConstRecord(p.lookupConstant("5"));
        one.setConstRecord(p.lookupConstant("1"));
        three.setConstRecord(p.lookupConstant("3"));
        two.setConstRecord(p.lookupConstant("2"));

        ten.setDataType(NodeDataType.Float);
        five.setDataType(NodeDataType.Float);
        one.setDataType(NodeDataType.Float);
        three.setDataType(NodeDataType.Float);
        two.setDataType(NodeDataType.Float);

        subtract1.setDataType(NodeDataType.Float);
        subtract2.setDataType(NodeDataType.Float);
        subtract3.setDataType(NodeDataType.Float);
        times.setDataType(NodeDataType.Float);
        divide.setDataType(NodeDataType.Float);

        subtract3.setLeft(subtract2);
        subtract3.setRight(divide);
        subtract2.setLeft(subtract1);
        subtract2.setRight(times);
        subtract1.setLeft(ten);
        subtract1.setRight(five);

        times.setLeft(one);
        times.setRight(three);

        divide.setLeft(two);
        divide.setRight(one);

        System.out.println(actual.printTree());
        Assert.assertTrue(subtract3.equals(actual));
    }

    @Test
    public void testParseMultiplyExpr() throws Exception {
        String input = "a * 1";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NExprNode.make(p);

        p.pushScope("#invalid");
        TreeNode mult = new TreeNode(ParseTreeNodeType.NMUL);
        TreeNode a = new TreeNode(ParseTreeNodeType.NSIMVAR, p.lookupIdentifier("a"));
        TreeNode one = new TreeNode(ParseTreeNodeType.NILIT);
        one.setConstRecord(p.lookupConstant("1"));
        one.setDataType(NodeDataType.Float);

        mult.setLeft(a);
        mult.setRight(one);

        Assert.assertTrue(mult.equals(actual));
    }

    @Test
    public void testParseDivideExpr() throws Exception {
        String input = "a / 1.1";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NExprNode.make(p);

        p.pushScope("#invalid");
        TreeNode divide = new TreeNode(ParseTreeNodeType.NDIV);
        TreeNode a = new TreeNode(ParseTreeNodeType.NSIMVAR, p.lookupIdentifier("a"));
        TreeNode oneOne = new TreeNode(ParseTreeNodeType.NFLIT);
        oneOne.setConstRecord(p.lookupConstant("1.1"));
        oneOne.setDataType(NodeDataType.Float);

        divide.setLeft(a);
        divide.setRight(oneOne);

        Assert.assertTrue(divide.equals(actual));
    }

    @Test
    public void testParseIntDivideExpr() throws Exception {
        String input = "a div (a >= 1)";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NExprNode.make(p);

        p.pushScope("#invalid");
        TreeNode div = new TreeNode(ParseTreeNodeType.NIDIV);
        TreeNode bool = new TreeNode(ParseTreeNodeType.NGEQ);
        TreeNode a = new TreeNode(ParseTreeNodeType.NSIMVAR, p.lookupIdentifier("a"));
        TreeNode one = new TreeNode(ParseTreeNodeType.NILIT);
        one.setConstRecord(p.lookupConstant("1"));
        one.setDataType(NodeDataType.Float);

        div.setLeft(a);
        div.setRight(bool);
        bool.setLeft(a);
        bool.setRight(one);

        Assert.assertTrue(div.equals(actual));
    }

    @Test
    public void testParsePrList() throws Exception {
        String input = "x,y[1], \"Hello\";";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NPrListNode.make(p);

        p.pushScope("#invalid");
        TreeNode prList1 = new TreeNode(ParseTreeNodeType.NPRLIST);
        TreeNode prList2 = new TreeNode(ParseTreeNodeType.NPRLIST);

        TreeNode x = new TreeNode(ParseTreeNodeType.NSIMVAR, p.lookupIdentifier("x"));
        TreeNode y = new TreeNode(ParseTreeNodeType.NARRVAR, p.lookupIdentifier("y"));
        TreeNode hello = new TreeNode(ParseTreeNodeType.NSTRG);
        TreeNode one = new TreeNode(ParseTreeNodeType.NILIT);
        one.setConstRecord(p.lookupConstant("1"));
        hello.setConstRecord(p.lookupConstant("Hello"));

        y.setLeft(one);

        prList1.setLeft(x);
        prList1.setRight(prList2);
        prList2.setLeft(y);
        prList2.setRight(hello);

        prList1.setDataType(NodeDataType.Node);
        prList2.setDataType(NodeDataType.Node);
        hello.setDataType(NodeDataType.String);
        one.setDataType(NodeDataType.Float);

        Assert.assertTrue(prList1.equals(actual));
    }

    @Test
    public void testParseIdList() throws Exception {
        String input = "x,y,z";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) { tokens.add(sc.nextValidToken()); }
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual = NIdListNode.make(p);

        TreeNode idList1 = new TreeNode(ParseTreeNodeType.NIDLST);
        TreeNode idList2 = new TreeNode(ParseTreeNodeType.NIDLST);
        TreeNode x = new TreeNode(ParseTreeNodeType.NSIMDEC, p.lookupIdentifier("x"));
        TreeNode y = new TreeNode(ParseTreeNodeType.NSIMDEC, p.lookupIdentifier("y"));
        TreeNode z = new TreeNode(ParseTreeNodeType.NSIMDEC, p.lookupIdentifier("z"));

        idList1.setLeft(x);
        idList1.setRight(idList2);
        idList2.setLeft(y);
        idList2.setRight(z);

        idList1.setDataType(NodeDataType.Node);
        idList2.setDataType(NodeDataType.Node);
        x.setDataType(NodeDataType.Float);
        y.setDataType(NodeDataType.Float);
        z.setDataType(NodeDataType.Float);

        Assert.assertTrue(idList1.equals(actual));
    }
}
