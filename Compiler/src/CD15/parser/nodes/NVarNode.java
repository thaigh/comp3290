package CD15.parser.nodes;

import CD15.entity.Token;
import CD15.entity.TokenType;
import CD15.parser.*;

import java.util.Stack;

/**
 * Generates a Var Node of the form:
 * <var>     ::= <id> <varTail>
 * <varTail> ::= ɛ | [ <expr> ]
 *
 * @author Tyler Haigh - C3182929
 * @since 29/08/2015
 */
public class NVarNode {

    /**
     * Attempts to make a Var Node given a valid token stream
     * @param p The Parser
     * @return A valid Var Tree Node for a valid token stream or NUNDF
     */
    public static TreeNode make(Parser p) {

        TreeNode var = new TreeNode(ParseTreeNodeType.NUNDEF);
        TreeNode expr = null;
        Token startOfExpression = null;

        if (!p.peekAndConsume(TokenType.TIDNT)) {
            p.syntaxError("Expected identifier name for variable", p.peekLine(), p.peekColumn());
            return var;
        }
        Token varId = p.lastToken();

        // ɛ | [ <expr> ]
        Token nextTok = p.peek();
        if (nextTok.getType() != TokenType.TLBRK){
            var.setNodeType(ParseTreeNodeType.NSIMVAR);
        }
        else {
            p.consume();
            startOfExpression = p.peek();
            expr = NExprNode.make(p);
            // TODO: Error recovery?

            if (!p.peekAndConsume(TokenType.TRBRK)) {
                p.syntaxError("Expected right bracket ] for array variable", p.peekLine(), p.peekColumn());
                return var;
            }

            var.setNodeType(ParseTreeNodeType.NARRVAR);
            var.setLeft(expr);
        }

        // Semantic Analysis
        boolean validVarId = false;
        boolean validExpr = false;
        boolean isArray = false;

        // Check ID. Requires looking at higher scopes
        SymbolTableRecord varRecord = null;
        Stack<String> scopeStack = new Stack<String>();

        boolean foundId = false;
        boolean emptyScope = false;
        while(!emptyScope && !foundId) {

            // Perform one additional lookup for the prog (global) scope
            if (p.emptyScope())
                emptyScope = true;

            if (p.lookupIdentifier(varId.getLexeme()) != null) {
                foundId = true;

                validVarId = true;
                varRecord = p.lookupIdentifier(varId.getLexeme());
                var.setIdentifierRecord(varRecord);

                if (varRecord.getDataType() == NodeDataType.FloatArray)
                    isArray = true;
            } else {
                scopeStack.push(p.popScope());
            }
        }

        // Push the scopes back
        while(!scopeStack.empty()) {
            p.pushScope(scopeStack.pop());
        }

        // Check if we found the id
        if (!foundId) {
            p.semanticError("Undefined variable '" + varId.getLexeme() + "'", varId.getLine(), varId.getColumn());
            varRecord = p.createInvalidRecord(varId);
            var.setIdentifierRecord(varRecord);
        }

        // Check expr is valid for an array variable
        if (isArray) {
            if (expr == null)
                p.semanticError(varId.getLexeme() + " is an array, but no array index was specified", varId.getLine(), varId.getColumn());
            else if (expr.getDataType() != NodeDataType.Float)
                p.semanticError("Invalid expression. Expected a numeric value but got "+ expr.getDataType().name(),
                        startOfExpression.getLine(), startOfExpression.getColumn());
            else
                validExpr = true;
        } else {
            if (expr != null) {
                p.semanticError(varId.getLexeme() + " is not an array, but an array index was given", startOfExpression.getLine(), startOfExpression.getColumn());
            }
        }

        if (validVarId && validExpr)
            var.setDataType(NodeDataType.Float);
        else
            var.setDataType(varRecord.getDataType());

        return var;
    }

}
