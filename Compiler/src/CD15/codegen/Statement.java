package CD15.codegen;

import CD15.parser.ParseTreeNodeType;
import CD15.parser.SymbolTableRecord;
import CD15.parser.TreeNode;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Generates the code for Statement nodes
 *
 * @author Tyler Haigh - C3182929
 * @since 10/10/2015
 */
public class Statement {

    /**
     * Generates the code for a statement or statement list
     * @param gen The Code Generator
     * @param node The root node to generate
     */
    public static void generate(CodeGenerator gen, TreeNode node) {

        if (node == null) return;

        // Determine the statement type
        ParseTreeNodeType nodeType = node.getNodeType();
        switch (nodeType) {
            case NSLIST: {
                generate(gen, node.getLeft());
                generate(gen, node.getRight());
                break;
            }
            case NPLEQ: case NMNEQ: case NSTEQ: case NDVEQ: case NASGN: {
                genAssignmentStatement(gen, node);
                break;
            }
            case NPRINT: case NPRLN: { genPrintStatement(gen, node); break; }
            case NINPUT: { genInputStatement(gen, node); break; }
            case NLOOP: { genLoopStatement(gen, node); break; }
            case NEXIT: { genExitStatement(gen, node); break; }
            case NIFT: case NIFTE: { genIfStatement(gen, node); break; }
            case NCALL: { genCallStatement(gen, node); break; }
        }


    }

    /**
     * Generates the code for an assignment statement
     * @param gen The Code Generator
     * @param node The root node for the assignment statement
     */
    private static void genAssignmentStatement(CodeGenerator gen, TreeNode node) {
        // Generate the expression code
        TreeNode left = node.getLeft();
        TreeNode right = node.getRight();

        int temp1 = Expression.generate(gen, right);

        // Store in the variable
        // Check if left is an array and we need to load the index register
        if (left.getNodeType() == ParseTreeNodeType.NARRVAR) {
            // Evaluate the expression
            int temp2 = Expression.generate(gen, left.getLeft());

            // Release memory
            gen.releaseTemp(temp2);

            // Load the index register and put temp1 back in accumulator
            gen.generate5Bytes(OpCode.LDIDX, temp2);
            gen.generate5Bytes(OpCode.GET, temp1);

            // Get the array descriptor index
            int arrayDescIndex = left.getIdentifierRecord().getArrayDescIndex();
            boolean useLocation = arrayDescIndex == -1;
            if (useLocation) arrayDescIndex = left.getIdentifierRecord().getArrayDescLocation();

            // Check if we need to add/subtract/multiply/divide
            switch (node.getNodeType()) {
                case NPLEQ:
                    // x += y  =>  x = x + y  =>  x = y + x
                    if (useLocation)
                        gen.generate5Bytes(OpCode.ADD, arrayDescIndex);
                    else
                        gen.generate3Bytes(OpCode.ADD_D, arrayDescIndex);
                    break;
                case NMNEQ:
                    // x -= y  =>  x = x - y
                    if (useLocation) {
                        gen.generate5Bytes(OpCode.GET, arrayDescIndex);
                        gen.generate5Bytes(OpCode.SUB, temp1);
                    } else {
                        gen.generate3Bytes(OpCode.GET_D, arrayDescIndex);
                        gen.generate5Bytes(OpCode.SUB, temp1);
                    }
                    break;
                case NSTEQ:
                    // x *= y  => x = x * y  =>  x = y * x
                    if (useLocation)
                        gen.generate5Bytes(OpCode.MUL, arrayDescIndex);
                    else
                        gen.generate3Bytes(OpCode.MUL_D, arrayDescIndex);
                    break;
                case NDVEQ:
                    // x /= y  =>  x = x / y
                    if (useLocation) {
                        gen.generate5Bytes(OpCode.GET, arrayDescIndex);
                        gen.generate5Bytes(OpCode.DIV, temp1);
                    } else {
                        gen.generate3Bytes(OpCode.GET_D, arrayDescIndex);
                        gen.generate5Bytes(OpCode.DIV, temp1);
                    }
                    break;
            }

            // Store the right hand expression
            if (useLocation)
                gen.generate5Bytes(OpCode.STO, arrayDescIndex);
            else
                gen.generate3Bytes(OpCode.STO_D, arrayDescIndex);

        }
        else {

            // Get the memory address
            int memoryAddress = left.getIdentifierRecord().getWordIndex();

            // Check if we need to add/subtract/multiply/divide
            switch (node.getNodeType()) {
                case NPLEQ:
                    // x += y  =>  x = x + y  =>  x = y + x
                    gen.generate5Bytes(OpCode.ADD, memoryAddress);
                    break;
                case NMNEQ:
                    // x -= y  =>  x = x - y
                    gen.generate5Bytes(OpCode.GET, memoryAddress);
                    gen.generate5Bytes(OpCode.SUB, temp1);
                    break;
                case NSTEQ:
                    // x *= y  => x = x * y  =>  x = y * x
                    gen.generate5Bytes(OpCode.MUL, memoryAddress);
                    break;
                case NDVEQ:
                    // x /= y  =>  x = x / y
                    gen.generate5Bytes(OpCode.GET, memoryAddress);
                    gen.generate5Bytes(OpCode.DIV, temp1);
                    break;
            }

            gen.generate5Bytes(OpCode.STO, memoryAddress);
        }

        // Release memory
        gen.releaseTemp(temp1);
    }

    /**
     * Generates the code for a Print or PrintLine statement
     * @param gen The Code Generator
     * @param node The root node for the print statement
     */
    private static void genPrintStatement(CodeGenerator gen, TreeNode node) {

        List<TreeNode> printList;

        // Check if it was a printline with no eList
        if (node.getNodeType() == ParseTreeNodeType.NPRLN && node.getLeft() == null)
            printList = new LinkedList<TreeNode>();
        else
            printList = node.getLeft().toList();

        // Iterate over the list
        for(TreeNode n : printList) {
            // Determine the type
            ParseTreeNodeType nodeType = n.getNodeType();
            switch (nodeType) {
                case NSIMVAR: {
                    // Get the variable's memory address
                    gen.generate5Bytes(OpCode.GET, n.getIdentifierRecord().getWordIndex());

                    // Print the variable
                    gen.generate1Byte(OpCode.VALPR);
                    break;
                }
                case NFLIT: case NILIT: {
                    // Get the variable's memory address
                    gen.generate5Bytes(OpCode.GET, n.getConstRecord().getWordIndex());

                    // Print the variable
                    gen.generate1Byte(OpCode.VALPR);
                    break;
                }
                case NARRVAR: {
                    // Evaluate the index expression
                    int temp1 = Expression.generate(gen, n.getLeft());

                    // Get the array index value
                    gen.generate5Bytes(OpCode.LDIDX, temp1);

                    // Get the array value
                    int arrayDescIdx = n.getIdentifierRecord().getArrayDescIndex();
                    if (arrayDescIdx == -1) {
                        // We are in a proc an need to use something else
                        gen.generate5Bytes(OpCode.GET, n.getIdentifierRecord().getArrayDescLocation());
                    }
                    else
                        gen.generate3Bytes(OpCode.GET_D, arrayDescIdx);

                    // Print the value
                    gen.generate1Byte(OpCode.VALPR);

                    // Release memory
                    gen.releaseTemp(temp1);

                    break;
                }
                case NLEN: {
                    gen.generate5Bytes(OpCode.GET, n.getIdentifierRecord().getArraySizeLocation());
                    gen.generate1Byte(OpCode.VALPR);
                    break;
                }
                case NSTRG: {
                    // Put the string into the accumulator
                    SymbolTableRecord rec = n.getConstRecord();
                    gen.generate5Bytes(OpCode.GETADDR, rec.getWordIndex());
                    gen.generate1Byte(OpCode.STRPR);
                    break;
                }
            }
        }

        // Check if it was a printline
        if (node.getNodeType() == ParseTreeNodeType.NPRLN)
            gen.generate1Byte(OpCode.NEWLN);
    }

    /**
     * Generates the code for an Input statement
     * @param gen The Code Generator
     * @param root The root node for the input statement
     */
    private static void genInputStatement(CodeGenerator gen, TreeNode root) {
        List<TreeNode> inputList = root.getLeft().toList();

        for(TreeNode node : inputList) {
            // Work out the memory address (could be storing in an array)
            ParseTreeNodeType nodeType = node.getNodeType();

            if (nodeType == ParseTreeNodeType.NARRVAR) {
                // Evaluate the expression
                int temp1 = Expression.generate(gen, node.getLeft());

                // Load the index register
                gen.generate5Bytes(OpCode.LDIDX, temp1);

                // Get the array descriptor index
                // Use auto-indirect if in a proc
                int arrayDescIndex = node.getIdentifierRecord().getArrayDescIndex();
                boolean useLocation = (arrayDescIndex == -1);
                if (useLocation) arrayDescIndex = node.getIdentifierRecord().getArrayDescLocation();

                // Read the value from input. Everything in AC15 is a float
                gen.generate1Byte(OpCode.READF);

                // Store in the memory address
                if (useLocation)
                    gen.generate5Bytes(OpCode.STO, arrayDescIndex);
                else
                    gen.generate3Bytes(OpCode.STO_D, arrayDescIndex);

                // Release Temp1
                gen.releaseTemp(temp1);
            }
            else {
                // Read the value from input. Everything in AC15 is a float
                gen.generate1Byte(OpCode.READF);

                // Store in the memory address
                gen.generate5Bytes(OpCode.STO, node.getIdentifierRecord().getWordIndex());
            }

        }
    }

    /**
     * Generates the code for a Loop construct
     * @param gen The Code Generator
     * @param root The root node of the Loop construct
     */
    private static void genLoopStatement(CodeGenerator gen, TreeNode root) {
        ProgramCounter start = gen.getProgramCounter();
        Statement.generate(gen, root.getLeft());
        ProgramCounter finish = gen.getProgramCounter();

        // Calculate total branch distance for branch back OpCode
        int totalBranchDistance = start.distance(finish);
        gen.generate5Bytes(OpCode.BB, totalBranchDistance+5); // Need to include this opcode as well
        ProgramCounter afterBranchBack = gen.getProgramCounter();

        // Bind the unbound instruction references found throughout the loop.
        // Example: EXIT statements
        List<ProgramCounter> unbound = root.getIdentifierRecord().getUnboundInstructions();
        for (ProgramCounter pc : unbound) {

            int pcVal = gen.getInstruction(pc);
            int cbfVal = OpCode.CBF.value();

            if (pcVal == cbfVal) {
                rebind(gen, pc, OpCode.CBF, pc.distance(afterBranchBack)-5, 5); // Need to subtract 5 for the instruction
            }
        }
    }

    /**
     * Generates the code for an Exit Statement
     * @param gen The Code Generator
     * @param root The root node for the Exit Statement
     */
    private static void genExitStatement(CodeGenerator gen, TreeNode root) {
        // Evaluate the boolean expression
        int temp1 = Expression.generate(gen, root.getLeft());

        // Get program counter
        ProgramCounter pc = gen.getProgramCounter();

        // CBF to undefined
        gen.generate5Bytes(OpCode.CBF, -1);

        // Add program counter to loop's list of unbound instructions
        SymbolTableRecord rec = root.getIdentifierRecord();
        rec.addUnboundInstruction(pc);

        // Release memory
        gen.releaseTemp(temp1);
    }

    /**
     * Generates the code for an If-Then-Else construct
     * @param gen The Code Generator
     * @param root The root node for the If Construct
     */
    private static void genIfStatement(CodeGenerator gen, TreeNode root) {
        // Generate boolean expression
        int temp1 = Expression.generate(gen, root.getLeft());
        gen.releaseTemp(temp1);

        gen.generate1Byte(OpCode.NOT);

        ProgramCounter condBranchForward = gen.getProgramCounter();
        gen.generate5Bytes(OpCode.CBF, -1);
        ProgramCounter thenBlock = gen.getProgramCounter();

        //Generate then clause
        TreeNode thenNode = (root.getNodeType() == ParseTreeNodeType.NIFTE) ?
                root.getMiddle() : root.getRight();
        Statement.generate(gen, thenNode);

        ProgramCounter endThenBlock = gen.getProgramCounter();
        gen.generate5Bytes(OpCode.BF, -1);
        ProgramCounter elsifBlock = gen.getProgramCounter();

        // Check for ELSIF
        TreeNode right = root.getRight();
        boolean hasElsif =
                root.getNodeType() == ParseTreeNodeType.NIFTE &&
                right.getNodeType() == ParseTreeNodeType.NIFTE;
        if (hasElsif)
            genIfStatement(gen, right);

        // Check for ELSE
        ProgramCounter elseBlock = gen.getProgramCounter();
        boolean hasElse = root.getNodeType() == ParseTreeNodeType.NIFTE;

        if (!hasElsif && hasElse)
            Statement.generate(gen, right);

        ProgramCounter endIf = gen.getProgramCounter();

        // Go back and fill in the gaps
        // 1. bind thenBlock to either elsif or else or endIf block (In order)
        int startToFinish = 0;
        if (hasElsif)
            startToFinish = thenBlock.distance(elsifBlock);
        else if (hasElse)
            startToFinish = thenBlock.distance(elseBlock);
        else
            startToFinish = thenBlock.distance(endIf);

        int branchForward = elsifBlock.distance(endIf);

        rebind(gen, condBranchForward, OpCode.CBF, startToFinish, 5);
        rebind(gen, endThenBlock, OpCode.BF, branchForward, 5);
    }

    /**
     * Generates the code for a Call Statement and copies the required variables into
     * the appropriate memory addresses
     * @param gen The Code Generator
     * @param root The root node for the Call Statement
     */
    private static void genCallStatement(CodeGenerator gen, TreeNode root) {
        // Get the Proc TreeNode
        TreeNode procNode = root.getIdentifierRecord().getTreeNode();
        TreeNode params = procNode.getLeft();
        List<TreeNode> vars = (params != null && params.getLeft() != null) ? params.getLeft().toList() : null;
        List<TreeNode> vals = (params != null && params.getRight() != null) ? params.getRight().toList() : null;
        List<TreeNode> locals = (procNode.getMiddle() != null) ? procNode.getMiddle().toList() : null;

        // Get the EList variables
        List<TreeNode> eList = (root.getLeft() != null) ? root.getLeft().toList() : null;

        /* Personally, I think that the data for the procedure should be reset
           on each call, however, this behaviour is not wanted, so it is commented out

        gen.generate1Byte(OpCode.CLEAR);

        // Clear Vals
        if (vals != null) {
            for(TreeNode v : vals) {
                SymbolTableRecord rec = v.getIdentifierRecord();
                gen.generate5Bytes(OpCode.STO, rec.getWordIndex());
            }
        }

        // Clear locals
        if (locals != null) {
            for(TreeNode l : locals) {
                SymbolTableRecord rec = l.getIdentifierRecord();
                gen.generate5Bytes(OpCode.STO, rec.getWordIndex());
            }
        }
         */

        if (eList != null) {

            // Get an iterator for the eList item
            Iterator<TreeNode> eListIterator = eList.listIterator();

            // Get Vars and set up addressing
            if (vars != null) {
                for(TreeNode v : vars) {
                    SymbolTableRecord varRec = v.getIdentifierRecord();
                    TreeNode eListTreeNode = eListIterator.next();
                    SymbolTableRecord eListRec = eListTreeNode.getIdentifierRecord();
                    SymbolTableRecord procVarRec = v.getIdentifierRecord();

                    if (v.getNodeType() == ParseTreeNodeType.NARRPAR) {
                        // Set up the array size
                        gen.generate5Bytes(OpCode.GET, eListRec.getArraySizeLocation());
                        gen.generate5Bytes(OpCode.STO, varRec.getArraySizeLocation());

                        int arrayDescIdx = eListRec.getArrayDescIndex();
                        if (arrayDescIdx == -1) {
                            // We're in a proc. Use the fake one stored in an address
                            gen.generate5Bytes(OpCode.GETDESC, eListRec.getArrayDescLocation());
                        }
                        else {
                            // Use the real one
                            gen.generate3Bytes(OpCode.GETDESC_D, eListRec.getArrayDescIndex());
                        }

                        gen.generate5Bytes(OpCode.STODIR, procVarRec.getArrayDescLocation());
                    }
                    else {
                        // Check if its an array var
                        if (eListTreeNode.getNodeType() == ParseTreeNodeType.NARRVAR) {
                            // Evaluate the expression
                            int temp = Expression.generate(gen, eListTreeNode.getLeft());
                            gen.releaseTemp(temp);

                            // Get the array descriptor for auto indirection
                            int arrayDescIdx = eListRec.getArrayDescIndex();
                            boolean useLocation = (arrayDescIdx == -1);
                            if (useLocation) arrayDescIdx = eListRec.getArrayDescLocation();

                            if (useLocation)
                                gen.generate5Bytes(OpCode.GETADDR, arrayDescIdx);
                            else
                                gen.generate3Bytes(OpCode.GETADDR_D, arrayDescIdx);

                            gen.generate5Bytes(OpCode.STODIR, procVarRec.getWordIndex());
                        } else {
                            gen.generate5Bytes(OpCode.GETADDR, eListRec.getWordIndex());
                            gen.generate5Bytes(OpCode.STODIR, procVarRec.getWordIndex());
                        }
                    }
                }
            }


            // Get Vals and set up copying. Keep in mind the eList iterator may reach end of list
            if (vals != null) {
                for(TreeNode v : vals) {

                    if (eListIterator.hasNext()) {

                        TreeNode eListNode = eListIterator.next();
                        SymbolTableRecord eListRec = null;

                        switch(eListNode.getNodeType()) {
                            case NILIT: case NFLIT: {
                                eListRec = eListNode.getConstRecord();
                                break;
                            } case NSIMVAR: {
                                eListRec = eListNode.getIdentifierRecord();
                                break;
                            }
                            default: {
                                int temp1 = Expression.generate(gen, eListNode);
                                gen.releaseTemp(temp1); //Release Temp

                                // Create a temporary elist record
                                eListRec = new SymbolTableRecord();
                                eListRec.setWordIndex(temp1);
                                break;
                            }
                        }

                        gen.generate5Bytes(OpCode.GET, eListRec.getWordIndex());

                        SymbolTableRecord rec = v.getIdentifierRecord();
                        gen.generate5Bytes(OpCode.STO, rec.getWordIndex());
                    }
                    else break;
                }
            }
        }

        // Generate CALL OpCode
        SymbolTableRecord procRec = procNode.getIdentifierRecord();
        gen.generate3Bytes(OpCode.CALL, procRec.getSubProgDescIndex());

    }

    /**
     * Rebinds a memory address somewhere in pre-generated code. Used for CBF
     * @param gen The Code Generator
     * @param startBind The place to start binding and re-writing instructions
     * @param op The Nnew OpCode
     * @param operand The new Operand
     * @param length The number of bytes to generate and replace
     */
    private static void rebind(CodeGenerator gen, ProgramCounter startBind, OpCode op, int operand, int length) {
        // Split into bytes
        int[] operands = new int[length];

        operands[0] = op.value();
        for (int i = 1; i < length; i++) {

            int power = 8 * (length - i - 1);
            int divisor = (int)Math.pow(2, power);

            int higherOrder = operand / divisor;
            operands[i] = higherOrder;
            operand -= higherOrder * divisor;
        }
        //operands[length - 1] = operand;

        gen.replaceInstruction(startBind, operands);
    }

}
