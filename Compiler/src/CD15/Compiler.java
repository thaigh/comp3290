package CD15;

import CD15.codegen.CodeGenerator;
import CD15.controller.OutputController;
import CD15.entity.Error;
import CD15.entity.Token;
import CD15.entity.TokenType;
import CD15.parser.Parser;
import CD15.parser.SymbolTable;
import CD15.parser.TreeNode;

import java.io.*;
import java.util.*;

/**
 * The core class of the compiler responsible for calling the main routines of compilation:
 * <ul>
 *   <li> Lexical Analysis </li>
 *   <li> Syntactic Analysis </li>
 *   <li> Semantic Analysis </li>
 *   <li> Code Generation </li>
 * </ul>
 *
 * @author Tyler Haigh - C3182929
 * @since 2/08/2015
 */
public class Compiler {

    private File cd15ProgramFile;
    private File programListingFile;
    private File codeModuleFile;

    private SymbolTable identifiers;
    private SymbolTable constants;
    private List<CD15.entity.Error> errorList;

    private CD15.scanner.Scanner scanner;
    private Parser parser;
    private CodeGenerator generator;
    private OutputController outputController;

    public Compiler(File cd15ProgramFile, File programListingFile, File codeModuleFile) {

        this.cd15ProgramFile = cd15ProgramFile;
        this.programListingFile = programListingFile;
        this.codeModuleFile = codeModuleFile;

        // Initialise private instance variables
        this.identifiers = new SymbolTable();
        this.constants = new SymbolTable();
        this.errorList = new LinkedList<Error>();

        try {
            OutputStream out = programListingFile != null ? new FileOutputStream(programListingFile) : System.out;
            this.outputController = new OutputController(new PrintWriter(out, true));
        } catch (IOException io) {
            System.out.println("Error: Unable to use program listing output file. Details: " + io.getMessage());
            System.out.print("Using System.out for program listing output.");
            this.outputController = new OutputController(new PrintWriter(System.out, true));
        }

        // Convert the file to a filestream and read its input
        String stream = readStream(this.cd15ProgramFile);
        this.scanner = new CD15.scanner.Scanner(stream, outputController);

    }


    public static String readStream(File f) {
        InputStream is = null;
        try {
            is = new FileInputStream(f);
            StringBuilder sb = new StringBuilder();
            try {
                Reader r = new InputStreamReader(is);
                int c = 0;
                while ((c = r.read()) != -1) {
                    sb.append((char) c);
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            return sb.toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
             try { if (is != null) is.close(); } catch (IOException io) {}
        }

        return "";
    }


    /**
     * Starts the compilation process to produce the machine code for the input filestream
     */
    public void run() {
        System.out.println("Starting Lexical analysis...\n");
        List<Token> tokens = lexicalAnalysis();
        printTokens(tokens); // Debug Method.

        System.out.println("\nFinished Lexical Analysis. Starting Syntactic and Semantic analysis...\n");
        TreeNode cd15 = syntacticAnalysis(tokens);
        printParseTree(cd15);

        System.out.println("\nFinished Syntactic and Semantic Analysis. The program listing file is located at " +
                ((programListingFile != null) ? programListingFile.getAbsolutePath() : "System.out"));

        if (parser.validProgram()) {
            System.out.println("Starting Code Generation...\n");
            String code = codeGeneration(cd15);
            printCode(code);
            System.out.println("\nFinished Code Generation. The code module is located at " +
                    ((codeModuleFile != null && codeModuleFile.exists()) ? codeModuleFile.getAbsolutePath() : "System.out" ));
        } else {
            System.err.println("Unable to generate code module for input program. See the program listing for any errors");
        }
    }

    /**
     * Performs lexical analysis on the input stream to generate the entire list of lexeme-tokens
     * in the source code. Comments will not be returned as a token
     *
     * @return The complete list of valid tokens that conform to the lexical nature of the CD15 language
     */
    private List<Token> lexicalAnalysis() {

        List<Token> tokens = new LinkedList<Token>();

        Token nextToken = new Token(TokenType.TUNDF,"", -1, -1);
        while (nextToken.getType() != TokenType.TEOF) {
            nextToken = scanner.nextValidToken();
            tokens.add(nextToken);
        }

        outputController.flushMessages();
        outputController.flushErrors();
        return tokens;
    }

    private TreeNode syntacticAnalysis(List<Token> tokens) {
        // Remove any unwanted tokens (such as TUNDF)
        boolean removedTokens = false;
        for (Iterator<Token> iter = tokens.iterator(); iter.hasNext(); ) {
            Token t = iter.next();
            if (t.getType() == TokenType.TUNDF) {
                iter.remove();
                removedTokens = true;
            }
        }

        if (removedTokens)
            System.out.println("Warning: Undefined tokens in program. These have been ignored.");

        // Start parsing
        parser = new Parser(tokens, constants, identifiers, outputController);
        TreeNode cd15 = parser.parse();
        return cd15;
    }

    private String codeGeneration(TreeNode program) {
        generator = new CodeGenerator(constants);
        String code = generator.generate(program);
        return code;
    }


    /**
     * Prints the list of generated tokens returned from the Scanner
     * @param tokens The list of Tokens to print
     */
    public void printTokens(List<Token> tokens) {
        StringBuilder sb = new StringBuilder();

        //System.out.println("\n********** Token Output **********\n");

        for(Token t : tokens) {

            if (sb.length() > 0 && t.getType() == TokenType.TUNDF) {
                System.out.println(sb.toString());
                sb.setLength(0);
            }

            sb.append(t.toString());
            int outputLength = t.toString().length();

            // Add padding
            int add;
            if (t.getLexeme() == null) {
                // Only displaying token id
                add = 6;
            } else {
                // Add multiple of 6
                int remainder = outputLength % 6;
                add = outputLength + 6 - remainder;
            }
            sb.append(String.format("%" + (add - outputLength) + "s", ""));

            if (sb.length() > 60 || t.getType() == TokenType.TUNDF) {
                System.out.println(sb.toString());
                sb.setLength(0);
            }
        }

        // Flush string builder
        System.out.println(sb.toString());
    }

    public void printParseTree(TreeNode tree) {
        System.out.println(tree.printTree());
    }

    public void printCode(String code) {
        if (codeModuleFile != null && codeModuleFile.exists()) {

            try {
                PrintWriter writer = new PrintWriter(codeModuleFile);
                writer.write(code);
                writer.close();
            } catch (IOException io) {
                System.err.println("Unable to write to code module file. Printing to System.out\n\n");
                System.out.println(code);
            }
        } else {
            System.out.println(code);
        }
    }
}
