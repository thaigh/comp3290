package CD15.parser.nodes;

import CD15.entity.Token;
import CD15.entity.TokenType;
import CD15.parser.NodeDataType;
import CD15.parser.ParseTreeNodeType;
import CD15.parser.Parser;
import CD15.parser.TreeNode;

/**
 * Generates a Var List Node of the form:
 * < vList>     ::= < var> < vListTail>
 * < vListTail> ::= ɛ | , < vList>
 *
 * @author Tyler Haigh - C3182929
 * @since 29/08/2015
 */
public class NVListNode {

    /**
     * Attempts to make a Var Node given a valid token stream
     * @param p The Parser
     * @return A valid Var Tree Node for a valid token stream or NUNDF
     */
    public static TreeNode make(Parser p) {

        TreeNode var = NVarNode.make(p);
        if (var != null && var.getNodeType() == ParseTreeNodeType.NUNDEF) {
            try { errorRecovery(p); }
            catch (Exception e) { return new TreeNode(ParseTreeNodeType.NUNDEF); }
        }

        TreeNode tail = vListTail(p);

        if (tail == null) return var;
        else {
            TreeNode vList = new TreeNode(ParseTreeNodeType.NVLIST, var, tail);
            vList.setDataType(NodeDataType.Node);
            return vList;
        }
    }

    /**
     * Attempts to make the Var List Tail node given a valid token stream
     * @param p The Parser
     * @return Null if no Var List follows, a valid Var List Tree Node or NUNDF
     */
    public static TreeNode vListTail(Parser p) {
        Token nextTok = p.peek();
        if (nextTok.getType() != TokenType.TCOMA) return null;
        else {
            p.consume();

            TreeNode vList = make(p);
            return vList;
        }
    }

    /**
     * Attempts error recovery in the event of an invalid token stream
     * @param p The Parser
     * @throws Exception Unable to recover
     */
    private static void errorRecovery(Parser p) throws Exception {
        // Find next comma or semi
        int nextComma = p.next(TokenType.TCOMA);
        int nextSemi = p.next(TokenType.TSEMI);

        if (nextComma > -1 && nextComma < 5) p.consume(nextComma);
        else if (nextSemi > -1) p.consume(nextSemi);
        else throw new Exception("Unable to recover");
    }
}
