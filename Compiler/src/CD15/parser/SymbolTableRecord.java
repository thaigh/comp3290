package CD15.parser;

import CD15.codegen.ProgramCounter;
import CD15.entity.Token;
import CD15.entity.TokenType;

import java.util.LinkedList;
import java.util.List;

/**
 * Stores the key information for defining a tokenised symbol
 *
 * @author Tyler haigh - C3182929
 * @since 22/08/2015
 */
public class SymbolTableRecord {

    private int key;
    private TokenType type;
    private String lexeme;

    // Variables for semantic analysis
    private String scope;
    private TreeNode treeNode;

    // Variables for Code Generation
    private int wordIndex = -1; // Start location in memory
    private int arrayDescIndex = -1; // Location in Array descriptor array
    private int subprogDescIndex = -1; // Location in Array descriptor array
    private int arraySizeLocation = -1; // Size of array block
    private int arrayDescLocation = -1; // Size of array block
    private List<ProgramCounter> unboundInstructions = new LinkedList<ProgramCounter>();

    /**
     * Default constructor for a symbol table record. Created an undefined symbol
     */
    public SymbolTableRecord() {
        this(TokenType.TUNDF, "", "");
    }

    /**
     * Constructor for a symbol table record given a Token
     * @param t The token to convert to a symbol
     */
    public SymbolTableRecord(Token t, String scope) { this(t.getType(), t.getLexeme(), scope); }

    /**
     * Constructor for a symbol table record given the token type and the hashable lexeme key
     * @param type The Token Type
     * @param lexeme The lexeme string to has hash
     */
    public SymbolTableRecord(TokenType type, String lexeme, String scope) {
        this.key = (scope + lexeme).hashCode();
        this.type = type;
        this.lexeme = lexeme;
        this.scope = scope;
        this.treeNode = null;
    }

    public int getKey() { return key; }
    public TokenType getType() { return type; }
    public String getLexeme() { return lexeme; }
    public String getScope() { return scope; }
    public TreeNode getTreeNode() { return treeNode; }
    public int getWordIndex() { return wordIndex; }
    public int getArrayDescIndex() { return arrayDescIndex; }
    public int getSubProgDescIndex() { return subprogDescIndex; }
    public int getArraySizeLocation() { return arraySizeLocation; }
    public int getArrayDescLocation() { return arrayDescLocation; }

    public void setKey(int key) { this.key = key; }
    public void setType(TokenType type) { this.type = type; }
    public void setLexeme(String lexeme) { this.lexeme = lexeme; }
    public void setScope(String scope) { this.scope = scope; }
    public void setTreeNode(TreeNode node) { this.treeNode = node; }
    public void setWordIndex(int wordIndex) { this.wordIndex = wordIndex; }
    public void setArrayDescIndex(int arrayDescIndex) { this.arrayDescIndex = arrayDescIndex; }
    public void setSubProgDescIndex(int subProgDescIndex) { this.subprogDescIndex = subProgDescIndex; }
    public void setArraySizeLocation(int arraySizeLocation) { this.arraySizeLocation = arraySizeLocation; }
    public void setArrayDescLocation(int arrayDescLocation) { this.arrayDescLocation = arrayDescLocation; }

    public NodeDataType getDataType() { return this.treeNode != null ? treeNode.getDataType() : NodeDataType.Undefined; }
    public void addUnboundInstruction(ProgramCounter pc) { unboundInstructions.add(pc); }
    public List<ProgramCounter> getUnboundInstructions() { return this.unboundInstructions; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SymbolTableRecord)) return false;

        SymbolTableRecord otherRec = (SymbolTableRecord) o;

        if (key != otherRec.key) return false;
        if (lexeme != null ? !lexeme.equals(otherRec.lexeme) : otherRec.lexeme != null) return false;
        if (type != otherRec.type) return false;
        if (scope != null ? !scope.equals(otherRec.scope) : otherRec.scope != null) return false;
        if (treeNode != null ? !treeNode.equals(otherRec.treeNode) : otherRec.treeNode != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = key;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (lexeme != null ? lexeme.hashCode() : 0);
        result = 31 * result + (scope != null ? scope.hashCode() : 0);
        result = 31 * result + (treeNode != null ? treeNode.hashCode() : 0);
        return result;
    }
}
