package CD15.parser.nodes;

import CD15.entity.Token;
import CD15.entity.TokenType;
import CD15.parser.NodeDataType;
import CD15.parser.ParseTreeNodeType;
import CD15.parser.Parser;
import CD15.parser.TreeNode;

/**
 * Generates a ProcL Node of the form:
 * <procs> ::= ɛ | <proc> <procs>
 *
 * @author Tyler Haigh - C3182929
 * @since 27/08/2015
 */
public class NProclNode {

    /**
     * Attempts to make the ProcList node given a valid token stream
     * @param p The Parser
     * @return A valid Tree Node for a valid token stream or NUNDF
     */
    public static TreeNode make(Parser p) {

        Token nextTok = p.peek();
        if (nextTok.getType() != TokenType.TPROC) return null;
        else {
            TreeNode proc = NProcNode.make(p);

            // Error Checking
            if (proc.getNodeType() == ParseTreeNodeType.NUNDEF) {
                try { errorRecovery(p); }
                catch (Exception e) { return new TreeNode(ParseTreeNodeType.NUNDEF); }
            }

            TreeNode procs = make(p);

            if (procs == null) return proc;
            else {
                TreeNode procl = new TreeNode(ParseTreeNodeType.NPROCL, proc, procs);
                procl.setDataType(NodeDataType.Node);
                return procl;
            }
        }
    }

    /**
     * Attempts error recovery in the event of an invalid token stream
     * @param p The Parser
     * @throws Exception Unable to recover
     */
    private static void errorRecovery(Parser p) throws Exception {
        int nextProc = p.next(TokenType.TPROC);

        // Consume up to end of next closing prog if possible
        if (nextProc > -1 &&
                p.peek(nextProc - 1).getType() == TokenType.TENDK &&
                p.peek(nextProc + 1).getType() == TokenType.TIDNT)
            p.consume(nextProc + 2);
        else if (nextProc > -1 && p.peek(nextProc - 1).getType() == TokenType.TENDK) {
            TokenType nextTokType = p.peek(nextProc + 1).getType();
            switch (nextTokType) {
                case TPROC: p.consume(nextProc + 1);
                case TLOCL: p.consume(nextProc + 1);
            }
        }

        else throw new Exception("Unable to recover");
    }

}
