package CD15.parser.nodes;

import CD15.entity.Token;
import CD15.entity.TokenType;
import CD15.parser.NodeDataType;
import CD15.parser.ParseTreeNodeType;
import CD15.parser.Parser;
import CD15.parser.TreeNode;

import java.util.Arrays;

/**
 * Generates a Params Node of the form:
 * < parameters>    ::= < parameterVars> < parameterVals>
 * < parameterVars> ::= ɛ | var < pList>
 * < parameterVals> ::= ɛ | val < idList>
 *
 * @author Tyler Haigh - C3182929
 * @since 28/08/2015
 */
public class SParamsNode {

    /**
     * Attempts to make a params node given a valid token stream
     * @param p The Parser
     * @return Null if no vars or vals, a valid Tree Node for a valid
     *         token stream or NUNDF
     */
    public static TreeNode make(Parser p) {

        TreeNode vars = vars(p);
        TreeNode vals = vals(p);

        // Check if nulls
        if (vars == null && vals == null) return null;

        // Create list node
        TreeNode params = new TreeNode(ParseTreeNodeType.NPARAMS, vars, vals);
        params.setDataType(NodeDataType.Node);

        return params;
    }

    /**
     * Attempts to generate a vars node given a valid token stream
     * @param p The Parser
     * @return A valid Tree Node for a valid token stream or NUNDF
     */
    private static TreeNode vars(Parser p) {

        Token nextTok = p.peek();
        if (nextTok.getType() != TokenType.TVARP) return null;
        else {
            p.consume();

            TreeNode pList = NPListNode.make(p);
            if(pList != null && pList.getNodeType() == ParseTreeNodeType.NUNDEF) {
                try { errorRecovery(p); }
                catch (Exception e) { return new TreeNode(ParseTreeNodeType.NUNDEF); }
            }

            return pList;
        }
    }

    /**
     * Attempts to generate a vals node for a valid token stream
     * @param p The Parser
     * @return A valid vals node for a valid token stream or NUNDF
     */
    private static TreeNode vals(Parser p) {
        Token nextTok = p.peek();
        if (nextTok.getType() != TokenType.TVALP) return null;
        else {
            p.consume();

            TreeNode idList = NPIdListNode.make(p);
            if(idList != null && idList.getNodeType() == ParseTreeNodeType.NUNDEF) {
                try { errorRecovery(p); }
                catch (Exception e) { return new TreeNode(ParseTreeNodeType.NUNDEF); }
            }

            return idList;
        }
    }

    /**
     * Attempts error recovery in the event of an invalid token stream
     * @param p The Parser
     * @throws Exception Unable to recover
     */
    private static void errorRecovery(Parser p) throws Exception {
        int[] scanAmounts = new int[13];
        // Scan ahead to find a statement or something we can synchronise to
        // Don't scan to identifiers yet as this would mean we were in ASGN statement
        scanAmounts[0] = p.next(TokenType.TLOOP);
        scanAmounts[1] = p.next(TokenType.TEXIT);
        scanAmounts[2] = p.next(TokenType.TIFKW);
        scanAmounts[3] = p.next(TokenType.TINPT);
        scanAmounts[4] = p.next(TokenType.TPRIN);
        scanAmounts[5] = p.next(TokenType.TPRLN);
        scanAmounts[6] = p.next(TokenType.TCALL);
        scanAmounts[7] = p.next(TokenType.TELSE);
        scanAmounts[8] = p.next(TokenType.TELSF);

        scanAmounts[9] = p.next(TokenType.TSEMI); // SEMI for end of LOCAL or first Statement
        scanAmounts[10] = p.next(TokenType.TVALP);
        scanAmounts[11] = p.next(TokenType.TLOCL);
        scanAmounts[12] = p.next(TokenType.TENDK); // END PROC or END PROGRAM

        // Find the min
        Arrays.sort(scanAmounts);
        int minScanAmount = -1;
        for (int i = 0; i < scanAmounts.length; i++) {
            if (scanAmounts[i] != -1) {
                minScanAmount = scanAmounts[i];
                break;
            }
        }

        if (minScanAmount == -1) {
            // See if the next thing is an IDNT for an assignment statement
            int nextIdent = p.next(TokenType.TIDNT);
            if (nextIdent != -1) p.consume(nextIdent);
            else throw new Exception("Unable to scan ahead");
        }
        else {
            if (p.peek(minScanAmount).getType() == TokenType.TSEMI)
                p.consume(minScanAmount+1);
            else p.consume(minScanAmount);
        }
    }

}
