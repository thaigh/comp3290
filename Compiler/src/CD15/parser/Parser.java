package CD15.parser;

import CD15.controller.OutputController;
import CD15.entity.*;
import CD15.entity.Error;
import CD15.parser.nodes.NProgNode;

import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

/**
 *
 * Processes the token stream and analyses for syntactically valid
 * structures
 *
 * @author Tyler Haigh
 * @since 27/08/2015
 */
public class Parser {

    private List<Token> tokens;
    private SymbolTable constants;
    private SymbolTable identifiers;
    private int tokenIndex;
    private OutputController output;

    private Stack<String> programScope = new Stack<String>();
    private Stack<String> loopScope = new Stack<String>();

    private boolean syntacticallyValid = true;
    private boolean semanticallyValid = true;

    /**
     * Constructor for the parser with a null output stream
     * @param tokens The token stream to parse
     */
    public Parser(List<Token> tokens) {
        this(tokens, new SymbolTable(), new SymbolTable(), new OutputController(new PrintWriter(new NullOutputStream())));
    }

    /**
     * Constructor for the parser with external symbol tables and user defined output controller
     * @param tokens The token stream to parse
     * @param constants The constants symbol table
     * @param identifiers The identifiers symbol table
     * @param output The output controller
     */
    public Parser(List<Token> tokens, SymbolTable constants, SymbolTable identifiers, OutputController output) {
        this.tokens = new LinkedList<Token>(tokens);

        this.constants = constants;
        this.identifiers = identifiers;
        this.tokenIndex = 0;
        this.output = output;

        this.programScope = new Stack<String>();
        this.programScope.push("");

        this.loopScope = new Stack<String>();
        this.loopScope.push("");
    }

    /**
     * Parses the token stream and analyses for syntactic errors
     * @return The generated parse tree for the given token stream
     */
    public TreeNode parse() {
        syntacticallyValid = true;
        semanticallyValid = true;

        //return NProgNode.make(this);

        try { return NProgNode.make(this); }
        catch (Exception e) {
            // The parser should never hit this state, however if something outside of
            // testing causes this to occur, then at least the program won't crash
            output.printImmediateError(new Error(ErrorType.Fatal, "An error occurred whilst parsing the program", peekLine(), peekColumn()));

            syntacticallyValid = false;
            semanticallyValid = false;

            return new TreeNode(ParseTreeNodeType.NUNDEF);
        }
    }

    /**
     * Peeks at the next token in the stream
     * @return The next token in the stream
     */
    public Token peek() { return peek(0); }

    /**
     * Peeks at the line number of the next token in the stream
     * @return The line number of the next token in the stream
     */
    public int peekLine() { return peek(0).getLine(); }

    /**
     * Peeks at the column number of the next token in the stream
     * @return The column number of the next token in the stream
     */
    public int peekColumn() { return peek(0).getColumn(); }

    /**
     * Peeks at the token n positions away from the next token in the stream
     * @param amount The number of tokens away to peek at
     * @return The token n positions away from the next token
     */
    public Token peek(int amount) { return (tokenIndex + amount) >= tokens.size() ? null : tokens.get(tokenIndex + amount); }

    /**
     * Peeks at the line number of the token n positions away from the next token
     * @param amount The number of positions away to peek at
     * @return The line number of the token n positions away
     */
    public int peekLine(int amount) { return peek(amount).getLine(); }

    /**
     * Peeks at the column number ot the token n positions away fro the next token
     * @param amount the number of positions away to peek at
     * @return The column number of the token n positions away
     */
    public int peekColumn(int amount) { return peek(amount).getColumn(); }

    /**
     * Consumes the next token in the stream
     */
    public void consume() { tokenIndex++; }

    /**
     * Consumes the next n tokens in the stream
     * @param amount The number of tokens to consume after the next Token
     */
    public void consume(int amount) { tokenIndex += amount; }

    /**
     * Peeks at the next token in the stream and consumes it if it matches the given type
     * @param type Tye type to match
     * @return True if the token was consumed, false if not
     */
    public boolean peekAndConsume(TokenType type) {
        Token nextTok = peek();
        if (nextTok != null && nextTok.getType() == type) {
            consume();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Gets the last token consumed by the parser
     * @return The last token consumed by the parser
     */
    public Token lastToken() { return tokenIndex <= 0 ? null : tokens.get(tokenIndex - 1); }

    /**
     * Inserts a token into the Constants symbol table
     * @param constant The token to insert
     */
    public void insertConstant(Token constant) {
        // Add to symbol Table or get Symbol Table reference
        SymbolTableRecord rec = new SymbolTableRecord(constant, "");
        if (constants.contains(rec)) rec = constants.lookup(rec);
        else constants.insert(rec);

        // Update token with symbol table record reference
        constant.setSymbolTableRecord(rec);
    }

    /**
     * Inserts a token into the Identifiers symbol table
     * @param identifier The token to insert
     */
    public void insertIdentifier(Token identifier) {

        // Add to symbol Table or get Symbol Table reference
        SymbolTableRecord rec = new SymbolTableRecord(identifier.getType(), identifier.getLexeme(), programScope.peek());
        if (identifiers.contains(rec)) rec = identifiers.lookup(rec);
        else identifiers.insert(rec);

        // Update token with symbol table record reference
        identifier.setSymbolTableRecord(rec);
    }

    /**
     * Looks for the given lexeme in the constants symbol table
     * @param lexeme The token lexeme to look up
     * @return The symbol table record stored under the lexeme name
     */
    public SymbolTableRecord lookupConstant(String lexeme) {
        return constants.lookup(lexeme);
    }

    /**
     * Looks for the given lexeme in the identifiers symbol table
     * @param lexeme The token lexeme to look up
     * @return The symbol table record stored under the lexeme name
     */
    public SymbolTableRecord lookupIdentifier(String lexeme) {
        return identifiers.lookup(programScope.peek() + lexeme);
    }

    /**
     * Searches ahead for the given token type and returns the distance of the next
     * token of that type
     * @param type The type to search for
     * @return The distance of the found token away from the next token, or -1 if not found
     */
    public int next(TokenType type) {
        for (int i = tokenIndex; i < tokens.size(); i++) {
            Token tok = tokens.get(i);
            if (tok.getType() == type) return i - tokenIndex;
        }
        return -1;
    }

    /**
     * Prints a syntax error to the output controller
     * @param message The message to write
     * @param line The line at which the error occurs
     * @param column The column at which the error occurs on the line
     */
    public void syntaxError(String message, int line, int column) {
        Error e = new Error(ErrorType.Syntactic, message, line, column);
        output.printImmediateError(e);
        syntacticallyValid = false;
    }

    public void semanticError(String message, int line, int column) {
        Error e = new Error(ErrorType.Semantic, message, line, column);
        output.printImmediateError(e);
        semanticallyValid = false;
    }

    public void pushScope(String newScope) { programScope.push(newScope); }
    public String popScope() { return programScope.pop(); }
    public boolean emptyScope() { return programScope.size() <= 2; }

    public void pushLoop(String newScope) { loopScope.push(newScope); }
    public String popLoop() { return loopScope.pop(); }
    public boolean emptyLoopStack() { return loopScope.size() <= 1; }

    public SymbolTableRecord createInvalidRecord(Token identifier) {
        // Push to the invalid scope. Since the scope starts with a #,
        // it won't be confused with a valid scope
        pushScope("#invalid");
        insertIdentifier(identifier);
        popScope();

        return identifier.getSymbolTableRecord();
    }

    public boolean validProgram() { return syntacticallyValid && semanticallyValid; }
}
