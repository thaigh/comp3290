package CD15.codegen;

import CD15.parser.ParseTreeNodeType;
import CD15.parser.SymbolTableRecord;
import CD15.parser.TreeNode;

/**
 * Generates the code for sub procedures
 *
 * @author Tyler Haigh - C3182929
 * @since 10/10/2015
 */
public class SubProc {

    /**
     * Generates the code for a sub procedure
     * @param gen The Code Generator
     * @param node The root node of the subprocedure
     */
    public static void generate(CodeGenerator gen, TreeNode node) {

        if (node == null) return;

        assert node.getNodeType() == ParseTreeNodeType.NPROCL ||
               node.getNodeType() == ParseTreeNodeType.NPROC;

        // Find whether it is a PROCL Node
        if (node.getNodeType() == ParseTreeNodeType.NPROCL) {
            generate(gen, node.getLeft());
            generate(gen, node.getRight());
        } else {
            // It is an NPROC
            TreeNode params = node.getLeft();
            Parameter.generate(gen, params);

            // Code gen for locals
            TreeNode locals = node.getMiddle();
            LocalWord.generate(gen, locals);

            // Code gen for statements
            TreeNode statements = node.getRight();
            Statement.generate(gen, statements);

            // Clear vars??

            // Generate return statement
            SymbolTableRecord procRec = node.getIdentifierRecord();
            gen.generate3Bytes(OpCode.RETN, procRec.getSubProgDescIndex());
        }


    }
}
