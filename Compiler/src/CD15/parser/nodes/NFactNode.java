package CD15.parser.nodes;

import CD15.entity.Token;
import CD15.entity.TokenType;
import CD15.parser.*;

import java.util.Stack;

/**
 * Generates a Fact Node of the form:
 * < fact>       ::= < intLit> | < realLit> | ( < bool> ) | < factId>
 * < factId>     ::= < id> < factIdTail>
 * < factIdTail> ::= ɛ | .length | [ < expr> ]
 *
 * @author Tyler Haigh - C3182929
 * @since 29/08/2015
 */
public class NFactNode {

    /**
     * Attempts to make a Fact Node given a valid token stream
     * @param p The Parser
     * @return A valid Tree Node for a valid token stream or NUNDF
     */
    public static TreeNode make(Parser p) {

        Token nextTok = p.peek();
        TreeNode fact = new TreeNode(ParseTreeNodeType.NUNDEF);

        if(nextTok.getType() == TokenType.TILIT) {
            p.consume();
            p.insertConstant(nextTok);

            fact.setNodeType(ParseTreeNodeType.NILIT);
            fact.setConstRecord(nextTok.getSymbolTableRecord());
            fact.setDataType(NodeDataType.Float);
        } else if (nextTok.getType() == TokenType.TFLIT) {
            p.consume();
            p.insertConstant(nextTok);

            fact.setNodeType(ParseTreeNodeType.NFLIT);
            fact.setConstRecord(nextTok.getSymbolTableRecord());
            fact.setDataType(NodeDataType.Float);
        } else if (nextTok.getType() == TokenType.TLPAR) {
            p.consume();

            TreeNode bool = NBoolNode.make(p);
            Token afterBool = p.peek();

            if (!p.peekAndConsume(TokenType.TRPAR)) {
                p.syntaxError("Expected matching right parenthesis ) for boolean factor expression", p.peekLine(), p.peekColumn());
                return fact;
            }

            fact = bool;
            fact.setIdentifierRecord(nextTok.getSymbolTableRecord());

            // Semantic Analysis on Bool to verify bool data type
            //
            // Since this is used to force order of operations, this can't be type checked
            //
            //if (bool.getDataType() != NodeDataType.Boolean)
            //    p.semanticError("Expected a boolean expression. Got " + bool.getDataType().name(), afterBool.getLine(), afterBool.getColumn());
            //else
            //    fact.setDataType(NodeDataType.Boolean);

        } else if (nextTok.getType() == TokenType.TIDNT) {
            fact = factId(p);
        } else {
            p.syntaxError("Invalid factor expression", p.peekLine(), p.peekColumn());
        }

        return fact;
    }

    /**
     * Generates the Fact ID and FactIdTail node for the Fact rule
     * @param p The Parser
     * @return A valid TreeNode for a valid token stream or NUNDF
     */
    private static TreeNode factId(Parser p) {

        TreeNode idNode = new TreeNode(ParseTreeNodeType.NUNDEF);
        TreeNode expr = null;
        Token startOfExpression = null;

        if (!p.peekAndConsume(TokenType.TIDNT)) {
            // Should never happen unless TIDNT is consumed accidentally
            p.syntaxError("Expected identifier name for factor expression", p.peekLine(), p.peekColumn());
            return idNode;
        }
        Token id = p.lastToken();

        Token nextTok = p.peek();
        Token lengthToken = null;
        if (nextTok.getType() == TokenType.TDOTT) {
            p.consume();

            lengthToken = p.peek();
            if (!p.peekAndConsume(TokenType.TLENG)) {
                p.syntaxError("Expected keyword 'length' in factor expression", p.peekLine(), p.peekColumn());
                return idNode;
            }

            idNode.setNodeType(ParseTreeNodeType.NLEN);

        } else if (nextTok.getType() == TokenType.TLBRK) {
            p.consume();
            startOfExpression = p.peek();
            expr = NExprNode.make(p);

            if (!p.peekAndConsume(TokenType.TRBRK)) {
                p.syntaxError("Expected right bracket ] in array variable factor expression", p.peekLine(), p.peekColumn());
                return idNode;
            }

            idNode.setNodeType(ParseTreeNodeType.NARRVAR);
            idNode.setLeft(expr);
        } else {
            idNode.setNodeType(ParseTreeNodeType.NSIMVAR);
        }

        // Semantic Analysis
        boolean validId = false;
        boolean isArray = false;
        boolean validExpression = false;

        // Look for id. Requires checking higher scopes
        SymbolTableRecord idRecord = null;
        Stack<String> scopeStack = new Stack<String>();

        boolean foundId = false;
        boolean emptyScope = false;

        while(!emptyScope && !foundId) {

            // Perform one additional lookup for the prog (global) scope
            if (p.emptyScope())
                emptyScope = true;

            if (p.lookupIdentifier(id.getLexeme()) != null) {
                foundId = true;
                validId = true;

                idRecord = p.lookupIdentifier(id.getLexeme());
                idNode.setIdentifierRecord(idRecord);

                idNode.setDataType(idRecord.getDataType());

                if (idRecord.getDataType() == NodeDataType.FloatArray)
                    isArray = true;
            } else {
                scopeStack.push(p.popScope());
            }
        }

        // Push the scopes back
        while(!scopeStack.empty()) {
            p.pushScope(scopeStack.pop());
        }

        // Check if we found the id
        if (!foundId) {
            p.semanticError("Undefined variable '" + id.getLexeme() + "'", id.getLine(), id.getColumn());
            idRecord = p.createInvalidRecord(id);
            idNode.setIdentifierRecord(idRecord);
        }

        // When passing an array to a CALL statement, the expression can be empty

        if (isArray) {
            if (expr == null)
                validExpression = false;
            else if (expr.getDataType() != NodeDataType.Float)
                p.semanticError("Invalid expression. Expected a numeric value but got "+ expr.getDataType().name(),
                        startOfExpression.getLine(), startOfExpression.getColumn());
            else
                validExpression = true;
        } else {
            if (expr != null) {
                p.semanticError(id.getLexeme() + " is not an array, but an array index was given", startOfExpression.getLine(), startOfExpression.getColumn());
            }
        }

        if (validId && validExpression)
            idNode.setDataType(NodeDataType.Float);
        else
            idNode.setDataType(idRecord.getDataType());

        // Check if we are trying to use mArray.length
        if (idNode.getNodeType() == ParseTreeNodeType.NLEN) {
            if (!isArray) {
                assert lengthToken != null;
                p.semanticError("Unable to get length of non-array type " + id.getLexeme(),
                        lengthToken.getLine(), lengthToken.getColumn());
                idNode.setDataType(NodeDataType.Undefined);
            } else
                idNode.setDataType(NodeDataType.Float);
        }

        return idNode;
    }
}
