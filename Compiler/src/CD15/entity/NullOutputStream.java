package CD15.entity;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Null output stream for use in JUnit testing
 */
public class NullOutputStream extends OutputStream {
    @Override
    public void write(int b) throws IOException {
    }
}