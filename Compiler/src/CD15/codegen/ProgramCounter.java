package CD15.codegen;

/**
 * Holds an index of the current word and byte address in a subprocedure instruction matrix
 *
 * @author Tyler Haigh - C3182929
 * @since 9/10/2015
 */
public class ProgramCounter {
    private int word;
    private int _byte;

    public static final int WORDLENGTH = 8;

    public ProgramCounter() {
        word = 0;
        _byte = 0;
    }

    public int getWord() { return word; }
    public int getByte() {
        return _byte;
    }

    public void setWord(int word) {
        this.word = word;
    }
    public void setByte(int _byte) {
        this._byte = _byte;
    }

    public void incrementWord() { word++; }
    public void incrementByte() { _byte++; }

    /**
     * Returns the byte distance between two program counters
     * @param pc The Program Counter that is ahead of this one
     * @return The distance from this Program Counter to the given one
     */
    public int distance(ProgramCounter pc) {
        int counter = 0;
        int startByte = _byte;

        // Check if we are on the same word
        if (word == pc.word) {
            // We only need to check the byte difference
            return pc._byte - _byte;
        }

        // Finish this word
        counter += WORDLENGTH - _byte - 1;

        // Loop up to the pc.word
        for (int i = 0; i < pc.word - this.word - 1; i++) {
            counter += WORDLENGTH;
        }

        // Get the final distance
        counter += pc._byte + 1;

        return counter;
    }

    @Override
    public String toString() {
        return "Word-" + word + " Byte-" + _byte;
    }
}
