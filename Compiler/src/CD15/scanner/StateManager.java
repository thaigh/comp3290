package CD15.scanner;


/**
 * Responsible for managing the transitions between Scanner states
 *
 * @author Tyler Haigh - C3182929
 * @since 5/08/2015
 */
public class StateManager {

    /**
     * Determines the next state given the current state of the scanner and a character input
     * @param c The character read
     * @param currentState The current state of the Scanner
     * @return The next state the Scanner should transition to
     */
    public static ScannerState nextState(char c, ScannerState currentState) {

        // Pre check inputs
        if (currentState == ScannerState.MultiLineComment && c != '+')
            return ScannerState.MultiLineComment;

        StringClassification type = classifyString(c);

        // Precheck type for quick calculation

        if ((currentState == ScannerState.InvalidIntLiteral || currentState == ScannerState.InvalidRealLiteral)&&
                type != StringClassification.Space &&
                type != StringClassification.NewLine &&
                type != StringClassification.Punctuation)
            return currentState;

        if (currentState == ScannerState.StringLiteral && c != '\"' && type != StringClassification.NewLine)
            return ScannerState.StringLiteral;

        if (currentState == ScannerState.SingleLineComment && type != StringClassification.NewLine)
            return ScannerState.SingleLineComment;

        // Use state machine to determine next state
        switch (type) {
            case String: {
                switch (currentState) {
                    case Initialised: return ScannerState.Identifier;

                    case Identifier: return ScannerState.Identifier;
                    case StringLiteral: return ScannerState.StringLiteral;

                    case IntegerLiteral: return ScannerState.InvalidIntLiteral;
                    case RealLiteral: return ScannerState.InvalidRealLiteral;

                    default: return ScannerState.Halt;

                }
            }
            case Numeric: {
                int val = Character.getNumericValue(c);

                switch (currentState) {
                    case Initialised: return (val == 0) ? ScannerState.ZeroLiteral : ScannerState.IntegerLiteral;
                    case ZeroLiteral: return ScannerState.Halt;

                    case Identifier: return ScannerState.Identifier;
                    case StringLiteral: return  ScannerState.StringLiteral;

                    case IntegerLiteral: return ScannerState.IntegerLiteral;
                    case RealLiteralStaging: return ScannerState.RealLiteral;
                    case RealLiteral: return ScannerState.RealLiteral;

                    default: return ScannerState.Halt;

                }
            }
            case Punctuation:

                switch (c) {
                    case ')': {
                        switch(currentState) {
                            case Initialised: return ScannerState.RightParenthesis;
                            default: return ScannerState.Halt;
                        }
                    }
                    case '(':{
                        switch(currentState) {
                            case Initialised: return ScannerState.LeftParenthesis;
                            default: return ScannerState.Halt;
                        }
                    }
                    case ',': {
                        switch(currentState) {
                            case Initialised: return ScannerState.Comma;
                            default: return ScannerState.Halt;
                        }
                    }
                    case ']': {
                        switch(currentState) {
                            case Initialised: return ScannerState.RightBracket;
                            default: return ScannerState.Halt;
                        }
                    }
                    case '[': {
                        switch(currentState) {
                            case Initialised: return ScannerState.LeftBracket;
                            default: return ScannerState.Halt;
                        }
                    }
                    case '=': {
                        switch (currentState) {
                            case Initialised: return ScannerState.Assignment;
                            case Minus: return ScannerState.MinusEquals;
                            case Multiply: return ScannerState.MultiplyEquals;
                            case Add: case CommentOrAdd:
                                return ScannerState.AddEquals;
                            case Divide: case SingleCommentOrDivide:
                                return ScannerState.DivideEquals;
                            case Assignment: return ScannerState.Equality;
                            case BangStaging: return ScannerState.BangEquals;
                            case GreaterThan: return ScannerState.GreaterThanEquals;
                            case LessThan: return ScannerState.LessThanEquals;
                            default:
                                return ScannerState.Halt;
                        }
                    }
                    case ';': {
                        switch (currentState) {
                            case Initialised: return ScannerState.SemiColon;
                            default: return ScannerState.Halt;

                        }
                    }
                    case '.': {
                        switch (currentState) {
                            case Initialised: return ScannerState.DotOperator;
                            case IntegerLiteral: return ScannerState.RealLiteralStaging;
                            case ZeroLiteral: return ScannerState.RealLiteralStaging;
                            default: return ScannerState.Halt;
                        }
                    }
                    case '/': {
                        switch (currentState) {
                            case Initialised: return ScannerState.SingleCommentOrDivide;
                            case SingleCommentOrDivide: return ScannerState.SingleLineComment;
                            case CommentOrAdd: return ScannerState.MultiLineCommentStage2;
                            case MultiLineCommentUnStage2: return ScannerState.MultiLineCommentUnStage1;

                            default: return ScannerState.Halt;
                        }
                    }
                    case '*': {
                        switch (currentState) {
                            case Initialised: return ScannerState.Multiply;
                            default: return ScannerState.Halt;
                        }
                    }
                    case '+': {
                        switch (currentState) {
                            case Initialised: return ScannerState.CommentOrAdd;
                            case MultiLineCommentStage2: return ScannerState.MultiLineComment;
                            case MultiLineComment: return ScannerState.MultiLineCommentUnStage2;
                            case MultiLineCommentUnStage1: return ScannerState.TerminateMLComment;

                            default: return ScannerState.Halt;
                        }
                    }
                    case '-': {
                        switch (currentState) {
                            case Initialised: return ScannerState.Minus;
                            default: return ScannerState.Halt;
                        }
                    }
                    case '>': {
                        switch (currentState) {
                            case Initialised: return ScannerState.GreaterThan;
                            default: return ScannerState.Halt;
                        }
                    }
                    case '<': {
                        switch (currentState) {
                            case Initialised: return ScannerState.LessThan;
                            default: return ScannerState.Halt;
                        }
                    }
                    case '\"': {
                        switch (currentState) {
                            case StringLiteral: return ScannerState.StringLiteralTerminate;
                            case Initialised:return ScannerState.StringLiteral;
                            default: return ScannerState.Halt;
                        }
                    }
                    case '!': {
                        switch (currentState) {
                            case Initialised: return ScannerState.BangStaging;
                            default: return ScannerState.Halt;
                        }
                    }
                    default: {
                        // Illegal character
                        switch (currentState) {
                            case Initialised: return ScannerState.Illegal;
                            default: return ScannerState.Halt;
                        }
                    }
                }
            case NonPrintable: {
                break;
            }
            case Space: case NewLine: {
                switch(currentState) {
                    case Initialised: case MultiLineComment:
                        return currentState;
                    default: return ScannerState.Return;
                }
            }
            case Unknown:
                break;
        }

        // Unable to determine Scanner State. You have missed something!
        return ScannerState.Illegal;

    }


    /**
     * Classify the ascii string into a group for easy processing
     * @param c The ascii character
     * @return The ascii group the character belongs to
     */
    public static StringClassification classifyString(char c) {

        // Use ascii values to determine character type
        int val = (int)c;

        // Check NewLine characters first!
        if (val == 10 || val == 13 || c == '\n' || c == '\r')
            return StringClassification.NewLine;
        else if (val == 32 || val == 9 || c == '\t')
            return StringClassification.Space;

        else if ((val >= 0 && val <= 31) || (val == 127))
            return StringClassification.NonPrintable;
        else if ((val >= 33 && val <= 47) ||
                (val >= 58 && val <= 64) ||
                (val >= 91 && val <= 96) ||
                (val >= 123 && val <= 126))
            return StringClassification.Punctuation;
        else if (val >= 48 && val <= 57)
            return StringClassification.Numeric;
        else if ((val >= 65 && val <= 90) ||
                (val >= 97 && val <= 122))
            return StringClassification.String;
        else
            return StringClassification.Unknown;
    }

}
