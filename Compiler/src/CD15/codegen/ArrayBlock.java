package CD15.codegen;

import CD15.parser.ParseTreeNodeType;
import CD15.parser.SymbolTableRecord;
import CD15.parser.TreeNode;

/**
 * Generates the declaration code for the NARRYL Node (Array Lists)
 *
 * @author Tyler Haigh - C3182929
 * @since 9/10/2015
 */
public class ArrayBlock {

    /**
     * Generates the BLOCK declarations for arrays
     * @param gen The Code Generator
     * @param node The root node to generate
     */
    public static void generate(CodeGenerator gen, TreeNode node) {

        if (node == null) return;

        assert node.getNodeType() == ParseTreeNodeType.NARRYL ||
               node.getNodeType() == ParseTreeNodeType.NARRDEC;

        //Determine if we have a single array or a list of them
        if (node.getNodeType() == ParseTreeNodeType.NARRYL) {
            generate(gen, node.getLeft());
            generate(gen, node.getRight());
        } else {
            // It is an NARRDEC
            SymbolTableRecord arrayRecord = node.getIdentifierRecord();

            TreeNode length = node.getLeft();
            assert length != null;

            SymbolTableRecord lengthRec = length.getConstRecord();
            assert lengthRec != null;

            gen.generateDecl(CodeWord.BLOCK, Integer.parseInt(lengthRec.getLexeme()), arrayRecord);
        }

    }
}
