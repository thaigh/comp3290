package CD15.codegen;

import CD15.parser.TreeNode;

/**
 * Generates the code for main method of the program
 *
 * @author Tyler Haigh - C3182929
 * @since 10/10/2015
 */
public class MainProc {

    /**
     * Generates the code for the main method of the program
     * @param gen The Code Generator
     * @param node The root node for the main
     */
    public static void generate(CodeGenerator gen, TreeNode node) {

        assert node != null;

        TreeNode locals = node.getLeft();
        TreeNode statements = node.getRight();

        LocalWord.generate(gen, locals);
        Statement.generate(gen, statements);
    }
}
