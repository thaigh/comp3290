package CD15.codegen;

import java.util.ArrayList;
import java.util.List;

/**
 * Holds a matrix of OpCodes and byte operands for a subprocedure
 *
 * @author Tyler Haigh - C3182929
 * @since 15/10/2015
 */
public class InstructionMatrix {

    private ProgramCounter pc;
    List<int[]> instructions;

    /**
     * Constructor for the Instruction Matrix. Initialises rhe matrix
     * with an empty row of bytes and Program Counter at 0,0
     */
    public InstructionMatrix() {
        pc = new ProgramCounter();
        instructions = new ArrayList<int[]>();
        instructions.add(new int[ProgramCounter.WORDLENGTH]);
    }

    /**
     * Adds a byte to the matrix and increments the program counter
     * @param _byte The Byte value of the instruction
     */
    public void addByte(int _byte) {
        int wordIndex = pc.getWord();
        int byteIndex = pc.getByte();
        instructions.get(wordIndex)[byteIndex] = _byte;

        pc.incrementByte();

        if (pc.getByte() > 7) {
            pc.incrementWord();
            pc.setByte(0);
            instructions.add(new int[ProgramCounter.WORDLENGTH]);
        }
    }

    /**
     * Retrieves the program counter for the instruction matrix
     * @return The program counter
     */
    public ProgramCounter getProgramCounter() { return this.pc; }

    /**
     * Sets the program counter for the instruction matrix
     * @param pc The new Program Counter
     */
    public void setProgramCounter(ProgramCounter pc) { this.pc = pc; }

    /**
     * Replaces a single byte at the given address in the matrix
     * @param wordIndex The word index of the matrix. Must be within bounds
     * @param byteIndex The byte index in the matrix. Must be within bounds
     * @param byteVal The byte value to replace with
     */
    public void replaceByte(int wordIndex, int byteIndex, int byteVal) {
        // Check the index values
        if (wordIndex < 0 || wordIndex > pc.getWord() ||
                byteIndex < 0 || byteIndex > ProgramCounter.WORDLENGTH)
            throw new IndexOutOfBoundsException("Word or Byte is out of bounds");

        //Replace the byte
        instructions.get(wordIndex)[byteIndex] = byteVal;
    }

    /**
     * Retrieves a byte from the instruction matrix at the given address
     * @param wordIndex The word index of the matrix. Must be within bounds
     * @param byteIndex The byte index of the matrix. Must be within bounds
     * @return The value at the given address
     */
    public int getByte(int wordIndex, int byteIndex) {
        // Check the index values
        if (wordIndex < 0 || wordIndex > pc.getWord() ||
                byteIndex < 0 || byteIndex > ProgramCounter.WORDLENGTH)
            throw new IndexOutOfBoundsException("Word or Byte is out of bounds");

        //Replace the byte
        return instructions.get(wordIndex)[byteIndex];
    }

    /**
     * Generates a formatted representation of the instruction matrix for
     * the code generator to print
     * @return A string formatted representation of the matrix
     */
    public String generate() {
        StringBuffer buffer = new StringBuffer();

        // Append the length of the matrix
        buffer.append("CODE ").append(pc.getWord()+1).append("\n");

        // Generate th matrix
        for (int i = 0; i < pc.getWord()+1; i++) {
            for (int j = 0; j < ProgramCounter.WORDLENGTH; j++) {
                int val = instructions.get(i)[j];
                buffer.append(String.format("%1$3s ", val));
            }
            buffer.append("\n");
        }

        return buffer.toString();
    }


}
