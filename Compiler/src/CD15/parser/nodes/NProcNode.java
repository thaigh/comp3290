package CD15.parser.nodes;

import CD15.entity.Token;
import CD15.entity.TokenType;
import CD15.parser.*;

/**
 * Generates a Proc node of the form:
 * < proc> ::= proc < id> < parameters> < locals> < stats> end proc < id>
 *
 * @author Tyler Haigh - C3182929
 * @since 28/08/2015
 */
public class NProcNode {

    /**
     * Attempts to make a Proc Node given a valid token stream
     * @param p The Parser
     * @return A valid Tree Node for a valid token stream or NUNDF
     */
    public static TreeNode make(Parser p) {

        TreeNode proc = new TreeNode(ParseTreeNodeType.NUNDEF);

        if (!p.peekAndConsume(TokenType.TPROC)) {
            // Should never be thrown
            p.syntaxError("Expected keyword 'proc' to begin procedure declaration", p.peekLine(), p.peekColumn());
            return proc;
        }

        if (!p.peekAndConsume(TokenType.TIDNT)) {
            p.syntaxError("Expected procedure name identifier", p.peekLine(), p.peekColumn());
            return proc;
        }
        Token firstId = p.lastToken();

        // Prevent double declaration
        SymbolTableRecord rec = p.lookupIdentifier(firstId.getLexeme());
        if (rec != null) {
            p.semanticError("Proc '" + firstId.getLexeme() + "' is already defined as a " + rec.getDataType(),
                    firstId.getLine(), firstId.getColumn());
            p.createInvalidRecord(firstId);
        } else {
            p.insertIdentifier(firstId);
        }

        // Push scope
        p.pushScope(firstId.getLexeme());

        TreeNode parameters = SParamsNode.make(p);
        TreeNode locals = SLocalsNode.make(p);
        TreeNode stats = SStatsNode.make(p);

        if (!p.peekAndConsume(TokenType.TENDK)) {
            p.syntaxError("Expected keyword 'end' to close procedure declaration", p.peekLine(), p.peekColumn());
            p.popScope();
            return proc;
        }
        if (!p.peekAndConsume(TokenType.TPROC)) {
            p.syntaxError("Expected keyword 'proc' to close procedure declaration", p.peekLine(), p.peekColumn());
            p.popScope();
            return proc;
        }

        if (!p.peekAndConsume(TokenType.TIDNT)) {
            p.syntaxError("Expected procedure name identifier", p.peekLine(), p.peekColumn());
            p.popScope();
            return proc;
        }

        // Pop scope back to global prog
        p.popScope();
        Token secondId = p.lastToken();
        p.insertIdentifier(secondId);


        // All good
        proc = new TreeNode(ParseTreeNodeType.NPROC, parameters, locals, stats);
        proc.setIdentifierRecord(firstId.getSymbolTableRecord());

        // Semantic analysis on ids
        if (firstId.getSymbolTableRecord().equals(secondId.getSymbolTableRecord())) {
            proc.setDataType(NodeDataType.Procedure);
            proc.getIdentifierRecord().setTreeNode(proc);
        } else
            p.semanticError("Procedure Ids do not match", secondId.getLine(), secondId.getColumn());

        return proc;
    }
}
