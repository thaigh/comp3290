package CD15.parser.nodes;

import CD15.entity.Token;
import CD15.entity.TokenType;
import CD15.parser.NodeDataType;
import CD15.parser.ParseTreeNodeType;
import CD15.parser.Parser;
import CD15.parser.TreeNode;

/**
 * Generates an Array Declaration node of the form
 * < arrayDecl> ::= < id> [ < intLit> ]
 *
 * @author Tyler Haigh - C3182929
 * @since 27/08/2015
 */
public class NArrDecNode {

    /**
     * Attempts to generate a ArrayDecl Node
     * @param p The Parser
     * @return A valid Tree Node for a valid ArrayDecl node or NUNDF
     */
    public static TreeNode make(Parser p) {

        TreeNode arrDecl = new TreeNode(ParseTreeNodeType.NUNDEF);

        if (!p.peekAndConsume(TokenType.TIDNT)) {
            p.syntaxError("Expected identifier for Array Declaration", p.peekLine(), p.peekColumn());
            return arrDecl;
        }
        Token id = p.lastToken();

        // Prevent double declaration
        if (p.lookupIdentifier(id.getLexeme()) != null) {
            p.semanticError("Array '" + id.getLexeme() + "' is already defined", id.getLine(), id.getColumn());
            p.createInvalidRecord(id);
        } else {
            p.insertIdentifier(id);
        }

        if (!p.peekAndConsume(TokenType.TLBRK)) {
            p.syntaxError("Expected left bracket [ in Array Declaration", p.peekLine(), p.peekColumn());
            return arrDecl;
        }

        if (!p.peekAndConsume(TokenType.TILIT)) {
            p.syntaxError("Array Declaration requires a integer length", p.peekLine(), p.peekColumn());
            return arrDecl;
        }
        Token intLit = p.lastToken();
        p.insertConstant(intLit);

        if (!p.peekAndConsume(TokenType.TRBRK)) {
            p.syntaxError("Expected right bracket ] in Array Declaration", p.peekLine(), p.peekColumn());
            return arrDecl;
        }

        // All is good
        TreeNode intLitNode = new TreeNode(ParseTreeNodeType.NILIT);
        intLitNode.setConstRecord(intLit.getSymbolTableRecord());
        intLitNode.setDataType(NodeDataType.Float);

        arrDecl.setNodeType(ParseTreeNodeType.NARRDEC);
        arrDecl.setIdentifierRecord(id.getSymbolTableRecord());
        arrDecl.setLeft(intLitNode);
        arrDecl.setDataType(NodeDataType.FloatArray);

        // Give the Symbol Table Record a link to the node so we can verify data types later
        id.getSymbolTableRecord().setTreeNode(arrDecl);

        return arrDecl;
    }
}
