package test;

import CD15.entity.Token;
import CD15.entity.TokenType;
import CD15.parser.NodeDataType;
import CD15.parser.ParseTreeNodeType;
import CD15.parser.Parser;
import CD15.parser.TreeNode;
import CD15.scanner.Scanner;
import junit.framework.Assert;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

/** 
* InputController Tester. 
* 
* @author <Authors name> 
* @since <pre>Aug 15, 2015</pre> 
* @version 1.0 
*/ 
public class ParserTests {


    /**
     * Method: processInput(String input)
     * Testing for removal of \r\n to only \n
     */
    @Test
    public void testParseProgram() throws Exception {
        String input = "program test end program test";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) {tokens.add(sc.nextValidToken());}
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual= p.parse();

        p.pushScope("prog");
        TreeNode prog = new TreeNode(ParseTreeNodeType.NPROG, p.lookupIdentifier("test"));
        TreeNode main = new TreeNode(ParseTreeNodeType.NUNDEF);

        prog.setRight(main);
        prog.setDataType(NodeDataType.Program);

        Assert.assertTrue(prog.equals(actual));

    }

    @Test
    public void testParseArrays() throws Exception {
        String input = "program test \n" +
                "    arrays mArr[0], arrx[1], arry[2]\n" +
                "end program test";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) {tokens.add(sc.nextValidToken());}
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual= p.parse();

        p.pushScope("prog");
        TreeNode prog = new TreeNode(ParseTreeNodeType.NPROG, p.lookupIdentifier("test"));

        TreeNode arrList = new TreeNode(ParseTreeNodeType.NARRYL);
        TreeNode arrList2 = new TreeNode(ParseTreeNodeType.NARRYL);

        TreeNode mArrDecl = new TreeNode(ParseTreeNodeType.NARRDEC, p.lookupIdentifier("mArr"));
        TreeNode arrXDecl = new TreeNode(ParseTreeNodeType.NARRDEC, p.lookupIdentifier("arrx"));
        TreeNode arrYDecl = new TreeNode(ParseTreeNodeType.NARRDEC, p.lookupIdentifier("arry"));

        TreeNode zero = new TreeNode(ParseTreeNodeType.NILIT);
        TreeNode one = new TreeNode(ParseTreeNodeType.NILIT);
        TreeNode two = new TreeNode(ParseTreeNodeType.NILIT);

        zero.setConstRecord(p.lookupConstant("0"));
        one.setConstRecord(p.lookupConstant("1"));
        two.setConstRecord(p.lookupConstant("2"));

        zero.setDataType(NodeDataType.Float);
        one.setDataType(NodeDataType.Float);
        two.setDataType(NodeDataType.Float);

        mArrDecl.setLeft(zero);
        arrXDecl.setLeft(one);
        arrYDecl.setLeft(two);
        mArrDecl.setDataType(NodeDataType.FloatArray);
        arrXDecl.setDataType(NodeDataType.FloatArray);
        arrYDecl.setDataType(NodeDataType.FloatArray);

        arrList2.setLeft(arrXDecl);
        arrList2.setRight(arrYDecl);
        arrList.setLeft(mArrDecl);
        arrList.setRight(arrList2);

        arrList.setDataType(NodeDataType.Node);
        arrList2.setDataType(NodeDataType.Node);

        prog.setLeft(arrList);

        TreeNode main = new TreeNode(ParseTreeNodeType.NUNDEF);

        prog.setRight(main);
        prog.setDataType(NodeDataType.Program);

        Assert.assertTrue(prog.equals(actual));

    }

    @Test
    public void testParseProcs() throws Exception {
        String input = "program test \n" +
                "    proc p end proc p\n" +
                "    proc p2 end proc p2\n" +
                "end program test";
        List<Token> tokens = new LinkedList<Token>();

        // Get Tokens
        Scanner sc = new Scanner(input);
        while (!sc.isFinished()) {tokens.add(sc.nextValidToken());}
        tokens.add(new Token(TokenType.TEOF, null, -1, -1));

        // Parse tokens
        Parser p = new Parser(tokens);
        TreeNode actual= p.parse();

        p.pushScope("prog");
        TreeNode prog = new TreeNode(ParseTreeNodeType.NPROG, p.lookupIdentifier("test"));

        TreeNode procList1 = new TreeNode(ParseTreeNodeType.NPROCL, null);
        TreeNode proc = new TreeNode(ParseTreeNodeType.NPROC, p.lookupIdentifier("p"));
        TreeNode proc2 = new TreeNode(ParseTreeNodeType.NPROC, p.lookupIdentifier("p2"));

        procList1.setDataType(NodeDataType.Node);
        proc.setDataType(NodeDataType.Procedure);
        proc2.setDataType(NodeDataType.Procedure);

        TreeNode undfP1 = new TreeNode(ParseTreeNodeType.NUNDEF);
        TreeNode undfP2 = new TreeNode(ParseTreeNodeType.NUNDEF);

        TreeNode main = new TreeNode(ParseTreeNodeType.NUNDEF);

        prog.setMiddle(procList1);
        prog.setRight(main);

        procList1.setLeft(proc);
        procList1.setRight(proc2);

        proc.setLeft(null);
        proc.setMiddle(null);
        proc.setRight(undfP1);

        proc2.setLeft(null);
        proc2.setMiddle(null);
        proc2.setRight(undfP2);

        prog.setDataType(NodeDataType.Program);

        Assert.assertTrue(prog.equals(actual));
    }

} 
