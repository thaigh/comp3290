package CD15.codegen;

import CD15.parser.ParseTreeNodeType;
import CD15.parser.SymbolTableRecord;
import CD15.parser.TreeNode;

/**
 * Generates the code to calculate an expression node
 *
 * @author Tyler Haigh - C3182929
 * @since 10/10/2015
 */
public class Expression {

    /**
     * Generates the code for the expression
     * @param gen The code generator
     * @param node The root node for the expression
     * @return An AC15 memory address for the result of the computed expression
     */
    public static int generate(CodeGenerator gen, TreeNode node) {

        if (node == null) return -1;

        int temp1 = -1;
        int temp2 = -1;
        int result = -1;

        // Evaluate the left side
        // Check for NARRVAR
        if (node.getNodeType() != ParseTreeNodeType.NARRVAR && node.getLeft() != null) {
            // Store the left side in a temp1
            temp1 = generate(gen, node.getLeft());
        }
        else {
            SymbolTableRecord rec = getRecord(node);

            // Check if it's a .length expression
            if (node.getNodeType() == ParseTreeNodeType.NLEN) {
                //rec = node.getIdentifierRecord().getTreeNode().getLeft().getConstRecord();
                gen.generate5Bytes(OpCode.GET, node.getIdentifierRecord().getArraySizeLocation());
            }

            // Check if we are getting from an array
            else if (node.getNodeType() == ParseTreeNodeType.NARRVAR) {
                // Get the array index
                // Evaluate the expression
                int temp3 = generate(gen, node.getLeft());

                // Load the index register
                gen.generate5Bytes(OpCode.LDIDX, temp3);

                // Get the array value. The array Descriptor index will be -1 if we are in a proc
                int arrayDescIdx = node.getIdentifierRecord().getArrayDescIndex();
                if (arrayDescIdx == -1)
                    gen.generate5Bytes(OpCode.GET, node.getIdentifierRecord().getArrayDescLocation());
                else
                    gen.generate3Bytes(OpCode.GET_D, arrayDescIdx);

                // Release temp3
                gen.releaseTemp(temp3);
            } else {
                gen.generate5Bytes(OpCode.GET, rec.getWordIndex());
            }

            temp1 = gen.getNextTemp();
            gen.generate5Bytes(OpCode.STO, temp1);
            return temp1;
        }

        // Evaluate the right side
        // Check for NARRVAR
        if (node.getNodeType() != ParseTreeNodeType.NARRVAR && node.getRight() != null) {
            // Store the right side in a temp2
            temp2 = generate(gen, node.getRight());
        }
        else {
            SymbolTableRecord rec = getRecord(node);

            // Check if it's a .length expression
            if (node.getNodeType() == ParseTreeNodeType.NLEN) {
                //rec = node.getIdentifierRecord().getTreeNode().getLeft().getConstRecord();
                gen.generate5Bytes(OpCode.GET, node.getIdentifierRecord().getArraySizeLocation());
            }

            // Check if we are getting from an array
            else if (node.getNodeType() == ParseTreeNodeType.NARRVAR) {
                // Get the array index
                // Evaluate the expression
                int temp3 = generate(gen, node.getLeft());

                // Load the index register
                gen.generate5Bytes(OpCode.LDIDX, temp3);

                // Get the array value. The array Descriptor index will be -1 if we are in a proc
                int arrayDescIdx = node.getIdentifierRecord().getArrayDescIndex();
                if (arrayDescIdx == -1)
                    gen.generate5Bytes(OpCode.GET, node.getIdentifierRecord().getArrayDescLocation());
                else
                    gen.generate3Bytes(OpCode.GET_D, arrayDescIdx);

                // Release temp3
                gen.releaseTemp(temp3);
            } else {
                gen.generate5Bytes(OpCode.GET, rec.getWordIndex());
            }

            temp2 = gen.getNextTemp();
            gen.generate5Bytes(OpCode.STO, temp2);
            return temp2;
        }

        // TODO: Handle null (-1) node temp references?

        // Perform the parent operation
        gen.generate5Bytes(OpCode.GET, temp1);
        switch (node.getNodeType()) {
            // Terms and Facts
            case NADD: gen.generate5Bytes(OpCode.ADD, temp2); break;
            case NMUL: gen.generate5Bytes(OpCode.MUL, temp2); break;
            case NSUB: gen.generate5Bytes(OpCode.SUB, temp2); break;
            case NDIV: gen.generate5Bytes(OpCode.DIV, temp2); break;
            case NIDIV: gen.generate5Bytes(OpCode.IDIV, temp2); break;

            // Relops
            case NEQL:
                gen.generate5Bytes(OpCode.SUB, temp2);
                gen.generate1Byte(OpCode.EQL);
                break;
            case NNEQ:
                gen.generate5Bytes(OpCode.SUB, temp2);
                gen.generate1Byte(OpCode.NEQ);
                break;
            case NGTR:
                gen.generate5Bytes(OpCode.SUB, temp2);
                gen.generate1Byte(OpCode.GRT);
                break;
            case NLEQ:
                gen.generate5Bytes(OpCode.SUB, temp2);
                gen.generate1Byte(OpCode.LEQ);
                break;
            case NLESS:
                gen.generate5Bytes(OpCode.SUB, temp2);
                gen.generate1Byte(OpCode.LSS);
                break;
            case NGEQ:
                gen.generate5Bytes(OpCode.SUB, temp2);
                gen.generate1Byte(OpCode.GEQ);
                break;

            // LogOps
            case NAND: gen.generate5Bytes(OpCode.AND, temp2); break;
            case NOR: gen.generate5Bytes(OpCode.OR, temp2); break;
            case NXOR: gen.generate5Bytes(OpCode.XOR, temp2); break;
        }

        // Release the temp variables
        gen.releaseTemp(temp1);
        gen.releaseTemp(temp2);

        // Store the result. Should be in temp1 after deallocation
        result = gen.getNextTemp();
        gen.generate5Bytes(OpCode.STO, result);
        return result;
    }

    /**
     * Gets the Symbol table record for a TreeNode
     * @param node The node
     * @return The Symbol table record for the node
     */
    private static SymbolTableRecord getRecord(TreeNode node) {
        // Determine if it's a constant or an identifier
        SymbolTableRecord rec;
        ParseTreeNodeType nodeType = node.getNodeType();

        if (nodeType == ParseTreeNodeType.NILIT ||
                nodeType == ParseTreeNodeType.NFLIT ||
                nodeType == ParseTreeNodeType.NSTRG)
            rec = node.getConstRecord();
        else
            rec = node.getIdentifierRecord();

        return rec;
    }
}
