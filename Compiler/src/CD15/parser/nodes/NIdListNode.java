package CD15.parser.nodes;

import CD15.entity.Token;
import CD15.entity.TokenType;
import CD15.parser.NodeDataType;
import CD15.parser.ParseTreeNodeType;
import CD15.parser.Parser;
import CD15.parser.TreeNode;

/**
 * Generates an ID List Node for the form:
 * < idList>        ::= < id> < idListTail>
 * < idListTail>    ::= ɛ | , < idList>
 *
 * @author Tyler Haigh - C3182929
 * @since 28/08/2015
 */
public class NIdListNode {

    /**
     * Attempts to generate an ID List node for a valid token stream
     * @param p The Parser
     * @return A valid Tree Node for a valid token stream or NUNDF
     */
    public static TreeNode make(Parser p) {

        TreeNode idNode = new TreeNode(ParseTreeNodeType.NUNDEF);

        Token id = p.peek();
        if (id.getType() != TokenType.TIDNT) {
            p.syntaxError("Expected identifier name in identifier list", p.peekLine(), p.peekColumn());

            try { errorRecovery(p); }
            catch (Exception e) { return idNode; }
        } else {
            p.consume();

            // Prevent double declaration
            if (p.lookupIdentifier(id.getLexeme()) != null) {
                p.semanticError("Invalid variable declaration for '" + id.getLexeme() +"'. It is already defined",
                        id.getLine(), id.getColumn());
                p.createInvalidRecord(id);
            } else {
                p.insertIdentifier(id);
            }

            idNode = new TreeNode(ParseTreeNodeType.NSIMDEC);
            idNode.setIdentifierRecord(id.getSymbolTableRecord());
            idNode.setDataType(NodeDataType.Float);
            id.getSymbolTableRecord().setTreeNode(idNode);

        }

        TreeNode tail = tail(p);
        if (tail == null) {
            return idNode;
        } else {
            TreeNode idList = new TreeNode(ParseTreeNodeType.NIDLST, idNode, tail);
            idList.setDataType(NodeDataType.Node);
            return idList;
        }
    }

    /**
     * Generates the Tail of the ID List node
     * @param p The Parser
     * @return Null if there is no following ID List, or an ID List Tree Node
     */
    private static TreeNode tail(Parser p) {

        Token nextTok = p.peek();
        if (nextTok.getType() != TokenType.TCOMA) return null;
        else {
            p.consume();

            TreeNode idList = make(p);
            return idList;
        }
    }

    /**
     * Attempts error recovery in the event of an invalid token stream
     * @param p The Parser
     * @throws Exception Unable to recover
     */
    private static void errorRecovery(Parser p) throws Exception {
        // Find next comma or semi
        int nextComma = p.next(TokenType.TCOMA);
        int nextSemi = p.next(TokenType.TSEMI);

        if (nextComma > -1 && nextComma < 5) p.consume(nextComma);
        else if (nextSemi > -1) p.consume(nextSemi);
        else throw new Exception("Unable to recover");
    }
}
