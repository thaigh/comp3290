package CD15.parser.nodes;

import CD15.entity.Token;
import CD15.entity.TokenType;
import CD15.parser.NodeDataType;
import CD15.parser.ParseTreeNodeType;
import CD15.parser.Parser;
import CD15.parser.TreeNode;

/**
 * Created by Administrator on 27/08/2015.
 *
 * < arrays> ::= ɛ | arrays < arrayDecl> < arraysTail>
 * < arraysTail>    ::= ɛ | , < arrayDecl> < arraysTail>
 */
public class NArrlNode {

    /**
     * Attempts to generate an NARRYL node
     * @param p The Parser
     * @return A valid TreeNode for the a valid array list or NUNDF
     */
    public static TreeNode make(Parser p) {

        // Check if we need to take epsilon rule
        Token nextTok = p.peek();
        if (nextTok.getType() != TokenType.TARRS) return null;
        else {
            p.consume();

            TreeNode arrayDecl = NArrDecNode.make(p);

            // Error Checking
            if (arrayDecl != null && arrayDecl.getNodeType() == ParseTreeNodeType.NUNDEF) {
                try {errorRecovery(p); }
                catch (Exception e) { return new TreeNode(ParseTreeNodeType.NUNDEF); }
            }

            TreeNode arrayTail = arrayListTail(p);

            // Return valid node based on whether it is a list or not
            if (arrayTail == null) return arrayDecl;
            else {
                TreeNode arrayList = new TreeNode(ParseTreeNodeType.NARRYL, arrayDecl, arrayTail);
                arrayList.setDataType(NodeDataType.Node);
                return arrayList;
            }
        }
    }

    /**
     * Attempts to generate the tail of the arrays node
     * @param p The Parser
     * @return Either null if there is no tail, NARRDEC for a single array declaration
     *         or NARRYL for a list of array declarations
     */
    private static TreeNode arrayListTail(Parser p) {

        // ɛ or ,
        Token nextTok = p.peek();
        if (nextTok.getType() != TokenType.TCOMA) return null;
        else {
            p.consume();

            TreeNode arrayDecl = NArrDecNode.make(p);

            // Error Checking
            if (arrayDecl != null && arrayDecl.getNodeType() == ParseTreeNodeType.NUNDEF) {
                try {errorRecovery(p); }
                catch (Exception e) { return new TreeNode(ParseTreeNodeType.NUNDEF); }
            }

            TreeNode arrayTail = arrayListTail(p);

            // Return valid node based on whether it is a list or not
            if (arrayTail == null) return arrayDecl;
            else {
                TreeNode arrayList = new TreeNode(ParseTreeNodeType.NARRYL, arrayDecl, arrayTail);
                arrayList.setDataType(NodeDataType.Node);
                return arrayList;
            }
        }
    }

    /**
     * The error recovery routine to recover within the arrays node
     * @param p The Parser
     * @throws Exception Unable to perform error recovery
     */
    private static void errorRecovery(Parser p) throws Exception {
        int nextComma = p.next(TokenType.TCOMA);
        if (nextComma > -1 && nextComma < 3)
            p.consume(nextComma);
        else throw new Exception("Unable to recover");
    }
}
