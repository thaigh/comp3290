package CD15.parser;

/**
 * Defines the syntax tree node value types for the Parser's Syntax Tree
 *
 * @author M. Hannaford
 * @since 21/8/2018
 */
public enum ParseTreeNodeType {
	/** Undefined*/NUNDEF,
    /** Program*/NPROG,
    /** Array List*/NARRYL,
    /** Proc List*/NPROCL,
    /** Main*/NMAIN,
    /** Proc*/NPROC,

    /** Params*/NPARAMS,

    /** Parameter List*/NPLIST,
    /** Simple Parameter*/NSIMPAR,
    /** Array Parameter*/NARRPAR,
	/** Declaration List*/NDLIST,
    /** Simple Declaration*/NSIMDEC,
    /** Array Declaration*/NARRDEC,
    /** Array Variable*/NARRVAR,
    /** Simple Variable*/NSIMVAR,

    /** Statement List*/NSLIST,
    /** Loop Statement*/NLOOP,
    /** Exit Statement*/NEXIT,
    /** If Statement*/NIFT,
    /** If-Then-Else Statement*/NIFTE,
    /** Input Statement*/NINPUT,
    /** Print Statement*/NPRINT,
    /** Printline Statement*/NPRLN,
    /** Call Statement*/NCALL,
    /** Expression List*/NELIST,

    /** Assign Statement*/NASGN,
    /** Plus-Equals Statement*/NPLEQ,
    /** Minus-Equals Statement*/NMNEQ,
    /** Star-Equals Statement*/NSTEQ,
    /** Divide-Equals Statement*/NDVEQ,

    /** Add Expression*/NADD,
    /** Subtract Expression*/NSUB,
    /** Multiply Expression*/NMUL,
    /** Divide Expression*/NDIV,
    /** Modulo-Division Expression*/NIDIV,

    /** NOT Bool Relation*/NNOT,
    /** AND Bool Relation*/NAND,
    /** NOR Bool Relation*/NOR,
    /** XOR Bool Relation*/NXOR,
    /** Equals Bool Relation*/NEQL,
    /** Not-Equals Bool Relation*/NNEQ,
    /** Greater-Than Bool Relation*/NGTR,
    /** Less-Than Bool Relation*/NLESS,
    /** Greater-Than-Equals Bool Relation*/NGEQ,
    /** Less-Than-Equals Bool Relation*/NLEQ,

    /** Integer Literal*/NILIT,
    /** Floating-Point Literal*/NFLIT,
    /** ID List*/NIDLST,
    /** Var List*/NVLIST,
    /** Print List*/NPRLIST,
    /** Length*/NLEN,
    /** String Literal*/NSTRG


};
