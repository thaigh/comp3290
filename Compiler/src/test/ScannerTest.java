package test;

import CD15.entity.Token;
import CD15.entity.TokenType;
import CD15.scanner.Scanner;
import junit.framework.Assert;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

/**
* Scanner Tester. 
* 
* @author Tyler Haigh
* @since <pre>Aug 2, 2015</pre> 
* @version 1.0 
*/ 
public class ScannerTest { 

    /**
    * Method: nextToken(String line)
    */
    @Test
    public void testNextToken_keywords() throws Exception {

        String inputs = "program end arrays proc var val \t   \n" +
                "local loop exit when call with if then else elsif \r\n" +
                "input print printline not and or xor div length";

        List<Token> tokens = new LinkedList<Token>() {{
            add(new Token(TokenType.TPROG, null, 1, 1)); add(new Token(TokenType.TENDK, null, 1, 9));
            add(new Token(TokenType.TARRS, null, 1, 13)); add(new Token(TokenType.TPROC, null, 1, 20));
            add(new Token(TokenType.TVARP, null, 1, 25)); add(new Token(TokenType.TVALP, null, 1, 29));

            add(new Token(TokenType.TLOCL, null, 2, 1)); add(new Token(TokenType.TLOOP, null, 2, 7));
            add(new Token(TokenType.TEXIT, null, 2, 12)); add(new Token(TokenType.TWHEN, null, 2, 17));
            add(new Token(TokenType.TCALL, null, 2, 22)); add(new Token(TokenType.TWITH, null, 2, 27));
            add(new Token(TokenType.TIFKW, null, 2, 32)); add(new Token(TokenType.TTHEN, null, 2, 35));
            add(new Token(TokenType.TELSE, null, 2, 40)); add(new Token(TokenType.TELSF, null, 2, 45));

            add(new Token(TokenType.TINPT, null, 3, 1)); add(new Token(TokenType.TPRIN, null, 3, 7));
            add(new Token(TokenType.TPRLN, null, 3, 13)); add(new Token(TokenType.TNOTK, null, 3, 23));
            add(new Token(TokenType.TANDK, null, 3, 27)); add(new Token(TokenType.TORKW, null, 3, 31));
            add(new Token(TokenType.TXORK, null, 3, 34)); add(new Token(TokenType.TIDIV, null, 3, 38));
            add(new Token(TokenType.TLENG, null, 3, 42)); add(new Token(TokenType.TEOF, null, 3, 48));
        }};

        Scanner sc = new Scanner(inputs);
        int i = 0;
        while(!sc.isFinished()) {
            Token tok = sc.nextValidToken();

            if (tok != null) {
                Assert.assertEquals(tokens.get(i).getType(), tok.getType());
                Assert.assertEquals(tokens.get(i).getLexeme(), tok.getLexeme());
                Assert.assertEquals(tokens.get(i).getLine(), tok.getLine());
                Assert.assertEquals(tokens.get(i).getColumn(), tok.getColumn());
                i++;
            }
        }
    }

    @Test
    public void testNextToken_mixedKeywordsSymbols() throws Exception {

        String inputs = "program]end[arrays)proc(var;val,\t.\n" +
                "=local+=loop-=exit*=when/=call==with!=if>then<=else<elsif>=\r\n" +
                "+input-print*printline/not&and~or@xor div length";

        List<Token> tokens = new LinkedList<Token>() {{
            add(new Token(TokenType.TPROG, null, 1, 1)); add(new Token(TokenType.TRBRK, null, 1, 8));
            add(new Token(TokenType.TENDK, null, 1, 9)); add(new Token(TokenType.TLBRK, null, 1, 12));
            add(new Token(TokenType.TARRS, null, 1, 13)); add(new Token(TokenType.TRPAR, null, 1, 19));
            add(new Token(TokenType.TPROC, null, 1, 20)); add(new Token(TokenType.TLPAR, null, 1, 24));
            add(new Token(TokenType.TVARP, null, 1, 25)); add(new Token(TokenType.TSEMI, null, 1, 28));
            add(new Token(TokenType.TVALP, null, 1, 29)); add(new Token(TokenType.TCOMA, null, 1, 32));
            add(new Token(TokenType.TDOTT, null, 1, 34));

            add(new Token(TokenType.TASGN, null, 2, 1));
            add(new Token(TokenType.TLOCL, null, 2, 2)); add(new Token(TokenType.TPLEQ, null, 2, 7));
            add(new Token(TokenType.TLOOP, null, 2, 9)); add(new Token(TokenType.TMNEQ, null, 2, 13));
            add(new Token(TokenType.TEXIT, null, 2, 15)); add(new Token(TokenType.TMLEQ, null, 2, 19));
            add(new Token(TokenType.TWHEN, null, 2, 21)); add(new Token(TokenType.TDVEQ, null, 2, 25));
            add(new Token(TokenType.TCALL, null, 2, 27));
            add(new Token(TokenType.TDEQL, null, 2, 31)); add(new Token(TokenType.TWITH, null, 2, 33));
            add(new Token(TokenType.TNEQL, null, 2, 37)); add(new Token(TokenType.TIFKW, null, 2, 39));
            add(new Token(TokenType.TGRTR, null, 2, 41)); add(new Token(TokenType.TTHEN, null, 2, 42));
            add(new Token(TokenType.TLEQL, null, 2, 46)); add(new Token(TokenType.TELSE, null, 2, 48));
            add(new Token(TokenType.TLESS, null, 2, 52)); add(new Token(TokenType.TELSF, null, 2, 53));
            add(new Token(TokenType.TGREQ, null, 2, 58));

            add(new Token(TokenType.TPLUS, null, 3, 1));
            add(new Token(TokenType.TINPT, null, 3, 2)); add(new Token(TokenType.TSUBT, null, 3, 7));
            add(new Token(TokenType.TPRIN, null, 3, 8)); add(new Token(TokenType.TMULT, null, 3, 13));
            add(new Token(TokenType.TPRLN, null, 3, 14)); add(new Token(TokenType.TDIVD, null, 3, 23));
            add(new Token(TokenType.TNOTK, null, 3, 24)); add(new Token(TokenType.TUNDF, "&", 3, 27));
            add(new Token(TokenType.TANDK, null, 3, 28)); add(new Token(TokenType.TUNDF, "~", 3, 31));
            add(new Token(TokenType.TORKW, null, 3, 32)); add(new Token(TokenType.TUNDF, "@", 3, 34));
            add(new Token(TokenType.TXORK, null, 3, 35)); add(new Token(TokenType.TIDIV, null, 3, 39));
            add(new Token(TokenType.TLENG, null, 3, 43)); add(new Token(TokenType.TEOF, null, 3, 49));

        }};

        Scanner sc = new Scanner(inputs);
        int i = 0;
        while(!sc.isFinished()) {
            Token tok = sc.nextValidToken();

            if (tok != null) {
                Assert.assertEquals(tokens.get(i).getType(), tok.getType());
                Assert.assertEquals(tokens.get(i).getLexeme(), tok.getLexeme());
                Assert.assertEquals(tokens.get(i).getLine(), tok.getLine());
                Assert.assertEquals(tokens.get(i).getColumn(), tok.getColumn());
                i++;
            }
        }
    }

    @Test
    public void testNextToken_specialSymbols() throws Exception {

        String inputs = "] [ ) ( ; , . = += -= *= /= == != " +
                "> <= < >= + - * /";

        List<Token> tokens = new LinkedList<Token>() {{
            add(new Token(TokenType.TRBRK, null, 1, 1));
            add(new Token(TokenType.TLBRK, null, 1, 3));
            add(new Token(TokenType.TRPAR, null, 1, 5));
            add(new Token(TokenType.TLPAR, null, 1, 7));
            add(new Token(TokenType.TSEMI, null, 1, 9));
            add(new Token(TokenType.TCOMA, null, 1, 11));
            add(new Token(TokenType.TDOTT, null, 1, 13));
            add(new Token(TokenType.TASGN, null, 1, 15));
            add(new Token(TokenType.TPLEQ, null, 1, 17));
            add(new Token(TokenType.TMNEQ, null, 1, 20));
            add(new Token(TokenType.TMLEQ, null, 1, 23));
            add(new Token(TokenType.TDVEQ, null, 1, 26));
            add(new Token(TokenType.TDEQL, null, 1, 29));
            add(new Token(TokenType.TNEQL, null, 1, 32));
            add(new Token(TokenType.TGRTR, null, 1, 35));
            add(new Token(TokenType.TLEQL, null, 1, 37));
            add(new Token(TokenType.TLESS, null, 1, 40));
            add(new Token(TokenType.TGREQ, null, 1, 42));
            add(new Token(TokenType.TPLUS, null, 1, 45));
            add(new Token(TokenType.TSUBT, null, 1, 47));
            add(new Token(TokenType.TMULT, null, 1, 49));
            add(new Token(TokenType.TDIVD, null, 1, 51));
            add(new Token(TokenType.TEOF, null, 1, 52));
        }};

        Scanner sc = new Scanner(inputs);
        int i = 0;
        while(!sc.isFinished()) {
            Token tok = sc.nextValidToken();

            if (tok != null) {
                Assert.assertEquals(tokens.get(i).getType(), tok.getType());
                Assert.assertEquals(tokens.get(i).getLexeme(), tok.getLexeme());
                Assert.assertEquals(tokens.get(i).getLine(), tok.getLine());
                Assert.assertEquals(tokens.get(i).getColumn(), tok.getColumn());
                i++;
            }
        }
    }

    @Test
    public void testNextToken_repeatedCharacters() throws Exception {

        String inputs = "]]] [[[ ))) ((( ;;; ,,, ... === +++= ---= ***= / /= !!!= " +
                ">>> <<= <=> <<< >>= >=< +++ --- *** /// anything here!";

        List<Token> tokens = new LinkedList<Token>() {{

            add(new Token(TokenType.TRBRK, null, 1, 1)); add(new Token(TokenType.TRBRK, null, 1, 2)); add(new Token(TokenType.TRBRK, null, 1, 3));
            add(new Token(TokenType.TLBRK, null, 1, 5)); add(new Token(TokenType.TLBRK, null, 1, 6)); add(new Token(TokenType.TLBRK, null, 1, 7));
            add(new Token(TokenType.TRPAR, null, 1, 9)); add(new Token(TokenType.TRPAR, null, 1, 10)); add(new Token(TokenType.TRPAR, null, 1, 11));
            add(new Token(TokenType.TLPAR, null, 1, 13)); add(new Token(TokenType.TLPAR, null, 1, 14)); add(new Token(TokenType.TLPAR, null, 1, 15));
            add(new Token(TokenType.TSEMI, null, 1, 17)); add(new Token(TokenType.TSEMI, null, 1, 18)); add(new Token(TokenType.TSEMI, null, 1, 19));
            add(new Token(TokenType.TCOMA, null, 1, 21)); add(new Token(TokenType.TCOMA, null, 1, 22)); add(new Token(TokenType.TCOMA, null, 1, 23));
            add(new Token(TokenType.TDOTT, null, 1, 25)); add(new Token(TokenType.TDOTT, null, 1, 26)); add(new Token(TokenType.TDOTT, null, 1, 27));
            add(new Token(TokenType.TDEQL, null, 1, 29)); add(new Token(TokenType.TASGN, null, 1, 31));
            add(new Token(TokenType.TPLUS, null, 1, 33)); add(new Token(TokenType.TPLUS, null, 1, 34)); add(new Token(TokenType.TPLEQ, null, 1, 35));
            add(new Token(TokenType.TSUBT, null, 1, 38)); add(new Token(TokenType.TSUBT, null, 1, 39)); add(new Token(TokenType.TMNEQ, null, 1, 40));
            add(new Token(TokenType.TMULT, null, 1, 43)); add(new Token(TokenType.TMULT, null, 1, 44)); add(new Token(TokenType.TMLEQ, null, 1, 45));
            add(new Token(TokenType.TDIVD, null, 1, 48)); add(new Token(TokenType.TDVEQ, null, 1, 50));
            add(new Token(TokenType.TUNDF, "!", 1, 53)); add(new Token(TokenType.TUNDF, "!", 1, 54)); add(new Token(TokenType.TNEQL, null, 1, 55));
            add(new Token(TokenType.TGRTR, null, 1, 58)); add(new Token(TokenType.TGRTR, null, 1, 59)); add(new Token(TokenType.TGRTR, null, 1, 60));

            add(new Token(TokenType.TLESS, null, 1, 62));
            add(new Token(TokenType.TLEQL, null, 1, 63));

            add(new Token(TokenType.TLEQL, null, 1, 66));
            add(new Token(TokenType.TGRTR, null, 1, 68));

            add(new Token(TokenType.TLESS, null, 1, 70)); add(new Token(TokenType.TLESS, null, 1, 71)); add(new Token(TokenType.TLESS, null, 1, 72));

            add(new Token(TokenType.TGRTR, null, 1, 74));
            add(new Token(TokenType.TGREQ, null, 1, 75));

            add(new Token(TokenType.TGREQ, null, 1, 78));
            add(new Token(TokenType.TLESS, null, 1, 80));

            add(new Token(TokenType.TPLUS, null, 1, 82)); add(new Token(TokenType.TPLUS, null, 1, 83)); add(new Token(TokenType.TPLUS, null, 1, 84));
            add(new Token(TokenType.TSUBT, null, 1, 86)); add(new Token(TokenType.TSUBT, null, 1, 87)); add(new Token(TokenType.TSUBT, null, 1, 88));
            add(new Token(TokenType.TMULT, null, 1, 90)); add(new Token(TokenType.TMULT, null, 1, 91)); add(new Token(TokenType.TMULT, null, 1, 92));

            add(new Token(TokenType.TEOF, null, 1, 112));

        }};

        Scanner sc = new Scanner(inputs);
        int i = 0;
        while(!sc.isFinished()) {
            Token tok = sc.nextValidToken();

            if (tok != null) {
                Assert.assertEquals(tokens.get(i).getType(), tok.getType());
                Assert.assertEquals(tokens.get(i).getLexeme(), tok.getLexeme());
                Assert.assertEquals(tokens.get(i).getLine(), tok.getLine());
                Assert.assertEquals(tokens.get(i).getColumn(), tok.getColumn());
                i++;
            }
        }
    }

    @Test
    public void testNextToken_numerics() throws Exception {

        String inputs = "123 123.456 0 00 001 00123.123 0.0000 0.00001 " +
                "123abc 123.abc 123.456abc";

        List<Token> tokens = new LinkedList<Token>() {{
            add(new Token(TokenType.TILIT, "123", 1, 1));
            add(new Token(TokenType.TFLIT, "123.456", 1, 5));
            add(new Token(TokenType.TILIT, "0", 1, 13));

            add(new Token(TokenType.TILIT, "0", 1, 15));
            add(new Token(TokenType.TILIT, "0", 1, 16));

            add(new Token(TokenType.TILIT, "0", 1, 18));
            add(new Token(TokenType.TILIT, "0", 1, 19));
            add(new Token(TokenType.TILIT, "1", 1, 20));

            add(new Token(TokenType.TILIT, "0", 1, 22));
            add(new Token(TokenType.TILIT, "0", 1, 23));
            add(new Token(TokenType.TFLIT, "123.123", 1, 24));
            add(new Token(TokenType.TFLIT, "0.0000", 1, 32));
            add(new Token(TokenType.TFLIT, "0.00001", 1, 39));

            add(new Token(TokenType.TUNDF, "123abc", 1, 47));
            add(new Token(TokenType.TUNDF, "123.", 1, 54));
            add(new Token(TokenType.TIDNT, "abc", 1, 58));
            add(new Token(TokenType.TUNDF, "123.456abc", 1, 62));
            add(new Token(TokenType.TEOF, null, 1, 72));
        }};

        Scanner sc = new Scanner(inputs);
        int i = 0;
        while(!sc.isFinished()) {
            Token tok = sc.nextValidToken();

            if (tok != null) {
                Assert.assertEquals(tokens.get(i).getType(), tok.getType());
                Assert.assertEquals(tokens.get(i).getLexeme(), tok.getLexeme());
                Assert.assertEquals(tokens.get(i).getLine(), tok.getLine());
                Assert.assertEquals(tokens.get(i).getColumn(), tok.getColumn());
                i++;
            }
        }
    }

    @Test
    public void testNextToken_strings() throws Exception {

        String inputs = "\"valid String\"  \" invalidString \n" +
                " \"string\"5\"another string\"";

        List<Token> tokens = new LinkedList<Token>() {{
            add(new Token(TokenType.TSTRG, "valid String", 1, 1));
            add(new Token(TokenType.TUNDF, "\" invalidString ", 1, 17));
            add(new Token(TokenType.TSTRG, "string", 2, 2));
            add(new Token(TokenType.TILIT, "5", 2, 10));
            add(new Token(TokenType.TSTRG, "another string", 2, 11));
            add(new Token(TokenType.TEOF, null, 3, 27));
        }};

        Scanner sc = new Scanner(inputs);
        int i = 0;
        while(!sc.isFinished()) {
            Token tok = sc.nextValidToken();

            if (tok != null) {
                Assert.assertEquals(tokens.get(i).getType(), tok.getType());
                Assert.assertEquals(tokens.get(i).getLexeme(), tok.getLexeme());
                Assert.assertEquals(tokens.get(i).getLine(), tok.getLine());
                Assert.assertEquals(tokens.get(i).getColumn(), tok.getColumn());
                i++;
            }
        }
    }

    @Test
    public void testSingleCharacterString() throws Exception {

        String inputs = "print temparray[counter], \",\";";

        List<Token> tokens = new LinkedList<Token>() {{
            add(new Token(TokenType.TPRIN, null, 1, 1));
            add(new Token(TokenType.TIDNT, "temparray", 1, 7));
            add(new Token(TokenType.TLBRK, null, 1, 16));
            add(new Token(TokenType.TIDNT, "counter", 1, 17));
            add(new Token(TokenType.TRBRK, null, 1, 24));
            add(new Token(TokenType.TCOMA, null, 1, 25));
            add(new Token(TokenType.TSTRG, ",", 1, 27));
            add(new Token(TokenType.TSEMI, null, 1, 30));
        }};

        Scanner sc = new Scanner(inputs);
        int i = 0;
        while(!sc.isFinished()) {
            Token tok = sc.nextValidToken();

            if (tok != null) {
                Assert.assertEquals(tokens.get(i).getType(), tok.getType());
                Assert.assertEquals(tokens.get(i).getLexeme(), tok.getLexeme());
                Assert.assertEquals(tokens.get(i).getLine(), tok.getLine());
                Assert.assertEquals(tokens.get(i).getColumn(), tok.getColumn());
                i++;
            }
        }
    }

} 
