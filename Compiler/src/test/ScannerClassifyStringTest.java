package test;

import CD15.scanner.StringClassification;
import CD15.scanner.StateManager;
import junit.framework.Assert;
import org.junit.Test;

/**
 * Scanner Tester.
 *
 * @author Tyler Haigh
 * @since <pre>Aug 2, 2015</pre>
 * @version 1.0
 */
public class ScannerClassifyStringTest {

    /**
     * Method: nextToken(String line)
     */
    @Test
    public void testClassifyString_allAscii() throws Exception {
        for (int i = 0; i <= 32; i++) {
            char c = (char)i;
            StringClassification sClass = StateManager.classifyString(c);

            if (i == 10 || i == 13) Assert.assertEquals(StringClassification.NewLine, sClass);
            else if (i == 9 || i == 32) Assert.assertEquals(StringClassification.Space, sClass);
            else Assert.assertEquals(StringClassification.NonPrintable, sClass);
        }

        for (int i = 33; i <= 47; i++) {
            char c = (char)i;
            StringClassification sClass = StateManager.classifyString(c);
            Assert.assertEquals(StringClassification.Punctuation, sClass);
        }

        for (int i = 48; i <= 57; i++) {
            char c = (char)i;
            StringClassification sClass = StateManager.classifyString(c);
            Assert.assertEquals(StringClassification.Numeric, sClass);
        }

        for (int i = 58; i <= 64; i++) {
            char c = (char)i;
            StringClassification sClass = StateManager.classifyString(c);
            Assert.assertEquals(StringClassification.Punctuation, sClass);
        }

        for (int i = 65; i <= 90; i++) {
            char c = (char)i;
            StringClassification sClass = StateManager.classifyString(c);
            Assert.assertEquals(StringClassification.String, sClass);
        }

        for (int i = 91; i <= 96; i++) {
            char c = (char)i;
            StringClassification sClass = StateManager.classifyString(c);
            Assert.assertEquals(StringClassification.Punctuation, sClass);
        }

        for (int i = 97; i <= 122; i++) {
            char c = (char)i;
            StringClassification sClass = StateManager.classifyString(c);
            Assert.assertEquals(StringClassification.String, sClass);
        }

        for (int i = 123; i <= 127; i++) {
            char c = (char)i;
            StringClassification sClass = StateManager.classifyString(c);

            if (c == 127) Assert.assertEquals(StringClassification.NonPrintable, sClass);
            else Assert.assertEquals(StringClassification.Punctuation, sClass);
        }
    }

}
