package CD15.parser.nodes;

import CD15.entity.Token;
import CD15.entity.TokenType;
import CD15.parser.NodeDataType;
import CD15.parser.ParseTreeNodeType;
import CD15.parser.Parser;
import CD15.parser.TreeNode;

/**
 * Generates a Rel Node of the form:
 * < rel>           ::= not < rel> | < expr> < relOp> < expr>
 * < relOp>         ::= != | == | < | > | <= | >=
 *
 * @author Tyler Haigh - C3182929
 * @since 30/08/2015
 */
public class NRelNode {

    /**
     * Attempts to make a Rel Node given a valid token stream
     * @param p The Parser
     * @return A valid Tree Node if given a valid token stream or NUNDF
     */
    public static TreeNode make(Parser p) {

        Token nextTok = p.peek();
        if (nextTok.getType() == TokenType.TNOTK) {
            p.consume();

            TreeNode rel = make(p);
            Token afterRel = p.peek();

            TreeNode not = new TreeNode(ParseTreeNodeType.NNOT);
            not.setLeft(rel);

            if (rel.getDataType() != NodeDataType.Boolean)
                p.semanticError("Expected a boolean relational expression, but got " + rel.getDataType().name(), afterRel.getLine(), afterRel.getColumn());
            else
                not.setDataType(NodeDataType.Boolean);

            return not;
        } else {
            TreeNode exprL = NExprNode.make(p);
            Token afterExpr = p.peek();

            TreeNode relTail = relTail(p);
            Token afterRel = p.peek();

            if (relTail == null) return exprL;
            else {
                relTail.setLeft(exprL);

                // From Mike Hannaford on Discussion Board:
                // So and/or/xor take two boolean operands and produce a boolean,
                // relational ops take two reals and produce a boolean,
                // while the others take two reals and produce a real.
                //
                // I decided the other way, ie ALL relational operators
                // take only numeric operands and this is what you should follow

                NodeDataType leftDataType = exprL.getDataType();
                NodeDataType rightDataType = (relTail.getRight() != null) ? relTail.getRight().getDataType() : NodeDataType.Undefined;

                // Must be a Float Type
                if (leftDataType != NodeDataType.Float)
                    p.semanticError("Invalid left hand datatype of " + leftDataType.name() +
                                    " for relational comparison check. Expected a Float numeric.",
                            afterExpr.getLine(), afterExpr.getColumn());
                else if (rightDataType != NodeDataType.Float)
                    p.semanticError("Invalid right hand datatype of " + rightDataType.name() +
                                " for relational comparison check. Expected a Float numeric.",
                        afterExpr.getLine(), afterExpr.getColumn());
                else
                    relTail.setDataType(NodeDataType.Boolean);

                return relTail;
            }

        }
    }

    /**
     * Attempts to generate the rel tail node given a valid token stream
     * @param p The Parser
     * @return The Relational Operator node joining the expression or NUNDF
     *         or null if no relational operator exists
     */
    private static TreeNode relTail(Parser p) {

        Token nextTok = p.peek();
        switch (nextTok.getType()) {
            case TDEQL: case TNEQL: case TGRTR: case TGREQ:
            case TLESS: case TLEQL: {
                TreeNode relOp = relOp(p);
                TreeNode exprR = NExprNode.make(p);

                relOp.setRight(exprR);
                return relOp;
            }
            default: return null;
        }
    }

    /**
     * Generates a Relational Operator node given a token
     * @param p The Parser
     * @return The Relational Operator Tree Node matching the given
     *         token or NUNDF for invalid tokens
     */
    private static TreeNode relOp(Parser p) {

        Token nextTok = p.peek();
        TreeNode relOp = new TreeNode(ParseTreeNodeType.NUNDEF);

        if (nextTok.getType() == TokenType.TDEQL) {
            p.consume();
            relOp.setNodeType(ParseTreeNodeType.NEQL);
        }

        if (nextTok.getType() == TokenType.TNEQL) {
            p.consume();
            relOp.setNodeType(ParseTreeNodeType.NNEQ);
        }

        if (nextTok.getType() == TokenType.TGRTR) {
            p.consume();
            relOp.setNodeType(ParseTreeNodeType.NGTR);
        }

        if (nextTok.getType() == TokenType.TGREQ) {
            p.consume();
            relOp.setNodeType(ParseTreeNodeType.NGEQ);
        }

        if (nextTok.getType() == TokenType.TLESS) {
            p.consume();
            relOp.setNodeType(ParseTreeNodeType.NLESS);
        }

        if (nextTok.getType() == TokenType.TLEQL) {
            p.consume();
            relOp.setNodeType(ParseTreeNodeType.NLEQ);
        }

        // Check if we did consume the token
        if (relOp.getNodeType() == ParseTreeNodeType.NUNDEF)
            p.syntaxError("Expected relational operator", nextTok.getLine(), nextTok.getColumn());

        return relOp;
    }
}
