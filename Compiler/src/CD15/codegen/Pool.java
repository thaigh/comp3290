package CD15.codegen;

import java.util.ArrayList;

/**
 * Represents a temporary variable resource in AC15 memory that can be
 * locked to prevent overwriting it when evaluating expressions
 *
 * @author Tyler Haigh - C3182929
 * @since 13/10/2015
 */
class TempVar {
    private int wordIndex;
    private boolean available;

    public TempVar() { wordIndex = 0; available = false; }

    public int getWordIndex() { return wordIndex; }
    public boolean getAvailable() { return available; }

    public void setWordIndex(int wordIndex) { this.wordIndex = wordIndex; }
    public void setAvailable(boolean available) { this.available = available; }
}

/**
 * Represents a pool of temporary variable resources in AC15 memory that can be
 * locked to prevent overwriting it when evaluating expressions
 *
 * @author Tyler Haigh - C3182929
 * @since 13/10/2015
 */
public class Pool {

    private ArrayList<TempVar> store;

    public Pool() {
        store = new ArrayList<TempVar>();
    }

    /**
     * Finds the next available resource in the pool. If no resource is
     * available, a new one will be created. It needs to be allocated
     * and given a memory address in AC15 before use
     * @return A TempVar object for the next available resource
     */
    public TempVar getResource() {
        // Find min available resource
        for (int i = 0; i < store.size(); i++) {
            if (store.get(i).getAvailable()) {
                TempVar var = store.get(i);
                var.setAvailable(false);
                return var;
            }
        }

        // No resource available. Make a new one and return it
        TempVar newTemp = new TempVar();
        newTemp.setAvailable(false);
        newTemp.setWordIndex(1);
        store.add(newTemp);
        return newTemp;
    }

    /**
     * Releases a resource from the pool so it can be reused later
     * @param location The AC15 memory address of the resource
     */
    public void releaseResource(int location) {
        for (int i = 0; i < store.size(); i++) {
            if (store.get(i).getWordIndex() == location)
                store.get(i).setAvailable(true);
        }
    }

    /**
     * Returns the total number of resources in the pool
     * @return The total number of resources in the pool
     */
    public int size() {
        return store.size();
    }
}
