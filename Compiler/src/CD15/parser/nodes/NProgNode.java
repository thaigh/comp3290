package CD15.parser.nodes;

import CD15.entity.Token;
import CD15.entity.TokenType;
import CD15.parser.NodeDataType;
import CD15.parser.ParseTreeNodeType;
import CD15.parser.Parser;
import CD15.parser.TreeNode;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Generates a Program node of the form:
 * <program> ::= program <id> <arrays> <procs> <main> end program <id>
 *
 * @author Tyler Haigh - C3182929
 * @since 27/08/2015
 */
public class NProgNode {

    /**
     * Attempts to make the program node given a valid token stream
     * @param p The Parser
     * @return A valid Tree Node for a valid token stream or NUNDF
     */
    public static TreeNode make(Parser p) {

        // Push scope
        p.pushScope("prog");

        TreeNode nProg = new TreeNode(ParseTreeNodeType.NUNDEF);

        if (!p.peekAndConsume(TokenType.TPROG)) {
            p.syntaxError("Expected the keyword 'program'", p.peekLine(), p.peekColumn());
            p.popScope();
            return nProg;
        }

        if (!p.peekAndConsume(TokenType.TIDNT)) {
            p.syntaxError("Expected a program name identifier", p.peekLine(), p.peekColumn());
            p.popScope();
            return nProg;
        }
        Token firstId = p.lastToken();
        p.insertIdentifier(firstId);

        TreeNode arrays = NArrlNode.make(p);
        if (arrays != null && arrays.getNodeType() == ParseTreeNodeType.NUNDEF) {
            try { errorRecovery(p);
            } catch (Exception e) { p.popScope(); return nProg; }
        }

        TreeNode procs = NProclNode.make(p);
        if (procs != null && procs.getNodeType() == ParseTreeNodeType.NUNDEF) {
            try {errorRecovery(p);}
            catch (Exception e) { p.popScope(); return nProg; }
        }

        TreeNode main = NMainNode.make(p);
        if (main != null && main.getNodeType() == ParseTreeNodeType.NUNDEF)
            errorRecoveryToEnd(p);

        /**
         * If we get to this point, we may have a completely valid program.
         * Any errors found now are easily corrected. We log an additional message
         * indicating that we are in fact correcting the programmers input code.
         */

        if (!p.peekAndConsume(TokenType.TENDK)) {
            p.syntaxError("Expected keyword 'end'. Correcting input...", p.peekLine(), p.peekColumn());
        }

        if (!p.peekAndConsume(TokenType.TPROG)) {
            p.syntaxError("Expected keyword 'program'. Correcting input...", p.peekLine(), p.peekColumn());
        }

        if (!p.peekAndConsume(TokenType.TIDNT)) {
            p.syntaxError("Expected a program name identifier. Correcting input...", p.peekLine(), p.peekColumn());
        }

        Token secondId = (p.lastToken() != null && p.lastToken().getType() == TokenType.TIDNT) ? p.lastToken() : firstId;
        p.insertIdentifier(secondId);

        // Make the node
        nProg = new TreeNode(ParseTreeNodeType.NPROG, arrays, procs, main);
        nProg.setIdentifierRecord(firstId.getSymbolTableRecord());

        // Perform Semantic Analysis
        if (firstId.getSymbolTableRecord().equals(secondId.getSymbolTableRecord()))
            nProg.setDataType(NodeDataType.Program);
        else
            p.semanticError("The program identifiers do not match", secondId.getLine(), secondId.getColumn());

        // Pop scope back to nothing
        p.popScope();

        return nProg;
    }

    /**
     * Attempts error recovery in the event of an invalid token stream
     * @param p The Parser
     * @throws Exception Unable to recover
     */
    private static void errorRecovery(Parser p) throws Exception {
        // Scan ahead to find PROC / LOCAL / END
        int[] scanAmounts = new int[3];
        scanAmounts[0] = p.next(TokenType.TPROC);
        scanAmounts[1] = p.next(TokenType.TLOCL);
        scanAmounts[2] = p.next(TokenType.TENDK);

        // Get min
        Arrays.sort(scanAmounts);
        int minAmount = -1;
        for (int i = 0; i < scanAmounts.length; i++) {
            if (scanAmounts[i] != -1) {
                minAmount = scanAmounts[i];
                break;
            }
        }

        if (minAmount == -1) throw new Exception("Unable to scan ahead");
        else p.consume(minAmount);
    }

    private static void errorRecoveryToEnd(Parser p) {
        int eof = p.next(TokenType.TEOF); // Must exist
        int recoverTo = eof;
        if (p.peek(eof-1).getType() == TokenType.TIDNT) recoverTo = eof - 1;
        if (p.peek(eof-2).getType() == TokenType.TPROG) recoverTo = eof - 2;
        if (p.peek(eof-3).getType() == TokenType.TENDK) recoverTo = eof - 3;

        p.consume(recoverTo);
    }

}
