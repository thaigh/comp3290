package CD15.parser.nodes;

import CD15.entity.Token;
import CD15.entity.TokenType;
import CD15.parser.NodeDataType;
import CD15.parser.ParseTreeNodeType;
import CD15.parser.Parser;
import CD15.parser.TreeNode;

import java.util.Arrays;

/**
 * Generates a Statement List node of the form:
 * < stats>    ::= < stat> < statTail>
 * < statTail> ::= ɛ | < stats>
 *
 * @author Tyler Haigh - C3182929
 * @since 28/08/2015
 */
public class SStatsNode {

    /**
     * Attempts to make a statement list node given a valid token stream
     * @param p The Parser
     * @return A valid Statement List node for a valid token stream or NUNDF
     */
    public static TreeNode make(Parser p) {

        TreeNode statNode = NStatNode.make(p);

        // Error Recovery
        if (statNode.getNodeType() == ParseTreeNodeType.NUNDEF) {
            try { errorRecovery(p); }
            catch (Exception e) { return new TreeNode(ParseTreeNodeType.NUNDEF); }
        }

        TreeNode statTail = statsTail(p);

        if (statTail == null) return statNode;
        else {
            TreeNode statList = new TreeNode(ParseTreeNodeType.NSLIST, statNode, statTail);
            statList.setDataType(NodeDataType.Node);
            return statList;
        }
    }

    /**
     * Attempts to generate the tail for a statement list
     * @param p The Parser
     * @return Null if no statement follows, a Statement Node, or NUNDF
     */
    private static TreeNode statsTail(Parser p) {
        Token nextTok = p.peek();
        TokenType type = nextTok.getType();

        if (type == TokenType.TLOOP ||
                type == TokenType.TEXIT ||
                type == TokenType.TIFKW ||
                type == TokenType.TINPT ||
                type == TokenType.TPRIN ||
                type == TokenType.TPRLN ||
                type == TokenType.TCALL ||
                type == TokenType.TIDNT) {
            TreeNode stats = make(p);
            return  stats;
        }
        else return null;
    }

    /**
     * Attempts error recovery in the event of an invalid token stream
     * @param p The Parser
     * @throws Exception Unable to recover
     */
    private static void errorRecovery(Parser p) throws Exception {

        int[] scanAmounts = new int[11];
        // Scan ahead to find the next statement or END
        scanAmounts[0] = p.next(TokenType.TLOOP);
        scanAmounts[1] = p.next(TokenType.TEXIT);
        scanAmounts[2] = p.next(TokenType.TIFKW);
        scanAmounts[3] = p.next(TokenType.TINPT);
        scanAmounts[4] = p.next(TokenType.TPRIN);
        scanAmounts[5] = p.next(TokenType.TPRLN);
        scanAmounts[6] = p.next(TokenType.TCALL);
        scanAmounts[7] = p.next(TokenType.TIDNT);
        scanAmounts[8] = p.next(TokenType.TELSE);
        scanAmounts[9] = p.next(TokenType.TELSF);
        scanAmounts[10] = p.next(TokenType.TENDK); // END PROC or END PROGRAM

        // Find the min
        Arrays.sort(scanAmounts);
        int minScanAmount = -1;
        for (int i = 0; i < scanAmounts.length; i++) {
            if (scanAmounts[i] != -1) {
                minScanAmount = scanAmounts[i];
                break;
            }
        }

        if (minScanAmount == -1) throw new Exception("Unable to scan ahead");
        else p.consume(minScanAmount);
    }

}
