package CD15.parser.nodes;

import CD15.entity.Token;
import CD15.entity.TokenType;
import CD15.parser.*;

import java.util.List;
import java.util.Stack;

/**
 * Generates a single statement node depending on the statement to parse
 *
 * @author Tyler Haigh - C3182929
 * @since 28/08/2015
 */
public class NStatNode {

    /**
     * Attempts to make a statement node given a valid token stream
     * @param p The parser
     * @return A valid tree node for a valid token stream or NUNDF
     */
    public static TreeNode make(Parser p) {

        Token nextTok = p.peek();
        TokenType type = nextTok.getType();

        if (type == TokenType.TLOOP) return loopStat(p);
        if (type == TokenType.TEXIT) return exitStat(p);
        if (type == TokenType.TIFKW) return NIfNode.make(p);
        if (type == TokenType.TINPT) return inputStat(p);
        if (type == TokenType.TPRIN) return printStat(p);
        if (type == TokenType.TPRLN) return printlineStat(p);
        if (type == TokenType.TCALL) return NCallNode.make(p);

        if (type == TokenType.TIDNT) return NAsgnNode.make(p);

        p.syntaxError("Expected a statement", p.peekLine(), p.peekColumn());
        return new TreeNode(ParseTreeNodeType.NUNDEF);

    }

    /**
     * Attempts to generate a Loop Statement of the form:
     * < loopStat> ::= loop < id> < stats> end loop < id>
     * @param p The Parser
     * @return A valid Loop Tree Node for a valid token stream or NUNDF
     */
    private static TreeNode loopStat(Parser p) {

        TreeNode loop = new TreeNode(ParseTreeNodeType.NUNDEF);

        // We have previously seen a TLOOP, so consume it
        if (!p.peekAndConsume(TokenType.TLOOP)) {
            // Should never happen, unless TLOOP is consumed by accident
            p.syntaxError("Expected keyword 'loop' for loop statement", p.peekLine(), p.peekColumn());
            return loop;
        }

        if (!p.peekAndConsume(TokenType.TIDNT)) {
            p.syntaxError("Expected identifier name for loop statement", p.peekLine(), p.peekColumn());
            return loop;
        }
        Token firstLoopId = p.lastToken();

        // Prevent double declaration
        if (p.lookupIdentifier(firstLoopId.getLexeme()) != null) {
            p.semanticError("Loop Id '" + firstLoopId.getLexeme() + "' is already defined",
                    firstLoopId.getLine(), firstLoopId.getColumn());
            p.createInvalidRecord(firstLoopId);
        } else {
            p.insertIdentifier(firstLoopId);
        }

        // Push to the stack so we can validate exit statements
        p.pushLoop(firstLoopId.getLexeme());

        // Recovers in the SStatsNode class
        TreeNode stats = SStatsNode.make(p);

        // Pop the scope back off
        p.popLoop();

        if (!p.peekAndConsume(TokenType.TENDK)) {
            p.syntaxError("Expected keyword 'end' to close 'loop' block", p.peekLine(), p.peekColumn());
            return loop;
        }

        if (!p.peekAndConsume(TokenType.TLOOP)) {
            p.syntaxError("Expected keyword 'loop' to close 'loop' block", p.peekLine(), p.peekColumn());
            return loop;
        }

        if (!p.peekAndConsume(TokenType.TIDNT)) {
            p.syntaxError("Expected identifier name to close 'loop' block", p.peekLine(), p.peekColumn());
            return loop;
        }
        Token secondLoopId = p.lastToken();

        SymbolTableRecord rec = p.lookupIdentifier(secondLoopId.getLexeme());
        if (rec != null) secondLoopId.setSymbolTableRecord(rec);
        else p.createInvalidRecord(secondLoopId);

        loop.setNodeType(ParseTreeNodeType.NLOOP);
        loop.setLeft(stats);
        loop.setIdentifierRecord(firstLoopId.getSymbolTableRecord());

        // Semantic Analysis on Loop IDs
        if (firstLoopId.getSymbolTableRecord().equals(secondLoopId.getSymbolTableRecord()))
            loop.setDataType(NodeDataType.Node);
        else
            p.semanticError("Loop Ids do not match", secondLoopId.getLine(), secondLoopId.getColumn());

        // Check for EXIT Node
        boolean containsExitStatement = false;
        List<TreeNode> childNodes = stats.toList();
        for(TreeNode node : childNodes) {
            if (node.getNodeType() == ParseTreeNodeType.NEXIT &&
                    node.getIdentifierRecord() == firstLoopId.getSymbolTableRecord()) {
                containsExitStatement = true;
                break;
            }
        }

        if (!containsExitStatement) {
            p.semanticError("No exit statement found for loop '" + firstLoopId.getLexeme() + "'",
                    firstLoopId.getLine(), firstLoopId.getColumn());
            loop.setDataType(NodeDataType.Undefined);
        }

        return loop;
    }

    /**
     * Attempts to generate an Exit Statement node given
     * a valid token stream of the form:
     * < exitStat> ::= exit < id> when < bool> ;
     * @param p The Parser
     * @return A valid Exit Tree node for a valid token stream or NUNDF
     */
    private static TreeNode exitStat(Parser p) {

        TreeNode exit = new TreeNode(ParseTreeNodeType.NUNDEF);

        // We have previously seen a TEXIT, so consume it
        if (!p.peekAndConsume(TokenType.TEXIT)) {
            // Should never happen, unless TEXIT is consumed by accident
            p.syntaxError("Expected keyword 'exit' for exit statement", p.peekLine(), p.peekColumn());
            return exit;
        }

        if (!p.peekAndConsume(TokenType.TIDNT)) {
            p.syntaxError("Expected identifier name for exit statement", p.peekLine(), p.peekColumn());
            return exit;
        }
        Token exitId = p.lastToken();
        p.insertIdentifier(exitId);

        if (! p.peekAndConsume(TokenType.TWHEN)) {
            p.syntaxError("Expected keyword 'when' in exit statement", p.peekLine(), p.peekColumn());
            return exit;
        }

        TreeNode bool = NBoolNode.make(p);
        // TODO: Maybe Error recovery??
        Token afterBool = p.peek();

        if (!p.peekAndConsume(TokenType.TSEMI)) {
            p.syntaxError("Expected semicolon ; in exit statement", p.peekLine(), p.peekColumn());
            return exit;
        }

        // All good
        exit.setNodeType(ParseTreeNodeType.NEXIT);
        exit.setLeft(bool);

        // Semantic analysis
        // Check for loop id
        boolean validLoopId = false;

        // Pop stacks off to try and find loop id. Used for nested loops
        Stack<String> loopStack = new Stack<String>();
        while (!p.emptyLoopStack()) {
            String loopId = p.popLoop();
            if (loopId.equals(exitId.getLexeme())) {
                p.pushLoop(loopId);
                validLoopId = true;
                break;
            } else
                loopStack.push(loopId);
        }

        // Push scopes back on
        while (!loopStack.empty()) {
            p.pushLoop(loopStack.pop());
        }

        if (!validLoopId) {
            p.semanticError("Unknown loop identifier '" + exitId.getLexeme() + "' for exit statement", exitId.getLine(), exitId.getColumn());
            exit.setIdentifierRecord(p.createInvalidRecord(exitId));
        }
        else
            exit.setIdentifierRecord(exitId.getSymbolTableRecord());

        // Check for bool data type
        boolean validBool = false;
        if (bool.getDataType() != NodeDataType.Boolean)
            p.semanticError("Expected a boolean expression for exit statement, but got " + bool.getDataType().name(),
                    afterBool.getLine(), afterBool.getColumn());
        else
            validBool = true;

        // Determine final node data type
        if (validLoopId && validBool)
            exit.setDataType(NodeDataType.Node);

        return exit;
    }

    /**
     * Attempts to generate an Input Statement of the form
     * input <vList> ;
     * @param p The Parser
     * @return A valid Input Tree Node for a valid token stream or NUNDF
     */
    private static TreeNode inputStat(Parser p) {

        TreeNode input = new TreeNode(ParseTreeNodeType.NUNDEF);

        // We have previously seen a TINPT, so consume it
        if (!p.peekAndConsume(TokenType.TINPT)) {
            // Should never happen, unless TINPT is consumed by accident
            p.syntaxError("Expected keyword 'input' for input statement", p.peekLine(), p.peekColumn());
            return input;
        }

        TreeNode varList = NVListNode.make(p);

        if (!p.peekAndConsume(TokenType.TSEMI)) {
            p.syntaxError("Expected semicolon ; in input statement", p.peekLine(), p.peekColumn());
            return input;
        }

        input.setNodeType(ParseTreeNodeType.NINPUT);
        input.setLeft(varList);
        input.setDataType(NodeDataType.Node);
        return input;
    }

    /**
     * Attempts to generate a print statement for a valid token stream
     * of the form:
     * print <prList> ;
     * @param p The Parser
     * @return A valid Print node for a valid token stream or NUNDF
     */
    private static TreeNode printStat(Parser p) {

        TreeNode print = new TreeNode(ParseTreeNodeType.NUNDEF);

        // We have previously seen a TPRIN, so consume it
        if (!p.peekAndConsume(TokenType.TPRIN)) {
            // Should never happen, unless TPRIN is consumed by accident
            p.syntaxError("Expected keyword 'print' for print statement", p.peekLine(), p.peekColumn());
            return print;
        }

        TreeNode printList = NPrListNode.make(p);

        if (!p.peekAndConsume(TokenType.TSEMI)) {
            p.syntaxError("Expected semicolon ; in print statement", p.peekLine(), p.peekColumn());
            return print;
        }

        print.setNodeType(ParseTreeNodeType.NPRINT);
        print.setLeft(printList);
        print.setDataType(NodeDataType.Node);
        return print;
    }

    /**
     * Attempts to generate a printline statement given a valid token stream
     * of the form:
     * printline < nullablePrList> ;
     * < nullablePrList>::= ɛ | < prList>
     * @param p The Parser
     * @return A valid printline tree node for a valid token stream or NUNDF
     */
    private static TreeNode printlineStat(Parser p) {

        TreeNode printLine = new TreeNode(ParseTreeNodeType.NUNDEF);

        // We have previously seen a TPRLN, so consume it
        if (!p.peekAndConsume(TokenType.TPRLN)) {
            // Should never happen, unless TPRLN is consumed by accident
            p.syntaxError("Expected keyword 'printline' for printline statement", p.peekLine(), p.peekColumn());
            return printLine;
        }

        TreeNode printList = null;
        Token nextTok = p.peek();
        if (nextTok.getType() != TokenType.TSEMI) {
            printList = NPrListNode.make(p);
        }

        if (!p.peekAndConsume(TokenType.TSEMI)) {
            p.syntaxError("Expected semicolon ; in printline statement", p.peekLine(), p.peekColumn());
            return printLine;
        }

        printLine.setNodeType(ParseTreeNodeType.NPRLN);
        if (printList != null) printLine.setLeft(printList);
        printLine.setDataType(NodeDataType.Node);
        return printLine;

    }


}
