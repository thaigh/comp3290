package CD15.parser.nodes;

import CD15.entity.Token;
import CD15.entity.TokenType;
import CD15.parser.NodeDataType;
import CD15.parser.ParseTreeNodeType;
import CD15.parser.Parser;
import CD15.parser.TreeNode;

/**
 * Generates an EList node of the form
 * < eList>     ::= < expr> < eListTail>
 * < eListTail> ::= ɛ | , < eList>
 *
 * @author Tyler Haigh - C3182929
 * @since 30/08/2015
 */
public class NEListNode {

    /**
     * Attempts to make an EList node given a valid Token Stream
     * @param p The Parser
     * @return A valid Tree Node given a valid token stream or NUNDF
     */
    public static TreeNode make(Parser p) {

        TreeNode expr = NExprNode.make(p);
        if (expr != null && expr.getNodeType() == ParseTreeNodeType.NUNDEF) {
            try { errorRecovery(p); }
            catch (Exception e) { return new TreeNode(ParseTreeNodeType.NUNDEF); }
        }

        TreeNode tail = eListTail(p);

        if (tail == null) return expr;
        else {
            TreeNode eList = new TreeNode(ParseTreeNodeType.NELIST, expr, tail);
            eList.setDataType(NodeDataType.Node);
            return eList;
        }
    }

    /**
     * Attempts to generate the eList Tail node
     * @param p The Parser
     * @return Null if no EList node follows, or an EList node
     */
    private static TreeNode eListTail(Parser p) {
        Token nextTok = p.peek();

        if (nextTok.getType() != TokenType.TCOMA) return null;
        else {
            p.consume();

            TreeNode eList = make(p);
            return eList;
        }
    }

    /**
     * Attempts error recovery in the event of an invalid expression
     * @param p The Parser
     * @throws Exception Unable to recover
     */
    private static void errorRecovery(Parser p) throws Exception {
        // Find next comma or semi
        int nextComma = p.next(TokenType.TCOMA);
        int nextSemi = p.next(TokenType.TSEMI);

        if (nextComma > -1 && nextComma < 5) p.consume(nextComma);
        else if (nextSemi > -1) p.consume(nextSemi);
        else throw new Exception("Unable to recover");
    }
}
