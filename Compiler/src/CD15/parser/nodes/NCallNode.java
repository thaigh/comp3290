package CD15.parser.nodes;

import CD15.entity.Token;
import CD15.entity.TokenType;
import CD15.parser.*;

import java.util.Iterator;
import java.util.List;
import java.util.Stack;

/**
 * Created by Administrator on 1/10/2015.
 */
public class NCallNode {

    //private static TreeNode eList = null;

    /**
     * Attempts to generate a Call statement given a valid token stream of the form
     * < callStat> ::= call < id> < callWith> ;
     * < callWith> ::= ɛ | with < eList>
     *
     * You can call a proc and pass a variable of the same name to it, as they are typed
     * differently by the compiler. If a proc and variable of the same name are declared in
     * the same scope, however, this is considered a double declaration.
     *
     * @param p The Parser
     * @return A valid Call Tree Node for a valid token stream or NUNDF
     */
    public static TreeNode make(Parser p) {

        TreeNode call = new TreeNode(ParseTreeNodeType.NUNDEF);

        // We have previously seen a TCALL, so consume it
        if (!p.peekAndConsume(TokenType.TCALL)) {
            // Should never happen, unless TCALL is consumed by accident
            p.syntaxError("Expected keyword 'call' for call statement", p.peekLine(), p.peekColumn());
            return call;
        }

        if (!p.peekAndConsume(TokenType.TIDNT)) {
            p.syntaxError("Expected procedure identifier name for call statement", p.peekLine(), p.peekColumn());
            return call;
        }
        Token callId = p.lastToken();

        Token nextTok = p.peek();
        TreeNode eList = null;
        if (nextTok.getType() == TokenType.TWITH) {
            p.consume();
            eList = NEListNode.make(p);
        }

        if (!p.peekAndConsume(TokenType.TSEMI)) {
            p.syntaxError("Expected semicolon ; in call statement", p.peekLine(), p.peekColumn());
            return call;
        }

        call.setNodeType(ParseTreeNodeType.NCALL);
        if (eList != null) call.setLeft(eList);

        // Semantic analysis
        // Check for proc id
        SymbolTableRecord procRec = findProcedureId(p, callId);
        boolean validProcId = procRec != null;

        if (!validProcId) {
            p.semanticError("Unknown procedure id '" + callId.getLexeme() + "' in call statement",
                    callId.getLine(), callId.getColumn());

            call.setIdentifierRecord(p.createInvalidRecord(callId));
        } else {
            call.setIdentifierRecord(procRec);
        }

        boolean validVars = false;
        boolean validVals = false;

        //TODO: Error check this!

        if (validProcId) {
            // Check procedure parameters
            TreeNode procNode = procRec.getTreeNode();
            TreeNode params = procNode.getLeft();

            List<TreeNode> vars = (params != null && params.getLeft() != null) ? params.getLeft().toList() : null;
            List<TreeNode> vals = (params != null && params.getRight() != null) ? params.getRight().toList() : null;
            List<TreeNode> elistNodes = (eList != null) ? eList.toList() : null;
            Iterator<TreeNode> elistIter = (elistNodes != null) ? elistNodes.iterator() : null;


            boolean errorFlagged = false;
            if (vars == null && vals == null && elistIter != null) {
                p.semanticError("Invalid number of parameters for procedure " + callId.getLexeme(),
                        callId.getLine(), callId.getColumn());
                errorFlagged = true;
            }
            else
                validVars = (vars == null) || checkVars(p, callId, vars, elistIter);

            if (vals == null && (elistIter != null && elistIter.hasNext()) && !errorFlagged)
                p.semanticError("Invalid number of parameters for procedure " + callId.getLexeme(),
                        callId.getLine(), callId.getColumn());
            else
                validVals = (vals == null) || checkVals(p, callId, vals, elistIter);

        }

        if (validProcId && validVars && validVals)
            call.setDataType(NodeDataType.Node);

        return call;
    }

    private static SymbolTableRecord findProcedureId(Parser p, Token callId)
    {
        // Pop stacks off to get back to global prog scope
        Stack<String> scopeStack = new Stack<String>();
        while (!p.emptyScope()) {
            scopeStack.push(p.popScope());
        }

        // Look for proc id
        SymbolTableRecord procRec = p.lookupIdentifier(callId.getLexeme());

        // Push scopes back on
        while (!scopeStack.empty()) {
            p.pushScope(scopeStack.pop());
        }

        if ((procRec != null) &&
                (procRec.getTreeNode() != null) &&
                (procRec.getTreeNode().getNodeType() == ParseTreeNodeType.NPROC))
            return procRec;
        else return null;
    }

    private static boolean checkVars(Parser p, Token callId, List<TreeNode> vars, Iterator<TreeNode> elistIter) {

        boolean validVars = true;
        boolean checkedOffVars = vars == null;
        boolean notEnoughElists = false;

        if (vars != null && elistIter != null) {
            for (TreeNode var : vars) {
                if (!elistIter.hasNext()) {

                        SymbolTableRecord varRec = var.getIdentifierRecord();

                        p.semanticError("Did not match var parameter " +
                                ((varRec != null) ? varRec.getLexeme() + " ": "") +
                                "for procedure " + callId.getLexeme(), callId.getLine(), callId.getColumn());

                        notEnoughElists = true;
                    break;
                }

                // ElistIter has next
                TreeNode elistVar = elistIter.next();
                if (elistVar.getNodeType() != ParseTreeNodeType.NUNDEF) {
                    validVars = checkDataTypes(p, callId, var, elistVar) && validVars;

                    // Check for passing constants as var param
                    if (elistVar.getNodeType() == ParseTreeNodeType.NILIT ||
                            elistVar.getNodeType() == ParseTreeNodeType.NFLIT) {
                        p.semanticError("Unable to pass a constant by reference", callId.getLine(), callId.getColumn());
                        validVars = false;
                    }
                }
            }

            if (!notEnoughElists)
                checkedOffVars = true;
        }

        return validVars && checkedOffVars;
    }

    private static boolean checkVals(Parser p, Token callId, List<TreeNode> vals, Iterator<TreeNode> elistIter) {

        boolean validVals = true;
        boolean checkedOffVals = true;

        if (vals != null && elistIter != null) {
            for (TreeNode val : vals) {

                if (!elistIter.hasNext()) {
                    // Reached end of elist
                    break;
                }

                // ElistIter has next
                TreeNode elistVar = elistIter.next();
                if (elistVar.getNodeType() != ParseTreeNodeType.NUNDEF)
                    validVals = checkDataTypes(p, callId, val, elistVar) && validVals;
            }
        }

        // Check elist is at end
        if (elistIter != null && elistIter.hasNext()) {
            // We have too many val parameters
            p.semanticError("Invalid number of val parameters were provided for procedure " + callId.getLexeme(),
                    callId.getLine(), callId.getColumn());
            checkedOffVals = false;
        }

        return validVals && checkedOffVals;
    }

    private static boolean checkDataTypes(Parser p, Token callId, TreeNode left, TreeNode right) {
        if (left != null && right != null) {
            if (left.getDataType() != right.getDataType()) {
                p.semanticError("Invalid argument type. Procedure expected a " + left.getDataType().name() +
                                " for parameter " + left.getIdentifierRecord().getLexeme() + " but got a " +
                                right.getDataType() + " from variable " + right.getIdentifierRecord().getLexeme(),
                        callId.getLine(), callId.getColumn());
                return false;
            }
        } else if (left != null && right == null) { // Happens when right is the final right child
            p.semanticError("Argument not supplied for procedure parameter " + left.getIdentifierRecord().getLexeme(),
                    callId.getLine(), callId.getColumn());
            return false;
        }

        return true;
    }

}
