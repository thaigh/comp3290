package CD15.parser;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Data structure for the parser syntax tree. Defines the structure of a single
 * syntax tree node
 *
 * @author M. Hannaford
 * @since 1/8/2015
 */
public class TreeNode {

	private static int index = 0;

	private ParseTreeNodeType nodeType;
	private int idx;
	private TreeNode left,middle,right;
	private SymbolTableRecord identifierRecord, constRecord;
    private NodeDataType dataType;

    /**
     * Default constructor for the Tree Node. Initialises to an undefined node
     */
	public TreeNode () {
		nodeType = ParseTreeNodeType.NUNDEF;

		idx = ++index;

        left = null;
		middle = null;
		right = null;

        identifierRecord = null;
		constRecord = null;
        dataType = NodeDataType.Undefined;
	}

    /**
     * Constructor for a tree node with a given node type
     * @param nodeType The node type to set
     */
	public TreeNode (ParseTreeNodeType nodeType) {
		this();
		this.nodeType = nodeType;
	}

    /**
     * Constructor for a Tree Node given a node type and identifier record
     * @param nodeValue The type of the node
     * @param identifierRec The symbol table record of the identifier stored in the node
     */
	public TreeNode (ParseTreeNodeType nodeValue, SymbolTableRecord identifierRec) {
		this(nodeValue);
		identifierRecord = identifierRec;
	}

    /**
     * Constructor for a TreeNode given a node type and the left and right children of the node
     * @param nodeType The type of the node
     * @param left The left child of the node
     * @param right The right child of the node
     */
	public TreeNode (ParseTreeNodeType nodeType, TreeNode left, TreeNode right) {
		this(nodeType);
		this.left = left;
		this.right = right;
	}

    /**
     * Constructor for a Tree Node given the node type and the left, middle, and right children
     * @param nodeType The type of the node
     * @param left The left child of the node
     * @param middle The middle child of the node
     * @param right The right child of the node
     */
	public TreeNode (ParseTreeNodeType nodeType, TreeNode left, TreeNode middle, TreeNode right) {
		this(nodeType,left,right);
		this.middle = middle;
	}

	public ParseTreeNodeType getNodeType() { return nodeType; }
	public TreeNode getLeft() { return left; }
	public TreeNode getMiddle() { return middle; }
	public TreeNode getRight() { return right; }
	public SymbolTableRecord getIdentifierRecord() { return identifierRecord; }
	public SymbolTableRecord getConstRecord() { return constRecord; }
    public NodeDataType getDataType() { return dataType; }

	public void setNodeType(ParseTreeNodeType value) { nodeType = value; }
	public void setLeft(TreeNode l) { left = l; }
	public void setMiddle(TreeNode m) { middle = m; }
	public void setRight(TreeNode r) { right = r; }
	public void setIdentifierRecord(SymbolTableRecord ident) { identifierRecord = ident; }
	public void setConstRecord(SymbolTableRecord constRec) { constRecord = constRec; }
    public void setDataType(NodeDataType dataType) { this.dataType = dataType; }

    /**
     * Resets the node index to 0
     */
	public static void resetIndex() {
		index = 0;
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TreeNode treeNode = (TreeNode) o;

        //if (idx != treeNode.idx) return false;
        if (left != null ? !left.equals(treeNode.left) : treeNode.left != null) return false;
        if (middle != null ? !middle.equals(treeNode.middle) : treeNode.middle != null) return false;
        if (identifierRecord != null ? !identifierRecord.equals(treeNode.identifierRecord) : treeNode.identifierRecord != null) return false;
        if (constRecord != null ? !constRecord.equals(treeNode.constRecord) : treeNode.constRecord != null) return false;
        if (nodeType != treeNode.nodeType) return false;
        if (right != null ? !right.equals(treeNode.right) : treeNode.right != null) return false;
        if (dataType != treeNode.dataType) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = nodeType.hashCode();
        result = 31 * result + idx;
        result = 31 * result + (left != null ? left.hashCode() : 0);
        result = 31 * result + (middle != null ? middle.hashCode() : 0);
        result = 31 * result + (right != null ? right.hashCode() : 0);
        result = 31 * result + (identifierRecord != null ? identifierRecord.hashCode() : 0);
        result = 31 * result + (constRecord != null ? constRecord.hashCode() : 0);
        result = 31 * result + dataType.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return nodeType.name()
                + ((dataType != NodeDataType.Node) ? " [" + dataType.name() + "]" : "")
                + ((identifierRecord != null) ? " " + identifierRecord.getLexeme() : "")
                + ((constRecord != null) ? " " + constRecord.getLexeme() : "");
    }

    /**
     * Generates a string representation of the node and its children
     * @return A string representation of the node and its children
     */
    public String printTree(){
        return printTree("", true);
    }

    /**
     * Generates a string representation of the node and its children
     * @param prefix Spacing to prefix the node in representing the tree structure
     * @param isTail Whether the node is a tail node or not
     * @return A string representation of the node and its children
     */
    private String printTree(String prefix, boolean isTail){

        String result = prefix + (isTail ? "\\--- " : "|--- ") + toString() + "\n";

        TreeNode l = getLeft();
        TreeNode m = getMiddle();
        TreeNode r = getRight();
        List<TreeNode> children = new ArrayList<TreeNode>();

        if(l != null) children.add(l);
        if(m != null) children.add(m);
        if(r != null) children.add(r);

        for (int i = 0; i < children.size() - 1; i++) {
            result += children.get(i).printTree(prefix + (isTail ? "     " : "|    "), false);
        }
        if(children.size() > 0) {
            result += children.get(children.size() - 1).printTree(prefix + (isTail ? "     " : "|    "), true);
        }

        return result;
    }

    public List<TreeNode> toList() {
        List<TreeNode> nodes = new LinkedList<TreeNode>();

        if (nodeType == ParseTreeNodeType.NARRYL ||
                nodeType == ParseTreeNodeType.NDLIST ||
                nodeType == ParseTreeNodeType.NELIST ||
                nodeType == ParseTreeNodeType.NIDLST ||
                nodeType == ParseTreeNodeType.NPLIST ||
                nodeType == ParseTreeNodeType.NPRLIST ||
                nodeType == ParseTreeNodeType.NPROCL ||
                nodeType == ParseTreeNodeType.NSLIST ||
                nodeType == ParseTreeNodeType.NVLIST) {
            nodes.addAll(left.toList());
            nodes.addAll(right.toList());
        } else if (nodeType == ParseTreeNodeType.NLOOP) {
            nodes.addAll(left.toList());
        } else if (nodeType == ParseTreeNodeType.NIFT) {
            nodes.addAll(right.toList());
        } else if (nodeType == ParseTreeNodeType.NIFTE) {
            nodes.addAll(middle.toList());
            nodes.addAll(right.toList());
        }
        else
            nodes.add(this);

        return nodes;
    }

}
