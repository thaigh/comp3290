package CD15.codegen;

import CD15.parser.ParseTreeNodeType;
import CD15.parser.SymbolTableRecord;
import CD15.parser.TreeNode;

/**
 * Generates the declarations for subprocedure parameters
 *
 * @author Tyler Haigh - C3182929
 * @since 10/10/2015
 */
public class Parameter {

    /**
     * Generates the declarations for subprocedure parameters
     * @param gen The Code Generator
     * @param node The root node for parameters (NPARAM)
     */
    public static void generate(CodeGenerator gen, TreeNode node) {

        if (node == null) return;

        TreeNode vars = node.getLeft();
        TreeNode vals = node.getRight();

        genParam(gen, vars);
        genParam(gen, vals);
    }

    /**
     * Generates declarations for a param list based on the parameter type
     * @param gen The code generator
     * @param root The root node for the param list
     */
    private static void genParam(CodeGenerator gen, TreeNode root) {
        if (root == null) return;

        assert root.getNodeType() == ParseTreeNodeType.NPLIST ||
                root.getNodeType() == ParseTreeNodeType.NSIMPAR ||
                root.getNodeType() == ParseTreeNodeType.NARRPAR;

        //Determine if we have a single array or a list of them
        if (root.getNodeType() == ParseTreeNodeType.NPLIST) {
            genParam(gen, root.getLeft());
            genParam(gen, root.getRight());
        } else {
            // It is an NSIMPAR or NARRPAR

            SymbolTableRecord arraySizeTemp = null;
            if (root.getNodeType() == ParseTreeNodeType.NARRPAR) {
                // Generate the array size word first
                arraySizeTemp = new SymbolTableRecord();
                gen.generateDecl(CodeWord.WORD, 0.0, arraySizeTemp);
            }

            SymbolTableRecord localRecord = root.getIdentifierRecord();
            gen.generateDecl(CodeWord.WORD, 0.0, localRecord);

            if (arraySizeTemp != null) {
                // We generated an array param. Store it's size location in memory
                localRecord.setArraySizeLocation(arraySizeTemp.getWordIndex());
                localRecord.setArrayDescLocation(localRecord.getWordIndex());
            }
        }
    }

}
