package CD15.parser.nodes;

import CD15.entity.Token;
import CD15.entity.TokenType;
import CD15.parser.NodeDataType;
import CD15.parser.ParseTreeNodeType;
import CD15.parser.Parser;
import CD15.parser.TreeNode;

import java.util.Arrays;

/**
 * Generates a Term Node of the form:
 * < term>     ::= < fact> < termTail>
 * < termTail> ::= ɛ | * < term> | / < term> | div < term>
 *
 * @author Tyler Haigh - C3182929
 * @since 29/08/2015
 */
public class NTermNode {

    /**
     * Attempts to make a Term Node given a valid token stream
     * @param p The Parser
     * @return A valid Term Tree Node if given a valid token stream or NUNDF
     */
    public static TreeNode make(Parser p) {

        TreeNode fact = NFactNode.make(p);
        TreeNode tail = termTail(p, fact);

        if (tail == null) return fact;
        else return tail;
    }

    /**
     * Attempts to generate the tail tree node for a term
     * @param p The Parser
     * @param left The generated tree node left of the tail
     * @return A valid tree node for the tail, given a valid token stream
     *         or NUNDF if invalid, or null if no tail is present
     */
    private static TreeNode termTail(Parser p, TreeNode left) {
        Token nextTok = p.peek();

        switch (nextTok.getType()) {
            case TMULT: case TDIVD: case TIDIV: {

                TreeNode termOp = termOperator(p);
                TreeNode fact = NFactNode.make(p);

                termOp.setLeft(left);
                termOp.setRight(fact);
                Token afterFact = p.peek();

                if (left.getDataType() != NodeDataType.Float)
                    p.semanticError("Left side of the expression is not a numeric value. Got " + left.getDataType().name(), nextTok.getLine(), nextTok.getColumn());
                else if (fact.getDataType() != NodeDataType.Float)
                    p.semanticError("Right side of the expression is not a numeric value. Got " + fact.getDataType().name(), afterFact.getLine(), afterFact.getColumn());
                else
                    termOp.setDataType(NodeDataType.Float);

                TreeNode termTail = termTail(p, termOp);
                if (termTail == null) return termOp;
                else return termTail;
            }
            default: return null;
        }
    }

    private static TreeNode termOperator(Parser p) {
        Token nextTok = p.peek();
        TreeNode termOp = new TreeNode(ParseTreeNodeType.NUNDEF);

        if (nextTok.getType() == TokenType.TMULT) {
            p.consume();
            termOp.setNodeType(ParseTreeNodeType.NMUL);
        }
        if (nextTok.getType() == TokenType.TDIVD) {
            p.consume();
            termOp.setNodeType(ParseTreeNodeType.NDIV);
        }
        if (nextTok.getType() == TokenType.TIDIV) {
            p.consume();
            termOp.setNodeType(ParseTreeNodeType.NIDIV);
        }

        // Check if we did consume the token
        if (termOp.getNodeType() == ParseTreeNodeType.NUNDEF)
            p.syntaxError("Expected a term operator", nextTok.getLine(), nextTok.getColumn());

        return termOp;

    }

}
