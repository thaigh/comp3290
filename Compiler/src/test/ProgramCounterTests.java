package test;

import CD15.codegen.ProgramCounter;
import junit.framework.Assert;
import org.junit.Test;

/**
 * Program Counter Tests.
 *
 * @author <Authors name>
 * @since <pre>Oct 17, 2015</pre>
 * @version 1.0
 */
public class ProgramCounterTests {


    /**
     * Method: distance(ProgramCounter pc)
     * Testing for correct distance
     */
    @Test
    public void testDistance_16() throws Exception {
        ProgramCounter pc1 = new ProgramCounter();
        pc1.setWord(1);
        pc1.setByte(1);

        ProgramCounter pc2 = new ProgramCounter();
        pc2.setWord(3);
        pc2.setByte(1);

        int expected = 16;
        int actual = pc1.distance(pc2);

        Assert.assertEquals(expected, actual);
    }

    /**
     * Method: distance(ProgramCounter pc)
     * Testing for correct distance
     */
    @Test
    public void testDistance_7() throws Exception {
        ProgramCounter pc1 = new ProgramCounter();
        pc1.setWord(1);
        pc1.setByte(0);

        ProgramCounter pc2 = new ProgramCounter();
        pc2.setWord(1);
        pc2.setByte(7);

        int expected = 7;
        int actual = pc1.distance(pc2);

        Assert.assertEquals(expected, actual);
    }

    /**
     * Method: distance(ProgramCounter pc)
     * Testing for correct distance
     */
    @Test
    public void testDistance_15() throws Exception {
        ProgramCounter pc1 = new ProgramCounter();
        pc1.setWord(0);
        pc1.setByte(0);

        ProgramCounter pc2 = new ProgramCounter();
        pc2.setWord(1);
        pc2.setByte(7);

        int expected = 15;
        int actual = pc1.distance(pc2);

        Assert.assertEquals(expected, actual);
    }

    /**
     * Method: distance(ProgramCounter pc)
     * Testing for correct distance
     */
    @Test
    public void testDistance_0() throws Exception {
        ProgramCounter pc1 = new ProgramCounter();
        pc1.setWord(1);
        pc1.setByte(7);

        ProgramCounter pc2 = new ProgramCounter();
        pc2.setWord(1);
        pc2.setByte(7);

        int expected = 0;
        int actual = pc1.distance(pc2);

        Assert.assertEquals(expected, actual);
    }

    /**
     * Method: distance(ProgramCounter pc)
     * Testing for correct distance
     */
    @Test
    public void testDistance_17() throws Exception {
        ProgramCounter pc1 = new ProgramCounter();
        pc1.setWord(1);
        pc1.setByte(1);

        ProgramCounter pc2 = new ProgramCounter();
        pc2.setWord(3);
        pc2.setByte(2);

        int expected = 17;
        int actual = pc1.distance(pc2);

        Assert.assertEquals(expected, actual);
    }
}
