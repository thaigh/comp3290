package CD15.parser.nodes;

import CD15.entity.Token;
import CD15.entity.TokenType;
import CD15.parser.NodeDataType;
import CD15.parser.ParseTreeNodeType;
import CD15.parser.Parser;
import CD15.parser.TreeNode;

/**
 * Generates a Declaration List node of the form:
 *
 * < decList>     ::= < decl> < decListTail>
 * < decListTail> ::= ɛ | , < decList>
 *
 * @author Tyler Haigh - C3182929
 * @since 28/08/2015
 */
public class NDeclListNode {

    /**
     * Attempts to make a Declaration List node given a valid token stream
     * @param p The Parser
     * @return A valid Tree Node if given a valid token stream, or NUNDF
     */
    public static TreeNode make(Parser p) {

        TreeNode decl = decl(p);
        if (decl != null && decl.getNodeType() == ParseTreeNodeType.NUNDEF) {
            try { errorRecovery(p); }
            catch (Exception e) { return new TreeNode(ParseTreeNodeType.NUNDEF); }
        }

        TreeNode tail = decListTail(p);

        if (tail == null) return decl;
        else {
            TreeNode decList = new TreeNode(ParseTreeNodeType.NDLIST, decl, tail);
            decList.setDataType(NodeDataType.Node);
            return decList;
        }
    }

    /**
     * Attempts to generate a Declaration Node of the form
     *
     * < decl>     ::= < id> < declTail>
     * < declTail> ::= ɛ | [ < intLit> ]
     *
     * @param p The Parser
     * @return A valid Decl Node if given a valid token stream
     */
    private static TreeNode decl(Parser p) {

        TreeNode decl = new TreeNode(ParseTreeNodeType.NUNDEF);

        if (!p.peekAndConsume(TokenType.TIDNT)) {
            p.syntaxError("Expected identifier name in local declaration", p.peekLine(), p.peekColumn());
            return decl;
        }
        Token id = p.lastToken();

        // Prevent double declaration
        if (p.lookupIdentifier(id.getLexeme()) != null) {
            p.semanticError("Variable '" + id.getLexeme() + "' is already declared", id.getLine(), id.getColumn());
            p.createInvalidRecord(id);
        } else {
            p.insertIdentifier(id);
        }

        // ɛ | [ <intLit> ]
        Token nextTok = p.peek();
        if (nextTok.getType() == TokenType.TLBRK) {
            p.consume();

            if (!p.peekAndConsume(TokenType.TILIT)) {
                p.syntaxError("Local array declarations must have an integer length", p.peekLine(), p.peekColumn());
                return decl;
            }
            Token intLit = p.lastToken();
            p.insertConstant(intLit);

            if (!p.peekAndConsume(TokenType.TRBRK)) {
                p.syntaxError("Expected right bracket ] in local array declaration", p.peekLine(), p.peekColumn());
                return decl;
            }

            TreeNode intLitNode = new TreeNode(ParseTreeNodeType.NILIT);
            intLitNode.setConstRecord(intLit.getSymbolTableRecord());
            intLitNode.setDataType(NodeDataType.Float);

            decl.setNodeType(ParseTreeNodeType.NARRDEC);
            decl.setLeft(intLitNode);
            decl.setDataType(NodeDataType.FloatArray);
        } else {
            decl.setNodeType(ParseTreeNodeType.NSIMDEC);
            decl.setDataType(NodeDataType.Float);
        }

        decl.setIdentifierRecord(id.getSymbolTableRecord());
        id.getSymbolTableRecord().setTreeNode(decl);
        return decl;
    }

    /**
     * Generates the declaration list tail
     * @param p The Parser
     * @return Null if there is no list following, or a DeclList node
     */
    private static TreeNode decListTail(Parser p) {

        Token nextTok = p.peek();
        if (nextTok.getType() != TokenType.TCOMA) return null;
        else {
            p.consume();

            TreeNode decList = make(p);
            return decList;
        }
    }

    /**
     * Attempts error recovery in the event of an invalid token stream
     * @param p The Parser
     * @throws Exception Unable to recover
     */
    private static void errorRecovery(Parser p) throws Exception {
        // Find next comma or semi
        int nextComma = p.next(TokenType.TCOMA);
        int nextSemi = p.next(TokenType.TSEMI);

        if (nextComma > -1 && nextComma < 5) p.consume(nextComma);
        else if (nextSemi > -1) p.consume(nextSemi);
        else throw new Exception("Unable to recover");
    }
}
