package CD15.parser.nodes;

import CD15.entity.Token;
import CD15.entity.TokenType;
import CD15.parser.NodeDataType;
import CD15.parser.ParseTreeNodeType;
import CD15.parser.Parser;
import CD15.parser.TreeNode;

/**
 * Created by Administrator on 29/08/2015.
 *
 * Generates an If Node of the form:
 * < ifStat>        ::= if < bool> then < stats> < ifTail> end if
 * < ifTail>        ::= ɛ | else < stats> | elsif < bool> then < stats> < elseifTail>
 * < elseifTail>    ::= elsif < bool> then < stats> < elseifTail> | else < stats>
 */
public class NIfNode {

    /**
     * Attempts to make an NIFT or NIFTE Node for a valid token stream
     * @param p The Parser
     * @return A valid Tree Node for a valid token stream or NUNDF
     */
    public static TreeNode make(Parser p) {

        TreeNode ifNode = new TreeNode(ParseTreeNodeType.NUNDEF);

        if(!p.peekAndConsume(TokenType.TIFKW)) {
            // Should never happen, unless TIFKW is consumed by accident
            p.syntaxError("Expected keyword 'if' for if statement", p.peekLine(), p.peekColumn());
            return ifNode;
        }

        TreeNode bool = NBoolNode.make(p);
        Token afterBool = p.peek();

        if (!p.peekAndConsume(TokenType.TTHEN)) {
            p.syntaxError("Expected keyword 'then' for if statement", p.peekLine(), p.peekColumn());
            return ifNode;
        }

        TreeNode stats = SStatsNode.make(p);
        TreeNode tail = ifTail(p);

        if (!p.peekAndConsume(TokenType.TENDK)) {
            p.syntaxError("Expected keyword 'end' for closing if statement", p.peekLine(), p.peekColumn());
            return ifNode;
        }

        if (!p.peekAndConsume(TokenType.TIFKW)) {
            p.syntaxError("Expected keyword 'if' for closing if statement", p.peekLine(), p.peekColumn());
            return ifNode;
        }

        if (tail == null) {
            ifNode.setNodeType(ParseTreeNodeType.NIFT);
            ifNode.setLeft(bool);
            ifNode.setRight(stats);
        } else {
            ifNode.setNodeType(ParseTreeNodeType.NIFTE);
            ifNode.setLeft(bool);
            ifNode.setMiddle(stats);
            ifNode.setRight(tail);
        }

        if (bool.getDataType() != NodeDataType.Boolean)
            p.semanticError("Expected boolean expression for if statement. Got " + bool.getDataType(), afterBool.getLine(), afterBool.getColumn());
        else
            ifNode.setDataType(NodeDataType.Node);

        return ifNode;

    }

    /**
     * Generates the tail of the IF Node
     * @param p The Parser
     * @return The TreeNode for the tail or NUNDF
     */
    public static TreeNode ifTail(Parser p) {

        Token nextTok = p.peek();
        if (nextTok.getType() == TokenType.TELSE) {
            p.consume();

            TreeNode stats = SStatsNode.make(p);
            return stats;
        } else if (nextTok.getType() == TokenType.TELSF) {
            p.consume();

            TreeNode bool = NBoolNode.make(p);
            Token afterBool = p.peek();

            if (!p.peekAndConsume(TokenType.TTHEN)) {
                p.syntaxError("Expected keyword 'then' for elsif statement", p.peekLine(), p.peekColumn());
                return new TreeNode(ParseTreeNodeType.NUNDEF);
            }

            TreeNode stats = SStatsNode.make(p);
            TreeNode elseifTail = elsifTail(p);

            TreeNode elseifNode = new TreeNode(ParseTreeNodeType.NIFTE, bool, stats, elseifTail);

            if (bool.getDataType() != NodeDataType.Boolean)
                p.semanticError("Expected boolean expression for if statement. Got " + bool.getDataType(), afterBool.getLine(), afterBool.getColumn());
            else
                elseifNode.setDataType(NodeDataType.Node);

            return elseifNode;
        }

        return null;
    }

    /**
     * Generates the tail for an ELSEIF Node
     * @param p The Parser
     * @return The Treenode for the tail or NUNDF
     */
    public static TreeNode elsifTail(Parser p) {

        Token nextTok = p.peek();
        if (nextTok.getType() == TokenType.TELSE) {
            p.consume();

            TreeNode stats = SStatsNode.make(p);
            return stats;
        } else if (nextTok.getType() == TokenType.TELSF) {
            p.consume();

            TreeNode bool = NBoolNode.make(p);
            Token afterBool = p.peek();

            if (!p.peekAndConsume(TokenType.TTHEN)) {
                p.syntaxError("Expected keyword 'then' for elsif statement", p.peekLine(), p.peekColumn());
                return new TreeNode(ParseTreeNodeType.NUNDEF);
            }

            TreeNode stats = SStatsNode.make(p);
            TreeNode elseifTail = elsifTail(p);

            TreeNode elseifNode = new TreeNode(ParseTreeNodeType.NIFTE, bool, stats, elseifTail);

            if (bool.getDataType() != NodeDataType.Boolean)
                p.semanticError("Expected boolean expression for if statement. Got " + bool.getDataType(), afterBool.getLine(), afterBool.getColumn());
            else
                elseifNode.setDataType(NodeDataType.Node);

            return elseifNode;
        } else {
            return new TreeNode(ParseTreeNodeType.NUNDEF);
        }

    }
}
