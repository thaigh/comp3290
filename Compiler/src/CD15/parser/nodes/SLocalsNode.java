package CD15.parser.nodes;

import CD15.entity.Token;
import CD15.entity.TokenType;
import CD15.parser.ParseTreeNodeType;
import CD15.parser.Parser;
import CD15.parser.TreeNode;

import java.util.Arrays;

/**
 * Generates a Locals node of the form:
 * <locals> ::= ɛ | local <decList> ;
 *
 * @author Tyle Haigh - C3182929
 * @since 28/08/2015
 */
public class SLocalsNode {

    /**
     * Attempts to make the Locals node given a valid token stream
     * @param p The Parser
     * @return A valid Locals Tree Node for a valid token stream ro NUNDF
     */
    public static TreeNode make(Parser p) {

        Token nextTok = p.peek();
        if (nextTok.getType() != TokenType.TLOCL) return null;
        else {
            p.consume();

            TreeNode decl = NDeclListNode.make(p);
            if (decl != null && decl.getNodeType() == ParseTreeNodeType.NUNDEF) {
                try { errorRecoverIdList(p); }
                catch (Exception e) { return new TreeNode(ParseTreeNodeType.NUNDEF); }
            }

            if (!p.peekAndConsume(TokenType.TSEMI)) {
                p.syntaxError("Expected semicolon ; at end of local declarations list", p.peekLine(), p.peekColumn());
                try { errorRecoverySemi(p); }
                catch (Exception e) { return new TreeNode(ParseTreeNodeType.NUNDEF); }
            }

            return decl;
        }
    }

    /**
     * Attempts error recovery in the event of an invalid Declaration list
     * @param p The Parser
     * @throws Exception Unable to recover
     */
    private static void errorRecoverIdList(Parser p) throws Exception {
        int nextSemi = p.next(TokenType.TSEMI);
        if (nextSemi > -1) p.consume(nextSemi);
        else throw new Exception("Unable to recover");
    }

    /**
     * Attempts error recovery in the event of a missing semicolon
     * @param p The Parser
     * @throws Exception Unable to recover
     */
    private static void errorRecoverySemi(Parser p) throws Exception {
        int[] scanAmounts = new int[12];
        // Scan ahead to find the next statement or END
        scanAmounts[0] = p.next(TokenType.TLOOP);
        scanAmounts[1] = p.next(TokenType.TEXIT);
        scanAmounts[2] = p.next(TokenType.TIFKW);
        scanAmounts[3] = p.next(TokenType.TINPT);
        scanAmounts[4] = p.next(TokenType.TPRIN);
        scanAmounts[5] = p.next(TokenType.TPRLN);
        scanAmounts[6] = p.next(TokenType.TCALL);
        scanAmounts[7] = p.next(TokenType.TIDNT);
        scanAmounts[8] = p.next(TokenType.TELSE);
        scanAmounts[9] = p.next(TokenType.TELSF);
        scanAmounts[10] = p.next(TokenType.TSEMI); // SEMI for IdList or first Statement
        scanAmounts[11] = p.next(TokenType.TENDK); // END PROC or END PROGRAM

        // Find the min
        Arrays.sort(scanAmounts);
        int minScanAmount = -1;
        for (int i = 0; i < scanAmounts.length; i++) {
            if (scanAmounts[i] != -1) {
                minScanAmount = scanAmounts[i];
                break;
            }
        }

        if (minScanAmount == -1) throw new Exception("Unable to scan ahead");
        else {
            if (p.peek(minScanAmount).getType() == TokenType.TSEMI)
                p.consume(minScanAmount+1);
            else p.consume(minScanAmount);
        }
    }
}
