package CD15.codegen;

import CD15.parser.SymbolTable;
import CD15.parser.SymbolTableRecord;
import CD15.parser.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * The Code generator responsible for generating the byte code instructions
 * for a CD15 code file
 *
 * @author Tyler Haigh - C3182929
 * @since 7/10/2015
 */
public class CodeGenerator {

    private StringBuffer declarations;
    private List<InstructionMatrix> subprograms;
    private InstructionMatrix currentSubprogram;

    private int nextWord;
    private int nextArrayIndex;
    private int nextSubProgIndex;

    private SymbolTable constants;
    private Pool tempPool;

    /**
     * Initialises the Code Generator
     * @param constants The Constants Symbol Table
     */
    public CodeGenerator(SymbolTable constants) {
        this.declarations = new StringBuffer();
        this.nextWord = 0;
        this.nextArrayIndex = 0;
        this.nextSubProgIndex = 1;
        this.constants = constants;
        this.tempPool = new Pool();
        this.subprograms = new ArrayList<InstructionMatrix>();
        this.currentSubprogram = null;
    }

    /**
     * Generates the code module for the program
     *
     * @param parseTree The root node of the CD15 parse tree (PROG)
     * @return A string containing the generated code to be printed
     */
    public String generate(TreeNode parseTree) {
        // We need to load the constants table into memory
        // Array sizes are stored in constants table and will be allocated memory as well
        for (SymbolTableRecord rec : constants.getRecords()) {
            switch (rec.getType()) {
                case TILIT: generateDecl(CodeWord.WORD, Integer.parseInt(rec.getLexeme()), rec); break;
                case TFLIT: generateDecl(CodeWord.WORD, Double.parseDouble(rec.getLexeme()), rec); break;
                case TSTRG: generateDecl(CodeWord.STRING, 0, rec); break;
            }
        }
        declarations.append("\n");

        TreeNode arrays = parseTree.getLeft();
        TreeNode procedures = parseTree.getMiddle();
        TreeNode main = parseTree.getRight();

        // Code Gen each subtree recursively
        ArrayBlock.generate(this, arrays);
        declarations.append("\n");

        generateSubProcs(procedures);
        generateMain(main);

        // Flush the AC15 machine print buffer
        generate1Byte(OpCode.NEWLN);

        return declarations.toString() + "\n" + generateInstruction() + "END";
    }

    /**
     * Generates the byte code for sub procedures
     * @param procs The root treenode for the Procs
     */
    private void generateSubProcs(TreeNode procs) {

        if (procs == null) return;

        // Get each subproc
        List<TreeNode> procList = procs.toList();
        for(TreeNode proc : procList) {

            proc.getIdentifierRecord().setSubProgDescIndex(nextSubProgIndex++);

            // Create a new subprogram code matrix to start writing to
            currentSubprogram = new InstructionMatrix();

            // Generate code
            SubProc.generate(this, proc);

            // Append extra row of empty data if we have completely filled the matrix
            if (currentSubprogram.getProgramCounter().getByte() == 7) {
                currentSubprogram.addByte(0); // Forces creation of new row
            }

            subprograms.add(currentSubprogram);
        }
    }

    /**
     * Generates the Main subprocedure of the program and adds it to the start of the procedures list
     * as the entry point to the program
     * @param main The Main TreeNode
     */
    private void generateMain(TreeNode main) {

        // Set the subprogram matrix and start writing code to it
        currentSubprogram = new InstructionMatrix();
        MainProc.generate(this, main);

        // Append extra row of empty data if we have completely filled the matrix
        if (currentSubprogram.getProgramCounter().getByte() == 7) {
            currentSubprogram.addByte(0); // Forces creation of new row
        }

        // Add the subprogram to the top of the list so it is the first program
        // in the AC15 machine
        subprograms.add(0, currentSubprogram);
    }

    /**
     * Converts each subprocedure bytecode matrix into a string representation
     * @return A string representation of the generated subprocedure code
     */
    private String generateInstruction() {
        StringBuffer buffer = new StringBuffer();

        for(InstructionMatrix proc : subprograms) {
            buffer.append(proc.generate());
            buffer.append("\n\n");
        }

        return buffer.toString();
    }

    /**
     * Generates a memory allocation in the AC15 machine
     * @param word The type of declaration to create
     * @param value An initial value for the declared variable
     * @param record The variable's symbol table record
     */
    public void generateDecl(CodeWord word, double value, SymbolTableRecord record) {

        switch (word) {
            case BLOCK: {

                String arraySizeDecl = CodeWord.WORD.name() + " " + (int)value + "\n";
                String arrayDescIndexDecl = CodeWord.WORD.name() + " " + nextArrayIndex + "\n";
                String arrayBlockDecl = word.name() + " " + (int)value + " " + "\n";

                declarations.append(arraySizeDecl);
                declarations.append(arrayDescIndexDecl);
                declarations.append(arrayBlockDecl);

                // Set array memory addresses to record
                record.setArraySizeLocation(nextWord++);
                record.setArrayDescLocation(nextWord++);

                record.setWordIndex(nextWord);
                record.setArrayDescIndex(nextArrayIndex);

                // Update the memory pointers
                nextWord += value;
                nextArrayIndex++;
                break;
            }
            case WORD: {
                String decl = word.name() + " " + value + " " + "\n";
                declarations.append(decl);

                // Set memory address to record
                record.setWordIndex(nextWord);

                // Update the memory pointers
                nextWord++;
                break;
            }
            case STRING:
                // Format:
                // STRING <wordLength>
                //    <ASCII Characters>
                //    <ASCII Characters> <null padding>

                String str = record.getLexeme();
                int words = (str.length() / 8) + 1;

                String decl = "";

                for (int i = 0; i < str.length(); i += 0) {

                    String substr = str.substring(i, Math.min(i + 8, str.length()));
                    int bytes = Math.min(8, substr.length());

                    for(int j = 0; j < bytes; j++) {
                        int ascii = (int)str.charAt(i++);
                        decl += String.format("%1$3s ", ascii);
                    }

                    if (i % 8 == 0)
                        decl += "\n";
                }

                // Append trailing 0s
                int remainder = 8 - (str.length() % 8);
                for(int i = 0; i < remainder; i++)
                    decl += String.format("%1$3s ", 0);

                decl += "\n";
                decl = "STRING " + words + "\n" + decl;
                declarations.append(decl);

                // Set memory address to record
                record.setWordIndex(nextWord);

                // Update the memory pointers
                nextWord += words;
                break;


            default: // TODO: Handle this. Should never happen
        }
    }

    /**
     * Generates a single byte instruction (an opcode)
     * @param opCode The opcode to generate
     */
    public void generate1Byte(OpCode opCode) {
        currentSubprogram.addByte(opCode.value());
    }

    /**
     * Generates a 3 byte instruction with an opcode and 2 effective operands
     * @param opCode The Opcode
     * @param operand The operand to generate
     */
    public void generate3Bytes(OpCode opCode, int operand) {
        currentSubprogram.addByte(opCode.value());

        // Split into bytes
        int op1 = operand / (int)Math.pow(2, 8);
        int op2 = operand - op1;

        currentSubprogram.addByte(op1);
        currentSubprogram.addByte(op2);
    }

    /**
     * Generates a 5 byte instruction with an opcode and 4 effective operands
     * @param opCode The OpCode
     * @param operand The operand
     */
    public void generate5Bytes(OpCode opCode, int operand) {

        // Split into bytes
        int[] operands = new int[5];

        operands[0] = opCode.value();
        for (int i = 1; i < 5; i++) {

            int power = 8 * (5 - i - 1);
            int divisor = (int)Math.pow(2, power);

            int higherOrder = operand / divisor;
            operands[i] = higherOrder;
            operand -= higherOrder * divisor;
        }

        currentSubprogram.addByte(operands[0]);
        currentSubprogram.addByte(operands[1]);
        currentSubprogram.addByte(operands[2]);
        currentSubprogram.addByte(operands[3]);
        currentSubprogram.addByte(operands[4]);

    }

    /**
     * Resets the current subprogram's program counter to Word 0, Byte 0
     */
    public void resetProgramCounter() { currentSubprogram.setProgramCounter(new ProgramCounter()); }

    /**
     * Retrieves a deep copy of the program counter for the current subprogram
     * @return The current subprocedure's program counter
     */
    public ProgramCounter getProgramCounter() {
        ProgramCounter pc = new ProgramCounter();
        ProgramCounter current = currentSubprogram.getProgramCounter();

        // Set the data to avoid java references updating the PC
        pc.setWord(current.getWord());
        pc.setByte(current.getByte());

        return pc;
    }

    /**
     * Gets the next available temporary memory storage variable in AC15.
     * If a new one is created, it is declared and allocated in static memory
     * @return The AC15 memory address of the next available temporary variable
     */
    public int getNextTemp() {
        int origSize = tempPool.size();
        TempVar res = tempPool.getResource();
        int afterSize = tempPool.size();

        //Check if we needed to create a new temp var and allocate in memory if so
        if (afterSize > origSize) {
            res.setWordIndex(nextWord);
            generateDecl(CodeWord.WORD, 0.0, new SymbolTableRecord());
        }

        return res.getWordIndex();
    }

    /**
     * Releases a temporary variable to allow it to be reused
     * @param temp The AC15 memory address of the temp variable
     */
    public void releaseTemp(int temp) { tempPool.releaseResource(temp); }

    /**
     * Replaces the instruction starting at the given program counter with all
     * values in the given array
     * @param pc The Subprogram Program Counter
     * @param values The values to overwrite with
     */
    public void replaceInstruction(ProgramCounter pc, int[] values) {

        // Get the start word and byte location
        int word = pc.getWord();
        int _byte = pc.getByte();

        for (int i = 0; i < values.length; i++) {
            if (_byte > 7) {
                word++;
                _byte = 0;
            }

            currentSubprogram.replaceByte(word, _byte, values[i]);

            // Increment the Program Counter location
            _byte++;
        }
    }

    /**
     * Gets the OpCode or Operand at the given program counter position
     * @param pc The subprogram's program counter
     * @return The OpCode or byte operand at the program counter'a address
     */
    public int getInstruction(ProgramCounter pc) {
        return currentSubprogram.getByte(pc.getWord(), pc.getByte());
    }

}
