import CD15.Compiler;

import java.io.*;

/**
 * Entry point to the Compiler Application.
 * Takes a file as command line input for the Compiler to perform
 * Lexical and Syntactic Analysis in order to produce machine code
 *
 * @author Tyler Haigh - C3182929
 * @since 2/08/2015
 */
public class Program {

    public static void main(String[] args) {

        if (args.length == 0) {
            System.out.println("Error: No file to compile");
            System.exit(1);
        }

        // Get the input file from command line
        String filePath = args[0];
        File file = new File(filePath);

        if (!file.exists()) {
            System.out.println("Error: File does not exist");
            System.exit(1);
        }

        // Get program listing file path if it's there
        File outFile = null;
        // Try to create the file
        try {
            if (args.length > 1) {
                outFile = new File(args[1]);
                if (!outFile.exists() && !outFile.createNewFile()) {
                    System.err.println("Error: Unable to create program listing file. Printing to System.out");
                    outFile = null;
                }
            }
        } catch (IOException io) {
            System.err.println("Error: Unable to create program listing file. Printing to System.out");
            outFile = null;
        }

        // Get the compiled code output file if its there
        File codeFile = null;
        try {
            if (args.length > 2) {
                codeFile = new File(args[2]);
                if (!codeFile.exists() && !codeFile.createNewFile()) {
                    System.err.println("Error: Unable to create code module file. Printing to System.out");
                    codeFile = null;
                }
            }
        } catch (IOException io) {
            System.err.println("Error: Unable to create code module file. Printing to System.out");
            codeFile = null;
        }


        // File exists. Start compiling
        CD15.Compiler comp = new Compiler(file, outFile, codeFile);
        comp.run();
    }
}