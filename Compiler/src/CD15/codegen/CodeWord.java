package CD15.codegen;

/**
 * Defines the possible words for generating declarations
 *
 * @author Tyler Haigh - C3182929
 * @since 10/10/2015
 */
public enum CodeWord {
    WORD,
    BLOCK,
    STRING,

    CODE,
    END
}
