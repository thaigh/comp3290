package CD15.codegen;

import CD15.parser.ParseTreeNodeType;
import CD15.parser.SymbolTableRecord;
import CD15.parser.TreeNode;

/**
 * Generates the declarations for local variables
 *
 * @author Tyler Haigh - C3182929
 * @since 10/10/2015
 */
public class LocalWord {

    /**
     * Generates the declarations for local variables
     * @param gen The Code Generator
     * @param node The root node for the locals
     */
    public static void generate(CodeGenerator gen, TreeNode node) {

        if (node == null) return;

        assert node.getNodeType() == ParseTreeNodeType.NSIMDEC ||
                node.getNodeType() == ParseTreeNodeType.NIDLST ||
                node.getNodeType() == ParseTreeNodeType.NDLIST;

        //Determine if we have a single array or a list of them
        if (node.getNodeType() == ParseTreeNodeType.NIDLST || node.getNodeType() == ParseTreeNodeType.NDLIST) {
            generate(gen, node.getLeft());
            generate(gen, node.getRight());
        } else {
            // It is an NSIMDEC
            SymbolTableRecord localRecord = node.getIdentifierRecord();
            gen.generateDecl(CodeWord.WORD, 0.0, localRecord);
        }
    }
}
