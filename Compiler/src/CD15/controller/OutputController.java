package CD15.controller;

import CD15.entity.Error;

import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

/**
 * COMP3290 CD15 Compiler - Output Controller class.
 *
 * Contains descriptor for the listing (PrintWriter) file and a
 * reference to the error buffer (StringBuffer).
 *
 * Produces the program listing (incl errors) line by line and a
 * complete error report to the error buffer. Expects the listing
 * file to be an open text stream and the error buffer to have been
 * initialised empty.
 *
 * The output protocol expected from whatever object is reading the
 * input is to read a character and to immediately "printChar" that character.
 *
 */
public class OutputController {

	private PrintWriter listing = null;

    private List<Error> errorList = null; // Permanent Storage
    private List<Error> errorBuffer = null; // Temp Storage per line

    private String currLine = null;
    private int line = 0;


    public OutputController(PrintWriter listing) {
		this.listing = listing;
        this.errorList = new LinkedList<Error>();
        this.errorBuffer = new LinkedList<Error>();

		this.currLine = "  1: ";
        this.line = 1;
	}

	public int getErrorCount() { return errorList.size(); }


    /**
     * Required as otherwise lexical errors occurring at end of
     * line will be reported as belonging to character position 0 of the following line.
     * @param error
     */
	public void printImmediateError(CD15.entity.Error error) {
		listing.println(error.toString());
		errorList.add(error);
	}

    /**
     * Method setError(String) serves as a central mechanism for reporting
     * errors and can be used by future phases to report parsing errors and
     * semantic errors.
     * @param error
     */
	public void addError(CD15.entity.Error error) {
        errorBuffer.add(error);
	}

    /**
     * Method printChar(char) saves the next character and performs necessary
     * output to file/buffer only when an end of line (\n) character is output.
     * @param ch
     */
	public void printChar(char ch) {
	    // stores next char - Prints a listing line if a newline char

		if (ch == '\n') {
			// At newline. Produce the next line of the listing
			listing.println(currLine);

            flushErrors();

			line++;
            currLine = (line < 10) ? "  " + line + ": " :
                      (line < 100) ? " "  + line + ": " : line + ": ";

		} else {
			// Put the character into the output buffer
			currLine += String.valueOf(ch);
		}
	}

    public void flushMessages() {
        if (!currLine.equals("")) {
            //Produce the next line of the listing
            listing.println(currLine);
            flushErrors();
        }
    }

	public void flushErrors( ) {
		// Due to sliding window of tokens, errors near eof must be explicitly flushed

        if (errorBuffer.size() > 0) {
            // Add the current line to the error list
            errorList.add(new Error() {{setMessage(currLine);}} );

            for(CD15.entity.Error e : errorBuffer) {
                listing.println(e.toString());

                // Add buffered error to permanent error list
                errorList.add(e);
            }

            // Flush error buffer
            errorBuffer.clear();
        }
	}

}
