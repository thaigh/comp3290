package CD15.parser.nodes;

import CD15.entity.Token;
import CD15.entity.TokenType;
import CD15.parser.NodeDataType;
import CD15.parser.ParseTreeNodeType;
import CD15.parser.Parser;
import CD15.parser.TreeNode;

import java.util.Arrays;

/**
 * Generates an Assignment Statement node of the form:
 *
 * < asgnStat>      ::= < var> < asgnOp> < expr> ;
 * < asgnOp>        ::= += | -= | *= | /= | =
 *
 * @author Tyler Haigh - C3182929
 * @since 30/08/2015
 */
public class NAsgnNode {

    /**
     * Attempts to generate an assignment statement node given a syntactically valid token stream
     * @param p The Parser
     * @return A valid Tree Node or NUNDF if the stream contains invalid tokens
     */
    public static TreeNode make(Parser p) {

        Token beforeVar = p.peek();

        TreeNode var = NVarNode.make(p);
        if (var != null && var.getNodeType() == ParseTreeNodeType.NUNDEF) {
            try { errorRecoveryVar(p); }
            catch (Exception e) { return new TreeNode(ParseTreeNodeType.NUNDEF); }
        }

        TreeNode asgnOp = asgnOp(p);
        if (asgnOp.getNodeType() == ParseTreeNodeType.NUNDEF) {
            p.syntaxError("Expected an assignment operator for assignment statement", p.peekLine(), p.peekColumn());
            return new TreeNode(ParseTreeNodeType.NUNDEF);
        }

        TreeNode expr = NExprNode.make(p);
        if (expr != null && expr.getNodeType() == ParseTreeNodeType.NUNDEF) {
            return new TreeNode(ParseTreeNodeType.NUNDEF);
        }

        if (!p.peekAndConsume(TokenType.TSEMI)) {
            p.syntaxError("Expected semicolon ; in assignment statement", p.peekLine(), p.peekColumn());
            return new TreeNode(ParseTreeNodeType.NUNDEF);
        }

        asgnOp.setLeft(var);
        asgnOp.setRight(expr);

        Token afterExpr = p.peek();

        if (var != null && var.getDataType() != NodeDataType.Float)
            p.semanticError("Left side of the expression is not a numeric value. Got " + var.getDataType().name(), beforeVar.getLine(), beforeVar.getColumn());
        else if (expr != null && expr.getDataType() != NodeDataType.Float)
            p.semanticError("Right side of the expression is not a numeric value. Got " + expr.getDataType().name(), afterExpr.getLine(), afterExpr.getColumn());
        else
            asgnOp.setDataType(NodeDataType.Float);

        return asgnOp;
    }

    /**
     * Returns an Assignment Operator node given a particular token
     * @param p The Parser
     * @return The node representing the token
     */
    private static TreeNode asgnOp(Parser p) {
        Token nextTok = p.peek();

        if (nextTok.getType() == TokenType.TPLEQ) {
            p.consume();
            return new TreeNode(ParseTreeNodeType.NPLEQ);
        }
        if (nextTok.getType() == TokenType.TMNEQ) {
            p.consume();
            return new TreeNode(ParseTreeNodeType.NMNEQ);
        }
        if (nextTok.getType() == TokenType.TMLEQ) {
            p.consume();
            return new TreeNode(ParseTreeNodeType.NSTEQ);
        }
        if (nextTok.getType() == TokenType.TDVEQ) {
            p.consume();
            return new TreeNode(ParseTreeNodeType.NDVEQ);
        }
        if (nextTok.getType() == TokenType.TASGN) {
            p.consume();
            return new TreeNode(ParseTreeNodeType.NASGN);
        }

        return new TreeNode(ParseTreeNodeType.NUNDEF);
    }

    /**
     * Attempts error recovery within the assignment statement
     * @param p The Parser
     * @throws Exception Unable to recover
     */
    private static void errorRecoveryVar(Parser p) throws Exception {
        // Find next assignment operator
        int[] scanAmounts = new int[6];
        scanAmounts[0] = p.next(TokenType.TPLEQ);
        scanAmounts[1] = p.next(TokenType.TMNEQ);
        scanAmounts[2] = p.next(TokenType.TMLEQ);
        scanAmounts[3] = p.next(TokenType.TDVEQ);
        scanAmounts[4] = p.next(TokenType.TASGN);

        // We have to recover to SEMI as well because
        // the next thing could be an IDNT which would
        // cause another ASGN statement
        scanAmounts[5] = p.next(TokenType.TSEMI);

        // Find the min
        Arrays.sort(scanAmounts);
        int minScanAmount = -1;
        for (int i = 0; i < scanAmounts.length; i++) {
            if (scanAmounts[i] != -1) {
                minScanAmount = scanAmounts[i];
                break;
            }
        }

        if (minScanAmount == -1) throw new Exception("Unable to scan ahead");
        else {
            if (p.peek(minScanAmount).getType() == TokenType.TSEMI)
                p.consume(minScanAmount + 1); // Consume the semi as well
            else p.consume(minScanAmount);
        }
    }

}
