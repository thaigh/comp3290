package CD15.controller;

/**
 * Sanitises the file input stream to the compiler to simplify character groups for parsing
 *
 * @author Tylerf Haigh - C3182929
 * @since 14/08/2015
 */
public class InputController {

    /**
     * Processes the input stream and removes any unwanted characters and replaces them
     * with easier to handle characters
     *
     * @param input The file input stream to the compiler
     * @return A sanitised stream ready for compilation
     */
    public static String processInput(String input) {
        // Strip out \r\n and replace with \n
        input = input.replaceAll("\r\n", "\n");

        // Replace \t with space
        input = input.replaceAll("\t", " ");

        return input;
    }
}
