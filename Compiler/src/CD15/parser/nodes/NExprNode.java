package CD15.parser.nodes;

import CD15.entity.Token;
import CD15.entity.TokenType;
import CD15.parser.NodeDataType;
import CD15.parser.ParseTreeNodeType;
import CD15.parser.Parser;
import CD15.parser.TreeNode;

/**
 * Generates an Expression node of the form:
 * < expr>     ::= < term> < exprTail>
 * < exprTail> ::= ɛ | + < expr> | - < expr>
 *
 * @author Tyler Haigh - C3182929
 * @since 29/08/2015
 */
public class NExprNode {

    /**
     * Attempts to make an expression node given a valid token stream
     * @param p The Parser
     * @return A valid Tree Node for a valid token stream or NUNDF
     */
    public static TreeNode make(Parser p) {

        TreeNode term = NTermNode.make(p);
        TreeNode tail = exprTail(p, term);

        if (tail == null) return term;
        else return tail;
    }

    /**
     * Attempts to generate the expression tail node
     * @param p The Parser
     * @param left The node on the left of the tail expression
     * @return A TreeNode for the expression
     */
    private static TreeNode exprTail(Parser p, TreeNode left) {
        Token nextTok = p.peek();

        switch (nextTok.getType()) {
            case TPLUS: case TSUBT: {
                TreeNode exprOp = exprOperator(p);
                TreeNode term = NTermNode.make(p);

                exprOp.setLeft(left);
                exprOp.setRight(term);
                Token afterTerm = p.peek();

                if (left.getDataType() != NodeDataType.Float)
                    p.semanticError("Left side of the expression is not a numeric value. Got " + left.getDataType().name(), nextTok.getLine(), nextTok.getColumn());
                else if (term.getDataType() != NodeDataType.Float)
                    p.semanticError("Right side of the expression is not a numeric value. Got " + term.getDataType().name(), afterTerm.getLine(), afterTerm.getColumn());
                else
                    exprOp.setDataType(NodeDataType.Float);

                TreeNode exprTail = exprTail(p, exprOp);
                if (exprTail == null) return exprOp;
                else return exprTail;
            }
            default: return null;
        }
    }

    private static TreeNode exprOperator(Parser p) {
        Token nextTok = p.peek();
        TreeNode exprOp = new TreeNode(ParseTreeNodeType.NUNDEF);

        if (nextTok.getType() == TokenType.TPLUS) {
            p.consume();
            exprOp.setNodeType(ParseTreeNodeType.NADD);
        }
        if (nextTok.getType() == TokenType.TSUBT) {
            p.consume();
            exprOp.setNodeType(ParseTreeNodeType.NSUB);
        }

        // Check if we did consume the token
        if (exprOp.getNodeType() == ParseTreeNodeType.NUNDEF)
            p.syntaxError("Expected expression operator", nextTok.getLine(), nextTok.getColumn());

        return exprOp;
    }

}
